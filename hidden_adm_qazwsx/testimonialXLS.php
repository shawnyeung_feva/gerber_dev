<?php
set_time_limit(3600);
define('_VALID_INCLUDE', TRUE); // flag to allow include or require files
$dir_level = "../"; //set the required files located

require_once($dir_level.'includes/vars.inc.php');
require_once($dir_level.'includes/common.inc.php');

checkadmin(); // require admin

$pagetitle = "Testimonial";

$pagesize = 99999999;

$sorting = trim(htmlencode($_GET["sorting"]));
$pageno = trim(htmlencode($_GET["selpageno"]));

$bb_name = trim(htmlencode($_GET["bb_name"]));
   
if ($pageno == "") {
	$pageno = 1;
}

// fix for IE catching or PHP bug issue
header("Pragma: public");
header("Expires: 0"); // set expiration time
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
// browser must download file from server instead of cache

// force download dialog
header("Content-Type: application/vnd.ms-excel;charset='utf-8'");

// use the Content-Disposition header to supply a recommended filename and
// force the browser to display the save dialog.
header("Content-Disposition: attachment; filename=testimonial_".date("Y_m_d_H_i_s").".xls;");

?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
                xmlns:x="urn:schemas-microsoft-com:office:excel"
                xmlns="http://www.w3.org/TR/REC-html40">
<head>
        <meta http-equiv=Content-Type content="text/html; charset=<?php echo $cfg['charset']; ?>">
        <!--[if gte mso 9]><xml>
        <x:ExcelWorkbook>
        <x:ExcelWorksheets>
                   <x:ExcelWorksheet>
                   <x:Name></x:Name>
                   <x:WorksheetOptions>
                                   <x:DisplayGridlines/>
                   </x:WorksheetOptions>
                   </x:ExcelWorksheet>
        </x:ExcelWorksheets>
        </x:ExcelWorkbook>
        </xml><![endif]-->
</head>
<body>
<?php

// filter query
$filter_sql="";

// open db connection
$$conn = openConnection($$conn);

$sql = " SELECT * FROM `gerberfev_game_201710` WHERE 1 ";
$sql.= " AND `status` = '1' ";
if ($bb_name != "") {
	$sql.= " and `bb_name` like ('%".$bb_name."%') ";
}
$sql.= " ORDER BY `id` ASC ";

$result = mysql_query($sql, $$conn);
$num = mysql_num_rows($result);

$totcount = $num;
$pagecount = ceil($totcount/$pagesize);

if ($pagecount < 1) {
	$pagecount = 1;
}

?>
<table width="1750" border="0" cellspacing="0" cellpadding="0">
	<tr height="17" style="height:12.75pt">
		<td><b><?php echo $pagetitle; ?></b></td>
	</tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr > 
		  <td width="50" >ID</td>
		  <td width="100" >MOTHER NAME</td>
		  <td width="100" >BB NAME</td>
		  <td width="100" >PHONE</td>
		  <td width="100" >TESTIMONIAL</td>
		  <td width="400" >ORIGINAL PHOTO URL</td>
          <td width="200" >LIKE COUNT</td>
          <td width="200" >SHARE FACBOOK COUNT</td>
          <td width="200" >SHARE WHATSAPP COUNT</td>
          <td width="100" >CODE</td>
          <td width="100" >TYPE</td>
          <td width="200" >DATE</td>
        </tr>
<?php
$totcurrent = 0;


while ($row=mysql_fetch_array($result)) {

?>
        <tr> 
		  <TD align="left" style='mso-number-format:"\@";' ><?php echo ($row["id"]); ?></TD>
		  <TD align="left"><?php echo ($row["mother_name"]); ?></TD>
		  <TD align="left"><?php echo ($row["bb_name"]); ?></TD>
		  <TD align="left" style='mso-number-format:"\@";' ><?php echo chunk_split($row["phone"], 4, ' '); ?></TD>
		  <TD align="left"><?php echo htmldecode($row["testimonial"]); ?></TD>
		  <TD align="left"><?php if ($row["filename"] != "") { ?><a href="<?php echo $cfg["root"]."../".($row["filename"]); ?>" target="_blank"><?php echo $cfg["root"]."../".($row["filename"]); ?></a><?php } ?></TD>
		  <TD align="left" style='mso-number-format:"\@";' ><?php echo ($row["like_count"]); ?></TD>
		  <TD align="left" style='mso-number-format:"\@";' ><?php echo ($row["share_fb_count"]); ?></TD>
		  <TD align="left" style='mso-number-format:"\@";' ><?php echo ($row["share_wa_count"]); ?></TD>
		  <TD align="left" style='mso-number-format:"\@";' ><?php echo ($row["code"]); ?></TD>
		  <TD align="left"><?php 
			if ($row["type"]=="1") {
				echo '<font color="#FF9900">Star</font>';
			} else if ($row["type"]=="2") {
				echo '<font color="#000000">Not Star</font>';
			}
		  ?></TD>
		  <TD align="left"><?php echo date("Y-m-d H:i:s",strtotime($row["created_date"])); ?></TD>
        </tr>
<?php

$totcurrent = $totcurrent + 1;
}

?>

      </table></td>
  </tr>
</table>
</body>
</html>
<?php

// close db connection
$$conn = closeConnection($$conn);

?>