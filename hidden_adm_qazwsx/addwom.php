<?php
define('_VALID_INCLUDE', TRUE); // flag to allow include or require files
$dir_level = "../"; //set the required files located

require_once($dir_level.'includes/vars.inc.php');
require_once($dir_level.'includes/common.inc.php');

checkadmin(); // require admin

// open db connection
$$conn = openConnection($$conn);

if ($_POST) {
	include_once("addwom.inc.php");
}

$frmAction = strtolower($_GET["frmaction"]);

$lngEditID = htmlencode($_GET["editid"]);

if ($frmAction == "edit") {

	// open db connection
	$$conn = openConnection($$conn);

	$sql = " select * from `nanpro_wom_share` where 1 ";
	$sql.= " and shareid='$lngEditID' ";

	$result = mysql_query($sql, $$conn);
	$num = mysql_num_rows($result);
	
	$row = mysql_fetch_array($result);

	// close db connection
	$$conn = closeConnection($$conn);

}

// Force charset
header("Content-type: text/html; charset=".$cfg['charset']);
?>
<html>
<head>
<title><?php echo $cfg['site_name']; ?> - CMS</title>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $cfg['charset']; ?>">
<meta name="keywords" content="<?php echo $cfg['meta_keywords']; ?>">
<meta name="description" content="<?php echo $cfg['meta_description']; ?>">
<meta name="generator" content="<?php echo $cfg['meta_generator']; ?>">
<meta name="robots" content="noindex, nofollow, noarchive">
<meta name="MSSmartTagsPreventParsing" content="TRUE">
<meta http-equiv="MSThemeCompatible" content="Yes">
<link href="images/global.css" rel="stylesheet" type="text/css">

<style>
.btn_upload,
.btn_upload_again {
  background-color: #999;
  color: #FFF;
  padding: 6px 30px;
  -webkit-border-radius: 6px;
  -webkit-background-clip: padding-box;
  -moz-border-radius: 6px;
  -moz-background-clip: padding;
  border-radius: 6px;
  background-clip: padding-box;
  display: inline-block;
  font-size: 1.1em;
  margin-right: 5px;
}
.photo {
  max-width: 300px;
}
.photo > .frame {
  display: none;
  margin-top: 10px;
  position: relative;
  height: auto;
  background-color: #FFFFFF;
  background-image: url(../images/game/15.png);
  background-repeat: no-repeat;
  background-position: center center;
  background-size: 30%;
  overflow: hidden;
}
.photo > .frame:before {
  content: "";
  display: block;
  padding-top: 87.46355685131195%;
}
.photo > .frame > .mask {
  position: absolute;
  width: 100%;
  z-index: 1;
  left: 0;
  top: 0;
}
.photo > .frame > .pic {
  position: absolute;
  left: 50%;
  top: 50%;
  -webkit-transform: translate(-50%, -50%);
  -moz-transform: translate(-50%, -50%);
  -o-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}
.photo > .frame .cropper-container {
  position: absolute;
  left: 50%;
  top: 50%;
  -webkit-transform: translate(-50%, -50%);
  -moz-transform: translate(-50%, -50%);
  -o-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}
.photo > .control {
  display: none;
  background-color: #FFF;
  text-align: center;
  -webkit-border-radius: 0 0 8px 8px;
  -webkit-background-clip: padding-box;
  -moz-border-radius: 0 0 8px 8px;
  -moz-background-clip: padding;
  border-radius: 0 0 8px 8px;
  background-clip: padding-box;
}
.photo > .control > a {
  display: inline-block;
  margin: 3% 4%;
}
.pic {
  position: relative;
  width: 300px;
  overflow: hidden;
}
.pic:before {
  content: "";
  display: block;
  padding-top: 87.46355685131195%;
}
.pic div {
  position: absolute;
  background-position: center center;
  background-size: cover;
  background-repeat: no-repeat;
  height: 100%;
  width: 100%;
  left: 0;
  top: 0;
}
.pic.star div:before {
  content: "";
  display: block;
  width: 100%;
  height: 100%;
  background-image: url(../images/testim/2.png);
  background-size: cover;
  position: absolute;
  left: 0;
  top: 0;
  z-index: 1;
}
</style>


<script type="text/javascript" src="../plugin/jquery/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="../plugin/jquery/jquery-migrate-1.2.1.min.js"></script>

<script type="text/javascript">
	function datacheck() {
		
			document.frmaddwom.submit();
		
	}
</script>
</head>
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td align="center" valign="top"><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<!-- Menu Box Start -->
				<?php require_once('menu.inc.php'); ?>
				<!-- Menu Box End -->
                </table></td>
			  </tr>
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<!-- Content Box Start -->
					<tr>
					  <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="10">&nbsp;</td>
							<td class="title"><?php echo ($frmAction == "edit")?"Edit":"Add"; ?> WOM</td>
							<td width="10">&nbsp;</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
							<td class="subtitle"></td>
							<td>&nbsp;</td>
						  </tr>
						</table></td>
					</tr>
					<tr>
					  <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td valign="top" width="10">&nbsp;</td>
							<td valign="top"></td>
							<td valign="top" width="10">&nbsp;</td>
						  </tr>
						  <tr>
							<td valign="top">&nbsp;</td>
							<td valign="top">&nbsp;</td>
							<td valign="top">&nbsp;</td>
						  </tr>
						</table></td>
					</tr>
					<tr>
					  <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td valign="top" width="10">&nbsp;</td>
							<td valign="top">
<form name="frmaddwom" method="post" enctype="multipart/form-data" >
<table width="1000" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td ><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr valign="top"> 
          <td width="150"><b>Member:</b></td>
          <td >
<?php 
if ($row["is_member"]=="1") {
	echo 'Yes';
} else if ($row["is_member"]=="0") {
	echo 'No';
}
?>
		  </td>
        </tr>
        <tr valign="top"> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td width="150"><b>Type:</b></td>
          <td >
<?php 
if ($row["parent_shareid"]<>"") {
	echo '接力<br><br>';
	echo '<a href="'.str_ireplace("hidden_adm_qazwsx/","",$cfg["root"])."wom_browse.php?shareid=".$row["parent_shareid"].'" target="_blank">'.str_ireplace("hidden_adm_qazwsx/","",$cfg["root"])."wom_browse.php?shareid=".$row["parent_shareid"].'</a>';
} else {
	echo '分享';
}
?>
		  </td>
        </tr>
        <tr valign="top"> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td width="150"><b>Select:</b></td>
          <td >
<?php 
if ($row["parent_shareid"]<>"") {
	if ($row["sharetype"]=="1") {
		echo '夢賞香港尖沙咀凱悅酒店自助餐';
	} else if ($row["sharetype"]=="2") {
		echo '夢賞“海迎灘Welcome Beach”家庭露營車體驗';
	} else if ($row["sharetype"]=="3") {
		echo '夢賞全家福';
	}
} else {
	if ($row["sharetype"]=="1") {
		echo 'A. 唔知點解小朋友有便秘/濕疹！食唔好、瞓唔到！夢想乜都做唔到！';
	} else if ($row["sharetype"]=="2") {
		echo 'B. 唔想小朋友有敏感，影響童年甚至一世，有無方法可預防？';
	} else if ($row["sharetype"]=="3") {
		echo 'C. 用盡方法小朋友都仲有敏感，好擔心，點算好？';
	}
}
?>
		  </td>
        </tr>
        <tr valign="top"> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td width="150"><b>Email:</b></td>
          <td >
<?php 
echo htmlencode($row["email"]);
?>
		  </td>
        </tr>
        <tr valign="top"> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td width="150"><b>Name:</b></td>
          <td >
<?php 
echo htmlencode($row["displayname"]);
?>
		  </td>
        </tr>
        <tr valign="top"> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td width="150"><b>Image:</b></td>
          <td >
<?php 
if ($row["pic"] != "") {
	echo '<img src="../storage/wom/'.$row["pic"].'" width="300">';
}
?>
		  </td>
        </tr>
        <tr valign="top"> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr valign="top"> 
          <td width="150"><b>Content:</b></td>
          <td >
<?php 
echo nl2br(htmlencode($row["content"]));
?>
		  </td>
        </tr>
        <tr valign="top"> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>

        <tr valign="top"> 
          <td><b>Status<font color=red>*</font> :</b></td>
          <td>
			<select name="is_active" size="1" style="width: auto;">
			<option value="0" <?php echo ($row["is_active"]=='0')?' selected':''?>>Not Approve</option>
			<option value="1" <?php echo ($row["is_active"]=='1')?' selected':''?>>Approve</option>
			</select>
		  </td>
        </tr>
        <tr valign="top"> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
 
        <tr valign="top"> 
          <td width="150"><b>Date:</b></td>
          <td >
<?php 
echo date("Y-m-d H:i:s",strtotime($row["adddate"]));
?>
		  </td>
        </tr>
        <tr valign="top"> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td >
<?php
if ($frmAction == "edit") {
?>
		<input type=hidden name="editid" value="<?php echo $lngEditID; ?>">
<?php
}
?>
		<input type=hidden name="frmaction" value="<?php echo $frmAction; ?>">
		<P>
		<INPUT type=button value="Save" name="save" onclick="javascript:datacheck();">&nbsp;
		<INPUT type=button value="Cancel" name="back" onclick="javascript:history.back();">
		</P>
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
	<td><font size="0">Fields marked with <FONT color=red>*</FONT> are mandatory</font></td>
  </tr>
</table>
</form>
							</td>
							<td valign="top" width="10">&nbsp;</td>
						  </tr>
						</table></td>
					</tr>
				<!-- Content Box End -->
                </table></td>
              </tr>
            </table></td>
        </tr>
		<!-- Footer Start -->
		<?php require_once('footer.php'); ?>
		<!--  Footer End -->
      </table></td>
  </tr>
</table>

<div id="uploading" style="display:none;width:100%;height:100%;background-color:#000000;position:fixed;top:0px;opacity: 0.7;filter: alpha(opacity=70);z-index:10000;">
<img src="../images/uploading.gif" width="100" style="position:absolute;top:50%;left:50%;margin-top:-50px;margin-left:-50px;">
</div>

<div style="visibility:hidden;">

<form name="uploadimage" id="uploadimage" method="post" action="../php/ajax_php_file.php" enctype="multipart/form-data" target="ajaxupload"></form>
<iframe name="ajaxupload" style="width:1px;height:1px;"></iframe>
<iframe name="process" id="game_process" style="width:1px;height:1px;"></iframe>
</div>


<link href="../plugin/cropper2/cropper.css" rel="stylesheet" type="text/css">
<script src="../plugin/cropper2/cropper.js"></script>

<script>
function readURL(input) {

	if (input.files && input.files[0]) {
		$('div.info div.intro').hide();
		$('#btns-upload').hide();
		$('div.info').show();
		$('div.info div.form').show();
		$('#btns-upload-again').show();
		$('#fbphoto').val('');
		
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#uploadpic').attr('src', e.target.result);

			$('div.photo > div.frame > img.pic').cropper('destroy').cropper({
				autoCropArea: 1,
				minCropBoxWidth: 500,
				minCropBoxHeight: 437,
				aspectRatio: 1 / 0.874,
				strict: false,
				guides: false,
				highlight: false,
				dragCrop: false,
				movable: false,
				resizable: false,
				  crop: function(data) {
					// Output the result data for cropping image.
					////console.log(data);
					$('#cropdata').val(JSON.stringify(data));
				  }
			});

			$('div.photo > div.frame').show();
			$('div.photo > div.control').show();
			$('div.photo > div.frame > img.mask').hide();
			
			$('.cropper-face').removeClass('cropper-invisible');
			
		}

		reader.readAsDataURL(input.files[0]);

		
		$('#uploadpic').css('visibility','');

	}
}

function readURLFB(pic) {
	$('#uploading').hide();

	$('#btns-upload').hide();
	$('#btns-upload-again').show();
	$('#photo').val("");
	$('#fbphoto').val(pic);

	$('#uploadpic').attr('src', pic);

	$('div.photo > div.frame > img.pic').cropper('destroy').cropper({
		autoCropArea: 1,
		minCropBoxWidth: 500,
		minCropBoxHeight: 437,
		aspectRatio: 1 / 0.874,
		strict: false,
		guides: false,
		highlight: false,
		dragCrop: false,
		movable: false,
		resizable: false,
		  crop: function(data) {
			// Output the result data for cropping image.
			////console.log(data);
			$('#cropdata').val(JSON.stringify(data));
		  }
	});

	$('div.photo > div.frame > img.pic').on("built.cropper", function(e) {
		$('div.photo > div.frame > img.pic').cropper('zoom', 0.5);
	});

	$('div.photo > div.frame').show();
	$('div.photo > div.control').show();
	$('div.photo > div.frame > img.mask').hide();
	
	$('.cropper-face').removeClass('cropper-invisible');
	

	$('#uploadpic').css('visibility','');

}

function uploadReset() {
	$('#uploading').hide();

	$('#photo').appendTo( $('#btn_upload_div') );
	
	$('div.photo > div.frame').hide();
	$('div.photo > div.control').hide();
	$('div.photo > div.frame > img.mask').hide();
	
	$('#uploadpic').css('visibility','hidden');
	
	$('.cropper-container').remove();
	
	$('#cropdata').val("");
	$('#photo').val("");
	$('#fbphoto').val("");
	
	$('#btns-upload-again').hide();
	$('#btns-upload').show();
	
}

$(document).ready(function(){
	
	$('#photo').on('click', function (e) {

	});
	
	$('#photo').on('change', function (e) {

		e.preventDefault();
		
		if ($('#photo').val()) {
			
			$('#uploading').show();
			$('#photo').appendTo( $('#uploadimage') );
			$("#uploadimage").submit();
			
		}

	});
		
	$('a.btn_upload_again').on('click', function (e) {
		e.preventDefault();
		
		uploadReset();

	});
	
	$('#btn_edit_image').on('click', function (e) {
		$('#btn_edit_image').hide();
		$('#div01').show();
		//readURLFB('<?php echo $display_ed_imglink; ?>');
	});
	
	$('.btn_toggle_mask').on('click', function (e) {
		if ($('div.photo').hasClass("nomask")) {
			$('div.photo').removeClass("nomask");
		} else {
			$('div.photo').addClass("nomask");
		}
	});
	
});	

$(window).load(function(){

	uploadReset();

});

</script>

</body>
</html>
