<?php
define('_VALID_INCLUDE', TRUE); // flag to allow include or require files
$dir_level = "../"; //set the required files located

require_once($dir_level.'includes/vars.inc.php');
require_once($dir_level.'includes/common.inc.php');

// Prevent caching, HTTP/1.1
header("Cache-Control: no-cache, must-revalidate");
// Prevent caching, HTTP/1.0
header("Pragma: no-cache");
// Force charset
header("Content-type: text/html; charset=".$cfg['charset']);


if ($_SESSION[$cfg['prefix_session'].'a_username'] !="") {
	header("Location: main.php");
	exit();
}

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $cfg['charset']; ?>">
<title><?php echo $cfg['site_name']; ?> - Report</title>
<meta name="keywords" content="<?php echo $cfg['meta_keywords']; ?>">
<meta name="description" content="<?php echo $cfg['meta_description']; ?>">
<meta name="generator" content="<?php echo $cfg['meta_generator']; ?>">
<meta name="robots" content="noindex, nofollow, noarchive">
<meta name="MSSmartTagsPreventParsing" content="TRUE">
<meta http-equiv="MSThemeCompatible" content="Yes">
<link href="images/global.css" rel="stylesheet" type="text/css">
</head>
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td align="center" valign="top"><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td height="90" class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="30">&nbsp;</td>
                <td class="title"><?php echo $cfg['site_name']; ?> - Report</td>
                <td width="30">&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td class="subtitle"></td>
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top" width="30">&nbsp;</td>
                <td valign="top"><table width="400" border="0" cellspacing="0" cellpadding="0">
				<!-- Login Box Start -->
				<form action="login.inc.php" method="post" name="frm_login">
				  <tr>
					<td colspan="2" class="contentB">&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan="2" class="contentB"><img src="images/trans.gif" width="1" height="10"></td>
				  </tr>
				  <tr>
					<td width="60" class="contentB">Username :</td>
					<td width="140" class="contentB"><input type="text" name="username" class="inpStyle" maxlength="255" /></td>
				  </tr>
				  <tr>
					<td colspan="2"><img src="images/trans.gif" width="1" height="10"></td>
				  </tr>
				  <tr>
					<td class="contentB">Password :</td>
					<td class="contentB"><input type="password" name="password" class="inpStyle" maxlength="255" /></td>
				  </tr>
				  <tr>
					<td colspan="2" valign="top"><img src="images/trans.gif" width="1" height="20"></td>
				  </tr>
				  <tr>
					<td class="contentB">&nbsp;</td>
					<td class="contentB"><input name="login" id="login" type="submit" value="Submit" class="btnStyle"></td>
				  </tr>
				  <tr>
					<td class="contentB">&nbsp;</td>
					<td class="contentB">&nbsp;</td>
				  </tr>
				</form>
				<!-- Login Box End -->
                  </table></td>
                <td valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td valign="top">&nbsp;</td>
                <td valign="top">&nbsp;</td>
                <td valign="top">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
		<!-- Footer Start -->
		<?php require_once('footer.php'); ?>
		<!--  Footer End -->
      </table></td>
  </tr>
</table>
</body>
</html>
