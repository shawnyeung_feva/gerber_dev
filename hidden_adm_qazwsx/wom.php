<?php
define('_VALID_INCLUDE', TRUE); // flag to allow include or require files
$dir_level = "../"; //set the required files located

require_once($dir_level."includes/vars.inc.php");
require_once($dir_level."includes/common.inc.php");

checkadmin(); // require admin

// open db connection
$$conn = openConnection($$conn);

if ( $_POST['sys_action'] == "deleteSconfirm" AND $_POST['id'] != ""){	 
	include_once("wom.inc.php");
}

// Prevent caching, HTTP/1.1
header("Cache-Control: no-cache, must-revalidate");
// Prevent caching, HTTP/1.0
header("Pragma: no-cache");
// Force charset
header("Content-type: text/html; charset=".$cfg['charset']);



?>
<html>
<head>
<title><?php echo $cfg['site_name']; ?> - CMS</title>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $cfg['charset']; ?>">
<meta name="keywords" content="<?php echo $cfg['meta_keywords']; ?>">
<meta name="description" content="<?php echo $cfg['meta_description']; ?>">
<meta name="generator" content="<?php echo $cfg['meta_generator']; ?>">
<meta name="robots" content="noindex, nofollow, noarchive">
<meta name="MSSmartTagsPreventParsing" content="TRUE">
<meta http-equiv="MSThemeCompatible" content="Yes">
<link href="images/global.css" rel="stylesheet" type="text/css">
<script type="text/javascript">

function changepage(page) {
	document.frmWorkplace.action = page;
	document.frmWorkplace.submit();
}

function formdelete(page) {
	document.frmWorkplace.id.value = "";

	if (confirm("Are you sure you want to delete the selected record?")) {
	
		document.frmWorkplace.sys_action.value = "deleteSconfirm";
		document.frmWorkplace.action = page;
		
		var box;
		var j = 0;
		
		box = eval("document.frmWorkplace.chk" + j); 
		
		while (box != null) {

			if (box != null && box.checked == true) {
				document.frmWorkplace.id.value = document.frmWorkplace.id.value + eval("document.frmWorkplace.id" + j + ".value")+",";
			}
			
			j = j + 1;		
			box = eval("document.frmWorkplace.chk" + j); 
				
		}
		
		
		document.frmWorkplace.submit();
	}
}

function checkOrUncheckAll(formname) {
box = eval("document." + formname + ".chkall"); 
if (box.checked == false)
	uncheckAll(formname);
else
	checkAll(formname);
}

function checkAll(formname) {
	
	var box;
	var j = 0;
	
	box = eval("document." + formname + ".chk" + j); 
	
	while (box != null) {

		if (box != null && box.checked == false) box.checked = true;
		
		j = j + 1;
		box = eval("document." + formname + ".chk" + j); 
   }
}

function uncheckAll(formname) {
	
	var box;
	var j = 0;
	
	box = eval("document." + formname + ".chk" + j); 
	
	while (box != null) {

		if (box != null && box.checked == true) box.checked = false;
		
		j = j + 1;
		box = eval("document." + formname + ".chk" + j); 
   }
}
</script>
</head>
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td align="center" valign="top"><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<!-- Menu Box Start -->
				<?php require_once('menu.inc.php'); ?>
				<!-- Menu Box End -->
                </table></td>
			  </tr>
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<!-- Content Box Start -->
					<tr>
					  <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="10">&nbsp;</td>
							<td class="title">WOM</td>
							<td width="10">&nbsp;</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
							<td class="subtitle"></td>
							<td>&nbsp;</td>
						  </tr>
						</table></td>
					</tr>
					<tr>
					  <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td valign="top" width="10">&nbsp;</td>
							<td valign="top"></td>
							<td valign="top" width="10">&nbsp;</td>
						  </tr>
						  <tr>
							<td valign="top">&nbsp;</td>
							<td valign="top">&nbsp;</td>
							<td valign="top">&nbsp;</td>
						  </tr>
						</table></td>
					</tr>
					<tr>
					  <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td valign="top" width="10">&nbsp;</td>
							<td valign="top">
<?php
$pagesize = 30;
$page = "wom.php?1=1";
$submitpage = "wom.php?1=1";

$sorting = trim(htmlencode($_GET["sorting"]));
$pageno = trim(htmlencode($_GET["selpageno"]));

$displayname = trim(htmlencode($_GET["displayname"]));
   
if ($pageno == "") {
	$pageno = 1;
}



// open db connection
$$conn = openConnection($$conn);

// sql
// get total record
$sql = "SELECT count(*)/".$pagesize." AS page_count, count(*) AS record_count ";
$sql .= "FROM nanpro_wom_share where 1 ";
if ($displayname != "") {
	$sql.= " and `displayname` like ('%".$displayname."%') ";
}
//$sql.= " and status <> '-1' ";

$result = mysql_query($sql,$$conn) or die (mysql_error($$conn));
if ($row=mysql_fetch_array($result)) {

	$page_count = ceil($row["page_count"]);
	$record_count = $row["record_count"];

} else {
	$page_count = 1;
	$record_count = 0;
}

$totcount = $record_count;
$pagecount = ceil($totcount/$pagesize);

mysql_free_result($result);

$sql = " select * from nanpro_wom_share where 1 ";
if ($displayname != "") {
	$sql.= " and `displayname` like ('%".$displayname."%') ";
}
//$sql.= " and status <> '-1' ";

if ($sorting != "") {
	$sql.= " order by ".$sorting." ";
} else {
	$sql.= " order by adddate desc, shareid desc ";
}

$sql.= " limit ".($pageno-1)*$pagesize.", ".$pagesize." ";

$result = mysql_query($sql, $$conn);
$num = mysql_num_rows($result);

?>
<form name="frmWorkplace" method="post">
<a href="womXLS.php?displayname=<?php echo $displayname; ?>" target="_blank">Download Approved WOM XLS</a>
<br>
<br>
NAME: <input type="text" maxlength="255" name="displayname" id="displayname" value="<?php echo $displayname; ?>" style="width:200px;" value="">&nbsp;&nbsp;<input type="button" value="Search" onclick="javascript:window.location = '<?php echo $page; ?>&displayname='+encodeURIComponent(document.getElementById('displayname').value)">
<br>
<br>
<table width="1300" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr valign="middle" height="25" >
          <td width="8%">
		  <!--<a href="addwom.php?frmaction=add">ADD</a>-->
		  </td>
          <td width="2%">
		  <!--<a href="javascript:formdelete('<?php echo $submitpage; ?>');">DELETE</a>-->
		  </td>    
          <td width="20%">
		  <!--<a href="editwomsort.php">SORTING STAR BABY</a>-->
		  </td>          
		  <td width="15%" align="right"><?php if ($pageno > 1) { ?>
			<a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=<?php echo $sorting; ?>&selpageno=<?php if ($pageno ==  1) {
															echo $pageno;
														} else {
															echo ($pageno - 1);
														} ?>"><</a><?php } ?></td>          
		  <td width="10%" align="right">Page&nbsp;</td>          
		  <td width="10%" >
		    <select name="selpageno" onchange="javascript:window.location = '<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=<?php echo $sorting; ?>&selpageno='+this.value">
<?php
	for ($i = 1; $i <= $pagecount ; $i++) {
?>
		<option value=<?php echo $i; ?><?php if  ($pageno == $i) { echo " selected"; } ?>><?php echo $i?></option>
<?php
	}
?>
</select></td>          
		  <td width="15%"><?php if ($pageno < $pagecount) { ?>
		    <a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=<?php echo $sorting; ?>&selpageno=<?php if ($pageno ==  $pagecount) { 
															echo $pageno; 
														} else {
															echo ($pageno + 1);
														} ?>">></a><?php } ?></td>          
		  <td width="20%" align="right"><b>Total : <?php echo $totcount; ?></b></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="1" cellpadding="2">
        <tr bgcolor="#CCCCCC" > 
          <!--<td width="30"><div align="center"><INPUT type=checkbox name=chkall onClick="javascript:checkOrUncheckAll('frmWorkplace')"></div></td>-->
          <td width="50" class="tableline"><font color="#990000">ID</font>&nbsp;<a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=shareid asc&selpageno=<?php echo $pageno; ?>"><img src="images/down2.gif" border="0"></a><a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=shareid desc&selpageno=<?php echo $pageno; ?>"><img src="images/up2.gif" border="0"></a></td>
          <td width="100" class="tableline"><font color="#990000">MEMBER</font>&nbsp;<a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=is_member asc&selpageno=<?php echo $pageno; ?>"><img src="images/down2.gif" border="0"></a><a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=is_member desc&selpageno=<?php echo $pageno; ?>"><img src="images/up2.gif" border="0"></a></td>
          <td width="100" class="tableline"><font color="#990000">TYPE</font>&nbsp;<a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=parent_shareid asc&selpageno=<?php echo $pageno; ?>"><img src="images/down2.gif" border="0"></a><a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=parent_shareid desc&selpageno=<?php echo $pageno; ?>"><img src="images/up2.gif" border="0"></a></td>
          <td width="350" class="tableline"><font color="#990000">SELECT</font>&nbsp;<a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=sharetype asc&selpageno=<?php echo $pageno; ?>"><img src="images/down2.gif" border="0"></a><a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=sharetype desc&selpageno=<?php echo $pageno; ?>"><img src="images/up2.gif" border="0"></a></td>
          <td width="250" class="tableline"><font color="#990000">EMAIL</font>&nbsp;<a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=email asc&selpageno=<?php echo $pageno; ?>"><img src="images/down2.gif" border="0"></a><a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=email desc&selpageno=<?php echo $pageno; ?>"><img src="images/up2.gif" border="0"></a></td>
          <td width="150" class="tableline"><font color="#990000">NAME</font>&nbsp;<a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=displayname asc&selpageno=<?php echo $pageno; ?>"><img src="images/down2.gif" border="0"></a><a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=displayname desc&selpageno=<?php echo $pageno; ?>"><img src="images/up2.gif" border="0"></a></td>
          <td width="100" class="tableline"><font color="#990000">STATUS</font>&nbsp;<a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=is_active asc&selpageno=<?php echo $pageno; ?>"><img src="images/down2.gif" border="0"></a><a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=is_active desc&selpageno=<?php echo $pageno; ?>"><img src="images/up2.gif" border="0"></a></td>
          <td width="150" class="tableline"><font color="#990000">DATE</font>&nbsp;<a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=adddate asc&selpageno=<?php echo $pageno; ?>"><img src="images/down2.gif" border="0"></a><a href="<?php echo $page; ?>&displayname=<?php echo $displayname; ?>&sorting=adddate desc&selpageno=<?php echo $pageno; ?>"><img src="images/up2.gif" border="0"></a></td>
		  <td width="150" class="tableline"><font color="#990000">ACTION</font></td>
		</tr>
<?php
$totcurrent = 0;


while ($row=mysql_fetch_array($result)) {

	$display_status = '';

?>
        <tr   bgcolor="#E8E8E8" onmouseover="this.style.background='#CCFFFF'" onmouseout="this.style.background='#E8E8E8'"> 
          <!--<td><div align="center"><INPUT type=checkbox name="chk<?php echo $totcurrent; ?>"><input type=hidden name="id<?php echo $totcurrent; ?>" value="<?php echo $row["shareid"]; ?>"></div></td>-->
		  <TD><?php echo $row["shareid"]; ?></TD>
		  <TD><?php 
			if ($row["is_member"]=="1") {
				echo 'Yes';
			} else if ($row["is_member"]=="0") {
				echo 'No';
			}
		  ?></TD>
		  <TD><?php 
			if ($row["parent_shareid"]<>"") {
				echo '接力';
			} else {
				echo '分享';
			}
		  ?></TD>
		  <TD><?php 
			if ($row["parent_shareid"]<>"") {
				if ($row["sharetype"]=="1") {
					echo '夢賞香港尖沙咀凱悅酒店自助餐';
				} else if ($row["sharetype"]=="2") {
					echo '夢賞“海迎灘Welcome Beach”家庭露營車體驗';
				} else if ($row["sharetype"]=="3") {
					echo '夢賞全家福';
				}
			} else {
				if ($row["sharetype"]=="1") {
					echo 'A. 唔知點解小朋友有便秘/濕疹！食唔好、瞓唔到！夢想乜都做唔到！';
				} else if ($row["sharetype"]=="2") {
					echo 'B. 唔想小朋友有敏感，影響童年甚至一世，有無方法可預防？';
				} else if ($row["sharetype"]=="3") {
					echo 'C. 用盡方法小朋友都仲有敏感，好擔心，點算好？';
				}
			}
		  ?></TD>
		  <TD><?php echo $row["email"]; ?></TD>
		  <TD><?php echo $row["displayname"]; ?></TD>
		  <TD><?php 
			if ($row["is_active"]=="1") {
				echo '<font color="#00FF00">Approve</font>';
			} else if ($row["is_active"]=="0") {
				echo '<font color="#FF0000">Not Approve</font>';
			}
		  ?></TD>
		  <TD><?php echo date("Y-m-d H:i:s",strtotime($row["adddate"])); ?></TD>
		  <TD>
		  <a href="addwom.php?editid=<?php echo $row["shareid"]; ?>&frmaction=edit" >Edit</a>
		  </TD>
        </tr>
<?php

$totcurrent = $totcurrent + 1;
}

?>
<input type=hidden name=sorting value="<?php echo $sorting; ?>">
<input type=hidden name=totcurrent value="<?php echo $totcurrent; ?>">
<input type=hidden name=sys_action value="">
<input type=hidden name=id value="">
<input type=hidden name="workplaceid" value="<?php echo $lngWorkplaceID; ?>">
      </table></td>
  </tr>
</table>
</form>
							</td>
							<td valign="top" width="10">&nbsp;</td>
						  </tr>
						</table></td>
					</tr>
				<!-- Content Box End -->
                </table></td>
              </tr>
            </table></td>
        </tr>
		<!-- Footer Start -->
		<?php require_once('footer.php'); ?>
		<!--  Footer End -->
      </table></td>
  </tr>
</table>
</body>
</html>
<?php

// close db connection
$$conn = closeConnection($$conn);

?>