<?php
define('_VALID_INCLUDE', TRUE); // flag to allow include or require files
$dir_level = "../"; //set the required files located

require_once($dir_level.'includes/vars.inc.php');
require_once($dir_level.'includes/common.inc.php');

// Prevent caching, HTTP/1.1
header("Cache-Control: no-cache, must-revalidate");
// Prevent caching, HTTP/1.0
header("Pragma: no-cache");
// Force charset
header("Content-type: text/html; charset=".$cfg['charset']); 


if ($_POST) {

	$username = $_POST['username'];
	$password = $_POST['password'];

	if ($username == $cfg['admin_id'] AND $password == $cfg['admin_password']) {

		$_SESSION[$cfg['prefix_session'].'a_id'] = "1";
		$_SESSION[$cfg['prefix_session'].'a_username'] = $username;
		$_SESSION[$cfg['prefix_session'].'a_role'] = "1";

		header("Location: main.php");
		exit();

	} else {
?>
<script type="text/javascript">
	alert("Invalid Username/Password!");
</script>
<?php
	}
}

?>
<script type="text/javascript">
	window.location='index.php';
</script>