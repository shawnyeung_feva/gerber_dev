<?php
define('_VALID_INCLUDE', TRUE); // flag to allow include or require files
$dir_level = "../"; //set the required files located

require_once($dir_level.'includes/vars.inc.php');
require_once($dir_level.'includes/common.inc.php');

checkadmin(); // require admin

if ($_POST) {
	include_once("edittestimonialsort.inc.php");
}

// Force charset
header("Content-type: text/html; charset=".$cfg['charset']);
?>
<html>
<head>
<title><?php echo $cfg['site_name']; ?> - CMS</title>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $cfg['charset']; ?>">
<meta name="keywords" content="<?php echo $cfg['meta_keywords']; ?>">
<meta name="description" content="<?php echo $cfg['meta_description']; ?>">
<meta name="generator" content="<?php echo $cfg['meta_generator']; ?>">
<meta name="robots" content="noindex, nofollow, noarchive">
<meta name="MSSmartTagsPreventParsing" content="TRUE">
<meta http-equiv="MSThemeCompatible" content="Yes">
<link href="images/global.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
function datachecksort() {
	var i;
	for (i=0;i<document.frmAddSort.elements['sorting[]'].length;i++) {
		document.frmAddSort.elements['sorting[]'].options[i].selected = true;
	}
	document.frmAddSort.submit();
}

function goup() {
	var index, stext, svalue;
	index = document.frmAddSort.elements['sorting[]'].selectedIndex;
	if (index > 0) {
		svalue = document.frmAddSort.elements['sorting[]'].options[index].value;
		stext = document.frmAddSort.elements['sorting[]'].options[index].text;
		document.frmAddSort.elements['sorting[]'].options[index].value = document.frmAddSort.elements['sorting[]'].options[index-1].value
		document.frmAddSort.elements['sorting[]'].options[index].text = document.frmAddSort.elements['sorting[]'].options[index-1].text
		document.frmAddSort.elements['sorting[]'].options[index-1].value = svalue
		document.frmAddSort.elements['sorting[]'].options[index-1].text = stext
		document.frmAddSort.elements['sorting[]'].selectedIndex = index - 1;
	}
}

function godown() {
	var index, stext, svalue;
	index = document.frmAddSort.elements['sorting[]'].selectedIndex;
	if (index > -1) {
		if (index < document.frmAddSort.elements['sorting[]'].length -1) {
			svalue = document.frmAddSort.elements['sorting[]'].options[index].value;
			stext = document.frmAddSort.elements['sorting[]'].options[index].text;
			document.frmAddSort.elements['sorting[]'].options[index].value = document.frmAddSort.elements['sorting[]'].options[index+1].value
			document.frmAddSort.elements['sorting[]'].options[index].text = document.frmAddSort.elements['sorting[]'].options[index+1].text
			document.frmAddSort.elements['sorting[]'].options[index+1].value = svalue
			document.frmAddSort.elements['sorting[]'].options[index+1].text = stext
			document.frmAddSort.elements['sorting[]'].selectedIndex = index + 1;
		}
	}
}
</script>
</head>
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td align="center" valign="top"><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<!-- Menu Box Start -->
				<?php require_once('menu.inc.php'); ?>
				<!-- Menu Box End -->
                </table></td>
			  </tr>
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<!-- Content Box Start -->
					<tr>
					  <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="10">&nbsp;</td>
							<td class="title">Home Key Visual Sorting</td>
							<td width="10">&nbsp;</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
							<td class="subtitle"></td>
							<td>&nbsp;</td>
						  </tr>
						</table></td>
					</tr>
					<tr>
					  <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td valign="top" width="10">&nbsp;</td>
							<td valign="top"></td>
							<td valign="top" width="10">&nbsp;</td>
						  </tr>
						  <tr>
							<td valign="top">&nbsp;</td>
							<td valign="top">&nbsp;</td>
							<td valign="top">&nbsp;</td>
						  </tr>
						</table></td>
					</tr>
					<tr>
					  <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td valign="top" width="10">&nbsp;</td>
							<td valign="top">
<!-- Page Content Start -->
<form name="frmAddSort" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr valign="top"> 
          <td  width="150">Item :</td>
          <td  >
			<select  name="sorting[]" style="WIDTH: auto; HEIGHT: 225px" multiple>
<?php

	// open db connection
	$$conn = openConnection($$conn);

	$sql = " select * from `gerberfev_game_201710` where 1 ";
	$sql.= " and type='1' ";
	$sql.= " and status<>'-1' ";
	$sql.= " order by sorting desc, created_date desc, id desc ";
	


	$result = mysql_query($sql, $$conn);
	$num = mysql_num_rows($result);
	
	while ($row = mysql_fetch_array($result)) {

?>
					<OPTION value=<?php echo $row['id']; ?>><?php echo $row['bb_name']; ?>&nbsp;<?php if ($row['status'] == "1") { ?>&nbsp;(Approve)<?php } else if ($row['status'] == "2") { ?>&nbsp;(Not Approve)<?php } ?></OPTION>
<?php

	}
	
	// close db connection
	$$conn = closeConnection($$conn);
	

?>
            </select></td>
        </tr>
        <tr valign="top"> 
          <td>&nbsp;</td>
          <td ><A href="javascript:goup();">Up</a> <A href="javascript:godown();">Down</a>&nbsp;</td>
        </tr>

  <tr>
    <td class="A12">

	  <INPUT class="btn" type="button" value="Save" name="save" onclick="javascript:datachecksort();">&nbsp;
	  
	  <INPUT class="btn" type="button" value="Cancel" name="cancel" onclick="javascript:window.location='testimonial.php';">
	</td>
  </tr>

</table>
</form>
<!-- Page Content End -->
							</td>
							<td valign="top" width="10">&nbsp;</td>
						  </tr>
						</table></td>
					</tr>
				<!-- Content Box End -->
                </table></td>
              </tr>
            </table></td>
        </tr>
		<!-- Footer Start -->
		<?php require_once('footer.php'); ?>
		<!--  Footer End -->
      </table></td>
  </tr>
</table>
</body>
</html>