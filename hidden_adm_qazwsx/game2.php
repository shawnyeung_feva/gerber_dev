<?php
define('_VALID_INCLUDE', TRUE); // flag to allow include or require files
$dir_level = "../"; //set the required files located

require_once($dir_level."includes/vars.inc.php");
require_once($dir_level."includes/common.inc.php");

checkadmin(); // require admin

// Prevent caching, HTTP/1.1
header("Cache-Control: no-cache, must-revalidate");
// Prevent caching, HTTP/1.0
header("Pragma: no-cache");
// Force charset
header("Content-type: text/html; charset=".$cfg['charset']);
?>
<html>
<head>
<title><?php echo $cfg['site_name']; ?> - CMS</title>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $cfg['charset']; ?>">
<meta name="keywords" content="<?php echo $cfg['meta_keywords']; ?>">
<meta name="description" content="<?php echo $cfg['meta_description']; ?>">
<meta name="generator" content="<?php echo $cfg['meta_generator']; ?>">
<meta name="robots" content="noindex, nofollow, noarchive">
<meta name="MSSmartTagsPreventParsing" content="TRUE">
<meta http-equiv="MSThemeCompatible" content="Yes">
<link href="images/global.css" rel="stylesheet" type="text/css">
<link href="images/dtree.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/dtree.js"></script>
<script type="text/javascript">

function formshows() {
	document.frmWorkplace.id.value = "";

	if (confirm("Are you sure you want to show the selected record?")) {
	
		document.frmWorkplace.sys_action.value = "showconfirm";
		document.frmWorkplace.action = '';
		
		var box;
		var j = 0;
		
		box = eval("document.frmWorkplace.chk" + j); 
		
		while (box != null) {

			if (box != null && box.checked == true) {
				document.frmWorkplace.id.value = document.frmWorkplace.id.value + eval("document.frmWorkplace.id" + j + ".value")+",";
			}
			
			j = j + 1;		
			box = eval("document.frmWorkplace.chk" + j); 
				
		}
		
		
		document.frmWorkplace.submit();
		
		document.getElementById("body").innerHTML = '<div style="text-align:center;"><br><br><br><br><br>Processing...</div>';
	}
}

function formhides() {
	document.frmWorkplace.id.value = "";

	if (confirm("Are you sure you want to hide the selected record?")) {
	
		document.frmWorkplace.sys_action.value = "hideconfirm";
		document.frmWorkplace.action = '';
		
		var box;
		var j = 0;
		
		box = eval("document.frmWorkplace.chk" + j); 
		
		while (box != null) {

			if (box != null && box.checked == true) {
				document.frmWorkplace.id.value = document.frmWorkplace.id.value + eval("document.frmWorkplace.id" + j + ".value")+",";
			}
			
			j = j + 1;		
			box = eval("document.frmWorkplace.chk" + j); 
				
		}
		
		
		document.frmWorkplace.submit();
		
		document.getElementById("body").innerHTML = '<div style="text-align:center;"><br><br><br><br><br>Processing...</div>';
	}
}

function formshow(tid) {
	document.frmWorkplace.id.value = "";

	//if (confirm("Are you sure you want to show the selected record?")) {
	
		document.frmWorkplace.sys_action.value = "showSconfirm";
		document.frmWorkplace.action = '';
		
		document.frmWorkplace.id.value = tid;
		
		document.frmWorkplace.submit();
		
		document.getElementById("body").innerHTML = '<div style="text-align:center;"><br><br><br><br><br>Processing...</div>';
	//}
}

function formhide(tid) {
	document.frmWorkplace.id.value = "";

	//if (confirm("Are you sure you want to hide the selected record?")) {
	
		document.frmWorkplace.sys_action.value = "hideSconfirm";
		document.frmWorkplace.action = '';
		
		document.frmWorkplace.id.value = tid;
		
		document.frmWorkplace.submit();
		
		document.getElementById("body").innerHTML = '<div style="text-align:center;"><br><br><br><br><br>Processing...</div>';
	//}
}

function formdelete(tid) {
	document.frmWorkplace.id.value = "";

	if (confirm("确认删除?")) {
	
		document.frmWorkplace.sys_action.value = "deleteSconfirm";
		document.frmWorkplace.action = '';
		
		document.frmWorkplace.id.value = tid;
		
		document.frmWorkplace.submit();
	}
}

function checkOrUncheckAll(formname) {
	box = eval("document." + formname + ".chkall"); 
	if (box.checked == false)
		uncheckAll(formname);
	else
		checkAll(formname);
}

function checkAll(formname) {
	
	var box;
	var j = 0;
	
	box = eval("document." + formname + ".chk" + j); 
	
	while (box != null) {

		if (box != null && box.checked == false) box.checked = true;
		
		j = j + 1;
		box = eval("document." + formname + ".chk" + j); 
   }
}

function uncheckAll(formname) {
	
	var box;
	var j = 0;
	
	box = eval("document." + formname + ".chk" + j); 
	
	while (box != null) {

		if (box != null && box.checked == true) box.checked = false;
		
		j = j + 1;
		box = eval("document." + formname + ".chk" + j); 
   }
}
</script>
</head>
<body id="body">
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td align="center" valign="top"><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<TD valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<!-- Menu Box Start -->
				<?php require_once('menu.inc.php'); ?>
				<!-- Menu Box End -->
                </table></td>
			  </tr>
              <tr>
                <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<!-- Content Box Start -->
					<tr>
					  <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="10">&nbsp;</td>
							<td class="title">Game Record List</td>
							<td width="10">&nbsp;</td>
						  </tr>
						  <tr>
							<TD valign="top">&nbsp;</td>
							<td class="subtitle"></td>
							<TD valign="top">&nbsp;</td>
						  </tr>
						</table></td>
					</tr>
					<tr>
					  <td class="tl_bg"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td valign="top" width="10">&nbsp;</td>
							<td valign="top">
<?php
$pagesize = 20;
$page = "game2.php?1=1";
$submitpage = "game2.php?1=1";

$sorting = $_GET["sorting"];
$pageno = $_GET["selpageno"];
   
if ($pageno == "") {
	$pageno = 1;
}

$page_prev = $pageno-1;
$page_next = $pageno+1;



// filter query
$filter_sql="";

// open db connection
$conn = openConnection($conn);

// sql
// get total record
$sql = "SELECT count(*)/".$pagesize." AS page_count, count(*) AS record_count ";
$sql .= "FROM ";
$sql .= "( ";
$sql .= "SELECT * FROM `nanprobb_game` WHERE 1 ";
//$sql .= "AND status='1' ";

$sql .= $filter_sql;

$sql .= ") tabletemp ";

$result = mysql_query($sql,$conn);
if ($row=mysql_fetch_array($result)) {

	$page_count = ceil($row["page_count"]);
	$record_count = $row["record_count"];

} else {
	$page_count = 1;
	$record_count = 0;
}

$totcount = $record_count;
$pagecount = ceil($totcount/$pagesize);

if ($pageno > $pagecount) {
	$pageno = $pagecount;
}

mysql_free_result($result);

// query sql
$sql = "SELECT * FROM `nanprobb_game` WHERE 1 ";
//$sql .= "AND status='1' ";

$sql .= $filter_sql;

if ($sorting != "") {

	$sql.= " order by ".$sorting." ";

} else {
	$sql.= " order by `created_date` desc ";
}

$sql.= " limit ".($pageno-1)*$pagesize.", ".$pagesize." ";

$result = mysql_query($sql,$conn);
$num = mysql_num_rows($result);

?>
<form name="frmWorkplace" method="post">
<table width="1100" border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td valign="top" align="right">
	</td>
  </tr>
  <tr>
    <TD valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr valign="middle" >
          <td width="32%">

		  </td>   
		  <td width="13%" align="right">
<?php
	if($page_prev > 0) {
?>
			<a href="<?php echo $page; ?>&sorting=<?php echo $sorting; ?>&selpageno=<?php if ($pageno ==  1) {
															echo $pageno;
														} else {
															echo ($pageno - 1);
														} ?>">PREV</a>
<?php		
	} else {
		//print "Prev";
		//print "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
	}	
?>
		  </td>          
		  <td width="10%" align="center">Page  
		    <select name="selpageno" onchange="javascript:window.location = '<?php echo $page; ?>&sorting=<?php echo $sorting; ?>&selpageno='+this.value">
<?php
	for ($i = 1; $i <= $pagecount ; $i++) {
?>
		<option value=<?php echo $i; ?><?php if  ($pageno == $i) { echo " selected"; } ?>><?php echo $i?></option>
<?php
	}
?>
</select></td>          
		  <td width="13%">
<?php	
	if($page_next <= $pagecount) {
?>
		    <a href="<?php echo $page; ?>&sorting=<?php echo $sorting; ?>&selpageno=<?php if ($pageno ==  $pagecount) { 
															echo $pageno; 
														} else {
															echo ($pageno + 1);
														} ?>">NEXT</a>
<?php
	} else {
		//print "Next";
		//print "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
	}
?>
		  </td>          
		  <td width="32%" align="right">Total : <?php echo $totcount; ?></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <TD valign="top">&nbsp;</td>
  </tr>
  <tr>
    <TD valign="top"><table width="100%" border="1" cellspacing="1" cellpadding="2" style='table-layout:fixed'>
        <tr bgcolor="#CCCCCC" > 
		  <td width="50" class="tableline"><font color="#990000">ID</font>&nbsp;</td>
		  <td width="200" class="tableline"><font color="#990000">Name</font>&nbsp;</td>
		  <td width="100" class="tableline"><font color="#990000">BB Date</font>&nbsp;</td>
		  <td width="50" class="tableline"><font color="#990000">Gender</font>&nbsp;</td>
		  <td width="100" class="tableline"><font color="#990000">Model Type</font>&nbsp;</td>
          <td width="200" class="tableline"><font color="#990000">Orginal Image</font>&nbsp;</td>
          <td width="200" class="tableline"><font color="#990000">Result Image</font>&nbsp;</td>
          <td width="200" class="tableline"><font color="#990000">Model Image</font>&nbsp;</td>
          <td width="150" class="tableline"><font color="#990000">Date</font>&nbsp;</td>
          <td width="50" class="tableline"><font color="#990000">Status</font>&nbsp;</td>

        </tr>
<?php
$totcurrent = 0;


while ($row=mysql_fetch_array($result)) {

?>
<?php
if ($_GET["sel"] == $row["id"]) {
?>
  <tr   bgcolor="#FFD800" onmouseover="this.style.background='#CCFFFF'" onmouseout="this.style.background='#FFD800'"> 
<?php
} else {
?>
  <tr   bgcolor="#E8E8E8" onmouseover="this.style.background='#CCFFFF'" onmouseout="this.style.background='#E8E8E8'"> 
<?php
}
?>
		  <TD valign="top"><a name="<?php echo $row["id"]; ?>"></a><?php echo $row["id"]; ?></TD>
		  <TD valign="top"><?php echo $row["name"]; ?></TD>
		  <TD valign="top"><?php echo $row["bb_year"]."-".$row["bb_month"].($row["bb_day"]==""?"":"-".$row["bb_day"]); ?></TD>
		  <TD valign="top"><?php echo $row["gender"]; ?></TD>
		  <TD valign="top"><?php echo $row["gender"]."_".$row["type"]; ?></TD>
          <TD valign="top"><a href="<?php echo "../".$row["org_image"]; ?>" target="_blank"><img src="<?php echo "../".$row["org_image"]; ?>" width="200" border="0"></a></TD>
          <TD valign="top"><?php if ($row["result_image"] != "") { ?><a href="<?php echo "../".$row["result_image"]; ?>" target="_blank"><img src="<?php echo "../".$row["result_image"]; ?>" width="200" border="0"></a><?php } ?></TD>
          <TD valign="top"><?php if ($row["result_image"] != "") { ?><a href="<?php echo "http://fevahost3.fevaworks.com/nestle_imageprocess/upload/con_".$row["gender"]."_".$row["type"].".jpg"; ?>" target="_blank"><img src="<?php echo "http://fevahost3.fevaworks.com/nestle_imageprocess/upload/con_".$row["gender"]."_".$row["type"].".jpg"; ?>" width="200" border="0"></a><?php } ?></TD>
		  <TD valign="top"><?php echo $row["created_date"]; ?></TD>
		  <TD valign="top"><?php 
			if ($row["status"] == "1") {
				echo "<span style='color:#66CC33;'>Success</span>";
			} else if ($row["status"] == "2") {
				echo "<span style='color:#0066FF;'>Fail</span>";
			}
		  ?></TD>
        </tr>
<?php

$totcurrent = $totcurrent + 1;
}

?>
<input type=hidden name=sorting value="<?php echo $sorting; ?>">
<input type=hidden name=totcurrent value="<?php echo $totcurrent; ?>">
<input type=hidden name=sys_action value="">
<input type=hidden name=id value="">
<input type=hidden name=pageno value="<?php echo $pageno; ?>">
      </table></td>
  </tr>
</table>
</form>
							</td>
							<td valign="top" width="10">&nbsp;</td>
						  </tr>
						</table></td>
					</tr>
				<!-- Content Box End -->
                </table></td>
              </tr>
            </table></td>
        </tr>
		<!-- Footer Start -->
		<?php require_once('footer.php'); ?>
		<!--  Footer End -->
      </table></td>
  </tr>
</table>
</body>
</html>
<?php

// close db connection
$conn = closeConnection($conn);

?>