<?php
set_time_limit(3600);
define('_VALID_INCLUDE', TRUE); // flag to allow include or require files
$dir_level = "../"; //set the required files located

require_once($dir_level.'includes/vars.inc.php');
require_once($dir_level.'includes/common.inc.php');

checkadmin(); // require admin

$pagetitle = "WOM";

$pagesize = 99999999;

$sorting = trim(htmlencode($_GET["sorting"]));
$pageno = trim(htmlencode($_GET["selpageno"]));

$displayname = trim(htmlencode($_GET["displayname"]));
   
if ($pageno == "") {
	$pageno = 1;
}

// fix for IE catching or PHP bug issue
header("Pragma: public");
header("Expires: 0"); // set expiration time
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
// browser must download file from server instead of cache

// force download dialog
header("Content-Type: application/vnd.ms-excel;charset='utf-8'");

// use the Content-Disposition header to supply a recommended filename and
// force the browser to display the save dialog.
header("Content-Disposition: attachment; filename=wom_".date("Y_m_d_H_i_s").".xls;");

?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
                xmlns:x="urn:schemas-microsoft-com:office:excel"
                xmlns="http://www.w3.org/TR/REC-html40">
<head>
        <meta http-equiv=Content-Type content="text/html; charset=<?php echo $cfg['charset']; ?>">
        <!--[if gte mso 9]><xml>
        <x:ExcelWorkbook>
        <x:ExcelWorksheets>
                   <x:ExcelWorksheet>
                   <x:Name></x:Name>
                   <x:WorksheetOptions>
                                   <x:DisplayGridlines/>
                   </x:WorksheetOptions>
                   </x:ExcelWorksheet>
        </x:ExcelWorksheets>
        </x:ExcelWorkbook>
        </xml><![endif]-->
</head>
<body>
<?php

// filter query
$filter_sql="";

// open db connection
$$conn = openConnection($$conn);

$sql = " SELECT * FROM `nanpro_wom_share` WHERE 1 ";
$sql.= " AND `is_active` = '1' ";
if ($displayname != "") {
	$sql.= " and `displayname` like ('%".$displayname."%') ";
}
$sql.= " order by adddate desc, shareid desc ";

$result = mysql_query($sql, $$conn);
$num = mysql_num_rows($result);

$totcount = $num;
$pagecount = ceil($totcount/$pagesize);

if ($pagecount < 1) {
	$pagecount = 1;
}

?>
<table width="3300" border="0" cellspacing="0" cellpadding="0">
	<tr height="17" style="height:12.75pt">
		<td><b><?php echo $pagetitle; ?></b></td>
	</tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr > 
		  <td width="50" >ID</td>
		  <td width="100" >MEMBER</td>
		  <td width="100" >TYPE</td>
		  <td width="350" >SELECT</td>
		  <td width="250" >EMAIL</td>
		  <td width="150" >NAME</td>
		  <td width="500" >CONTENT</td>
		  <td width="100" >COMMENT COUNT</td>
          <td width="100" >STATUS</td>
          <td width="150" >DATE</td>
		  <td width="50" >姓氏</td>
		  <td width="100" >名字</td>
		  <td width="100" >電話</td>
		  <td width="100" >地區</td>
		  <td width="100" >分區</td>
		  <td width="400" >通訊地址</td>
          <td width="200" >電郵地址</td>
          <td width="100" >懷孕狀態</td>
          <td width="100" >BB的出生日期</td>
          <td width="200" >BB的出生證明書</td>
        </tr>
<?php
$totcurrent = 0;


while ($row=mysql_fetch_array($result)) {

?>
        <tr> 
		  <TD align="left" style='mso-number-format:"\@";' ><?php echo $row["shareid"]; ?></TD>
		  <TD align="left"><?php 
			if ($row["is_member"]=="1") {
				echo 'Yes';
			} else if ($row["is_member"]=="0") {
				echo 'No';
			}
		  ?></TD>
		  <TD align="left"><?php 
			if ($row["parent_shareid"]<>"") {
				echo '接力';
			} else {
				echo '分享';
			}
		  ?></TD>
		  <TD align="left"><?php 
			if ($row["parent_shareid"]<>"") {
				if ($row["sharetype"]=="1") {
					echo '夢賞香港尖沙咀凱悅酒店自助餐';
				} else if ($row["sharetype"]=="2") {
					echo '夢賞“海迎灘Welcome Beach”家庭露營車體驗';
				} else if ($row["sharetype"]=="3") {
					echo '夢賞全家福';
				}
			} else {
				if ($row["sharetype"]=="1") {
					echo 'A. 唔知點解小朋友有便秘/濕疹！食唔好、?唔到！夢想乜都做唔到！';
				} else if ($row["sharetype"]=="2") {
					echo 'B. 唔想小朋友有敏感，影響童年甚至一世，有無方法可預防？';
				} else if ($row["sharetype"]=="3") {
					echo 'C. 用盡方法小朋友都仲有敏感，好擔心，點算好？';
				}
			}
		  ?></TD>
		  <TD align="left"><?php echo $row["email"]; ?></TD>
		  <TD align="left"><?php echo $row["displayname"]; ?></TD>
		  <TD align="left"><?php echo ($row["content"]); ?></TD>
		  <TD align="left"><?php 
		  if ($row["parent_shareid"]<>"") {
			  
		  } else {
			echo $row["commentcount"]; 
		  }
		  ?></TD>
		  <TD align="left"><?php 
			if ($row["is_active"]=="1") {
				echo 'Approve';
			} else if ($row["is_active"]=="0") {
				echo 'Not Approve';
			}
		  ?></TD>
		  <TD align="left"><?php echo date("Y-m-d H:i:s",strtotime($row["adddate"])); ?></TD>
<?php
if ($row["non_memberid"] != "") {
	

	$sql2 = " SELECT * FROM `nanpro_non_member` WHERE 1 ";
	$sql2.= " AND `id` = '".$row["non_memberid"]."' ";


	$result2 = mysql_query($sql2, $$conn);
	$row2=mysql_fetch_array($result2);

?>
			<TD align="left"><?php echo ($row2["name_last"]); ?></TD>
			<TD align="left"><?php echo ($row2["name_first"]); ?></TD>
			<TD align="left"style='mso-number-format:"\@";' ><?php echo "+".$row2["area_code"]."-".$row2["mobile_phone"]; ?></TD>
			<TD align="left"><?php 
if ($row2["area"] == "Hong Kong") {
	echo "香港"; 
} else 
if ($row2["area"] == "Kowloon") {
	echo "九龍"; 
} else 
if ($row2["area"] == "New Territories") {
	echo "新界"; 
} else 
if ($row2["area"] == "Outlying Island") {
	echo "離島"; 
} else 
if ($row2["area"] == "Macau") {
	echo "澳門"; 
} else 
if ($row2["area"] == "China") {
	echo "中國"; 
} else 
if ($row2["area"] == "Others") {
	echo "其他"; 
}			
			?></TD>
			<TD align="left"><?php 

	$sql3 = " SELECT `name_TC` FROM `salesforce_district` WHERE 1 ";
	$sql3.= " AND `id` = '".$row2["district"]."' ";


	$result3 = mysql_query($sql3, $$conn);
	$row3=mysql_fetch_array($result3);

			echo ($row3["name_TC"]); 
			
			?></TD>
			<TD align="left"><?php echo ($row2["address"]); ?></TD>
			<TD align="left"><?php echo ($row2["email"]); ?></TD>
			<TD align="left"><?php if ($row2["pregent"] == "1") {
				echo "懷孕中";
			} else if ($row2["havechild"] == "1") {
				echo "已有孩子";
			}
				?></TD>
			<TD align="left"><?php echo ($row2["pregnancy_year"]."-".$row2["pregnancy_month"]."-".$row2["pregnancy_day"]); ?></TD>
			<TD align="left"><?php if ($row2["cert"] != "") { echo str_replace("hidden_adm_qazwsx","storage/birth_cert",$cfg["root"]).($row2["cert"]); } ?></TD>
<?php
} else {
?>
			<TD align="left"></TD>
			<TD align="left"></TD>
			<TD align="left"></TD>
			<TD align="left"></TD>
			<TD align="left"></TD>
			<TD align="left"></TD>
			<TD align="left"></TD>
			<TD align="left"></TD>
			<TD align="left"></TD>
			<TD align="left"></TD>
<?php
}
?>
        </tr>
<?php

$totcurrent = $totcurrent + 1;
}

?>

      </table></td>
  </tr>
</table>
</body>
</html>
<?php

// close db connection
$$conn = closeConnection($$conn);

?>