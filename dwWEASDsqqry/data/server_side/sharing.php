<?php session_start();?>
<?php
if(!isset($_SESSION["user"])){
    header('Location: '.'login.php');
}
 ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
	<title>Questionnaire Records</title>
	<link rel="stylesheet" type="text/css" href="../../media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../resources/syntax/shCore.css">
	<link rel="stylesheet" type="text/css" href="../resources/demo.css">
	<link rel="stylesheet" type="text/css" href="../../media/css/buttons.dataTables.min.css">
	<style type="text/css" class="init">

	</style>
	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.3.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="../../media/js/jquery.dataTables.js">
	</script>
	<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/1.10.11/sorting/chinese-string.js">
	</script>
	<script type="text/javascript" language="javascript" src="../resources/syntax/shCore.js">
	</script>
	<script type="text/javascript" language="javascript" src="../resources/demo.js">
	</script>
	<script type="text/javascript" language="javascript" src="scripts/dataTables.buttons.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="scripts/buttons.flash.min.js">
	</script>

	<script type="text/javascript" language="javascript" src="scripts/jszip.min.js">
	</script>

	<script type="text/javascript" language="javascript" src="scripts/pdfmake.min.js">
	</script>

	<script type="text/javascript" language="javascript" src="scripts/vfs_fonts.js">
	</script>

	<script type="text/javascript" language="javascript" src="scripts/buttons.html5.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="scripts/buttons.print.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="scripts/dataTables.select.min.js">
	</script>
  <!-- Frame Buster -->
  <script type="text/javascript">
  if (top.location != self.location) {
  top.location = self.location.href;
  }
  </script>

	<script type="text/javascript" language="javascript" class="init">
		$(document).ready(function() {
			$('#example').DataTable({
				"bJQueryUI": true,
				"dom": 'Bfrtip',
				"lengthMenu": [
					[10, 25, 50, -1],
					['10 rows', '25 rows', '50 rows', 'Show all']
				],
				"buttons": [{
					extend: 'pageLength',
				}, {
					extend: 'excel'
				}],
				select: true,
				"aoColumns": [{
					"mData": 'id',
					"sTitle": "ID",
					"bSortable": true
				}, {
					"mData": 'page',
					"sTitle": "page",
					"bSortable": true
				}, {
					"mData": 'fbook',
					"sTitle": "fbook",
					"bSortable": true
				}, {
					"mData": 'twi',
					"sTitle": "twi",
					"bSortable": true
				}, {
					"mData": 'google',
					"sTitle": "google",
					"bSortable": true
        }, {
          "mData": 'link',
          "sTitle": "link",
          "bSortable": true
        }, {
          "mData": 'whatsapp',
          "sTitle": "whatsapp",
          "bSortable": true
				}],
				"oLanguage": {
					"sProcessing": "處理中...",
					"sLengthMenu": "顯示 _MENU_ 項結果",
					"sZeroRecords": "沒有匹配結果",
					"sInfo": "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
					"sInfoEmpty": "顯示第 0 至 0 項結果，共 0 項",
					"sInfoFiltered": "(從 _MAX_ 項結果過濾)",
					"sSearch": "搜索:",
					"oPaginate": {
						"sFirst": "首頁",
						"sPrevious": "上頁",
						"sNext": "下頁",
						"sLast": "尾頁"
					}
				},
				/*
				 * aLengthMenu
				 * 允许用户选择每页显示多少条记录
				 */

				"sAjaxSource": "json3.php"
			});

		});
	</script>
  <style>
    table td{
      max-width: 400px !important;
      word-break: break-all;
    }
  </style>
</head>

<body class="dt-example">
	<div class="container">
		<section>
			<br>
			<a href="report.php">Contact Message Page </a> / <a href="questionnaire.php">Questionnaire Page </a> / <a href="logout.php">Logout</a>
			<br><br>
			<table id="example" class="display" cellspacing="0" width="100%">

			</table>
		</section>
	</div>

</body>

</html>
