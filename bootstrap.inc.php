<?php
/* check valid include */
defined('_VALID_INCLUDE') or die('Direct access not allowed.');

$_REAL_BASE_DIR = realpath(dirname(__FILE__)); // filesystem path of this file's directory (config.php)


// forward slash to backward slash
if (version_compare(PHP_VERSION, '5.2.6', '<')) {
   $_REAL_BASE_DIR = preg_replace('/(\/){2,}|(\\\){1,}/','/', $_REAL_BASE_DIR);
}

if(!defined('PHP_REAL_BASE_DIR') OR (PHP_REAL_BASE_DIR != $_REAL_BASE_DIR)) {

	define('PHP_REAL_BASE_DIR', $_REAL_BASE_DIR);

}

?>