    // A $( document ).ready() block.
    //
    $(document).ready(function() {
        $("#contactus").submit(function() {
            var contactType = $("#contact-type option:selected").val();
            var surname = $('#surname').val();
            var firstName = $('#first-name').val();
            var emaillAddress = $('#email-address').val();
            var message = $('#message').val();
            var disagree = 0;
            if($('#disagree').is(':checked'))
              disagree = 1;
            // reset all
            $('.error').hide();
            $('.error-checkbox').hide();
            // checking
            if (!contactType || contactType == '請選擇') {
                scrollToDiv('#contact-type', '(請選擇查詢類別)');
                console.log(123);
            } else if (!firstName) {
                scrollToDiv('#first-name', '(不是有效的名字)');
                console.log(11);
            } else if (!isEmail(emaillAddress)) {
                scrollToDiv('#email-address', '(不是有效的電郵地址)');
            } else if (!message) {
                scrollToDiv('#message', '(請輸入您的訊息)');
            } else if (!$('#agreement').is(':checked')) {
                $('.error-checkbox').text('(請勾選同意)');
                $('.error-checkbox').css('display','block');
                scrollToEl('#agreement');
            } else {
                // everything find , go
                // ajax call
                $.ajax({
                    type: "POST",
                    url: "api/contactus",
                    data: { 'messagetype': contactType, 'surname': surname, 'firstName': firstName, 'emaillAddress': emaillAddress, 'message' : message, 'disagree' : disagree},
                    cache: false,
                    success: function(data)
                        {
                          if(data == '1'){
                            //alert('信息已成功發送');
                            //location.reload();
                            $('#thankyou').trigger('click');
                          }
                          else
                            alert('Server error, please try again later.');
                        },
                    error: function(xhr, status, error) {
                      alert(error);
                    }
                    });
            }
            return false;
        });





        // scroll to div and format error message
        function scrollToDiv(el, errorMsg) {
            $('html,body').animate({
                scrollTop: $(el).offset().top - 200
            });
            $(el).focus();
            $(el).parent().children('.error').text(errorMsg);
            $(el).parent().children('.error').css('display','block');
        }
        // scroll to div
        function scrollToEl(el) {
            $('html,body').animate({
                scrollTop: $(el).offset().top - 200
            });
        }
        // email va
        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

    });
