var ratingLearn = 0;
var ratingBuy = 0;
var ipAddress = '';

$(document).ready(function() {
    //$('#questionnaire').modal('show');
    //
    //
    //
    //
    $('.learn-more.mobileOff>a').addClass('hvr-fade');
    $('.product-img>.img-responsive').removeClass('hvr-buzz-out');
    $('.lSNext .fa').addClass('hvr-grow');
    $('.lSPrev .fa').addClass('hvr-grow');

    $('.container>.row>.header-type>map>area').each(function(index, obj) {
        var currentHeader = $('.header-type').children('.mobileOff').attr('src');
        $(this).mouseover(function() {
            //console.log(index);
            switch (index) {
                case 5:
                    changeBg('stage1full.png');
                    break;
                case 6:
                    changeBg('stage2full.png');
                    break;
                case 7:
                    changeBg('stage3full.png');
                    break;
                case 8:
                    changeBg('stage4full.png');
                    break;
            }
        });
        $(this).mouseout(function() {
          $('.header-type').children('.mobileOff').attr('src',currentHeader);
        });
    });

    function changeBg($bg){
      $('.header-type').children('.mobileOff').attr('src','../images/menu-tab/desktop/'+$bg);
      //console.log('../images/menu-tab/desktop'+$bg);
    }

    $('<span class="right-hidden mobileOff"></span>').appendTo('.lSSlideWrapper');

    apis = $('.scroll-pane').jScrollPane({
        verticalDragMinHeight: 140,
        verticalDragMaxHeight: 140
    });
    $('.scroll-pane-arrows').jScrollPane({
        showArrows: true,
        horizontalGutter: 10,
    });

    $(".load-more").on("click", function() {
        $('.hidden-div').fadeIn('slow');
        $(this).hide();
    });

    $('<span class="shadow mobileOff"></span>').appendTo('.jspContainer');

    $('map').imageMapResize();


    var autoplaySlider = $('#lightSlider').lightSlider({
        item: 4,
        auto: true,
        loop: false,
        pauseOnHover: true,
        onBeforeSlide: function(el) {
            $('#current').text(el.getCurrentSlideCount());
        },
        onAfterSlide: function(el) {
          var sliderLength = $('#lightSlider>li').length;
          $('#lightSlider>li').each(function(index, obj) {
            if (parseInt($(window).width()) > 568) {
              if(index == 0){
                if($(this).hasClass('active')){
                  $('.lSPrev').hide();
                }else{
                  $('.lSPrev').show();
                }
              }
              if(index == (sliderLength - 4)){
                if($(this).hasClass('active')){
                  $('.lSNext').hide();
                }else{
                  $('.lSNext').show();
                }
              }
            }else{
              //console.log(index);
              if(index == 0){
                if($(this).hasClass('active')){
                  $('.lSPrev').hide();
                }else{
                  $('.lSPrev').show();
                }
              }
              if(index == sliderLength-1){
                if($(this).hasClass('active')){
                  $('.lSNext').hide();
                }else{
                  $('.lSNext').show();
                }
              }
            }

          });
        },
        responsive: [
          {
            breakpoint: 568,
            settings: {
                item: 1,
                slideMove: 1,
                slideMargin: 6,
            }
          }
        ]
    });

    //autoplaySlider.refresh;
    //
    $(window).resize(function() {
      var sliderLength = $('#lightSlider>li').length;
      if( $(window).width() > 568 ){
        autoplaySlider.refresh;
        if(sliderLength <= 4){
          $('.lSNext').hide();
        }
      }else{
        autoplaySlider.refresh;
        if(sliderLength > 1){
          $('.lSNext').show();
        }
      }
    });
    var sliderLength = $('#lightSlider>li').length;
    $('.lSPrev').hide();
    if(sliderLength <= 4 && $(window).width() > 568){
      $('.lSNext').hide();
    }

    $('#total').text(autoplaySlider.getTotalSlideCount());

    $(".lSPrev").bind("click", function() {
        autoplaySlider.goToPrevSlide();
    });
    $(".lSNext").bind("click", function() {
        autoplaySlider.goToNextSlide();
    });






});
