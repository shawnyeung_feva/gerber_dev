window.fbAsyncInit = function() {
  FB.init({
    appId      : '244729375896622',
    xfbml      : true,
    version    : 'v2.6'
  });
};


(function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));



function  socialOpen (url, width, height) {
      /*
       var msie6 = (navigator.userAgent.match(/msie/i) && navigator.userAgent.match(/6/));
       if (msie6 == true) {
       width = screen.width;
       height = screen.height;
       }*/

      var screenX = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft,
              screenY = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop,
              outerWidth = typeof window.outerWidth != 'undefined' ? window.outerWidth : document.body.clientWidth,
              outerHeight = typeof window.outerHeight != 'undefined' ? window.outerHeight : (document.body.clientHeight - 22),
              left = parseInt(screenX + ((outerWidth - width) / 2), 10),
              top = parseInt(screenY + ((outerHeight - height) / 2.5), 10),
              features = (
                      'width=' + width +
                      ',height=' + height +
                      ',left=' + left +
                      ',top=' + top +
                      ',resizable=' + 0
                      );
      tmp_obj = window.open(url, '_blank', features);
      if (window.focus) {
          tmp_obj.focus();
      }
      return false;
  };

$(document).ready(function(){

        $(".share li>a.pop").on('click', function() {
          var name = $(this).attr("name");
          var params = {};



          switch (name) {
              case "fbook":
                  url = "https://www.facebook.com/sharer/sharer.php?";
                  params.u = window.location.href;
                  /*params.t = "";
                   params.pic = "";*/
                  params.width = 598;
                  params.height = 369;
                  $.getJSON("https://api.facebook.com/method/links.getStats?urls="+  window.location.href +"&format=json", function(data) {
                    sharing(window.location.href,'fbook', '');
                  });
                  break;
              case "twi":
                  url = "https://twitter.com/intent/tweet?";
                  params.text = $("title").text() + " " + window.location.href;
                  params.width = 598;
                  params.height = 440;
                  sharing(window.location.href,'twi','');
                  break;
              case "tsina":
                  url = "http://service.weibo.com/share/share.php?";
                  params.url = window.location.href;
                  params.title = $("title").text();
                  /*params.appkey = ""
                   params.searchPic = "";*/
                  params.width = 632;
                  params.height = 635;
                  break;
              case "tqq":
                  url = "http://share.v.t.qq.com/index.php?";
                  params.c = "share";
                  params.a = "index";
                  params.url = window.location.href;
                  params.title = $("title").text();
                  /*params.appkey = "";*/
                  params.width = 640;
                  params.height = 354;
                  break;
              case "qzone":
                  url = "http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?";
                  params.url = window.location.href;
                  params.title = $("title").text();
                  /*params.desc = "";
                   params.summary = "";
                   params.site = "";*/
                  params.width = 682;
                  params.height = 613;
                  break;
              case "renren":
                  url = "http://widget.renren.com/dialog/share?";
                  params.resourceUrl = window.location.href;
                  params.srcUrl = window.location.href;
                  params.title = $("title").text();
                  params.width = 598;
                  params.height = 484;
                  break;
              case "douban":
                  url = "http://www.douban.com/share/service?";
                  params.href = window.location.href;
                  params.name = $("title").text();
                  /*params.text = "";*/
                  params.width = 642;
                  params.height = 364;
                  break;
              case "google":
                  url = "https://plus.google.com/share?";
                  params.url = window.location.href;
                  //params.name = $("title").text();
                  params.width = 642;
                  params.height = 364;
                  /* Social Share: Google Plus JSON */
                  var data = {
                      "method":"pos.plusones.get",
                      "id":window.location.href,
                      "params":{
                          "nolog":true,
                          "id":window.location.href,
                          "source":"widget",
                          "userId":"@viewer",
                          "groupId":"@self"
                      },
                      "jsonrpc":"2.0",
                      "key":"p",
                      "apiVersion":"v1"
                  };
                  $.ajax({
                      type: "POST",
                      url: "https://clients6.google.com/rpc",
                      processData: true,
                      contentType: 'application/json',
                      data: JSON.stringify(data),
                      success: function(r){
                        //console.log(r.result.metadata.globalCounts.count);
                        sharing(window.location.href,'google',r.result.metadata.globalCounts.count);
                          //$('#googleplus_page_share_count').text(format(r.result.metadata.globalCounts.count));
                      }

                  });
                  break;
          }
          url = url + $.param(params);
          socialOpen(url, params.width, params.height);
      });

      function sharing(page,type,number){
        $.ajax({
            type: "POST",
            url: baseUrl+"api/sharingupdate",
            data: { 'page': page, 'type': type, 'number': number},
            cache: false,
            success: function(data)
                {
                  //console.log(data);
                },
            error: function(xhr, status, error) {
              alert(error);
            }
            });
      }

});
