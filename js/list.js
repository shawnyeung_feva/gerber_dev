    $(document).ready(function() {
        // 1:1 item
        resizeHeight();
        window.onresize = function(event) {
            resizeHeight();
        };


        $('.supported-root').waypoint(function() {

        }, {
            offset: '50%'
        });

    });

    function btnCall(e) {

        if ($(window).width() <= 767) {
            var el = $(e).attr('data-child');
            el = '.' + el;
            el = $(el);
            if (el.hasClass('fadeIn')) {
                $(e).css('margin-bottom', '0px');
                $(e).removeClass('active-arrow');
                $(e).children('.arrow').children('.fa-angle-up').addClass('fa-angle-down');
                $(e).children('.arrow').children('.fa-angle-up').removeClass('fa-angle-up');
                el.hide();
                el.removeClass('fadeIn');
            } else {
                el.addClass('fadeIn');
                el.fadeIn();
                $(e).css('margin-bottom', '10px');
                $(e).children('.arrow').children('.fa-angle-down').addClass('fa-angle-up');
                $(e).children('.arrow').children('.fa-angle-down').removeClass('fa-angle-down');
            }
        }

        resizeHeight();
    }

    function resizeHeight() {
        var cw = $('.item').width();
        //console.log(cw);
        $('.item').css({
            'height': cw + 'px'
        });
        //console.log($(window).width());
        if ($(window).width() <= 750) {
            $('.mobileRemove').removeClass('col-2');
            //$('.wow').addClass('wow-removed').removeClass('fadeIn').removeClass('wow');

        } else {
            $('.mobileRemove').addClass('col-2');
            //$('.wow-removed').addClass('wow').addClass('fadeIn').removeClass('wow-removed');
        }
    }
