// Activates Tooltips for Social Links

window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, "script", "twitter-wjs"));

var is_safari = 'false';
if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {var is_safari = true;}



$(document).ready(function() {

  shareEffect();
  $( window ).resize(function() {
    shareEffect();
  });

  function shareEffect(){
    if (parseInt($(window).width()) > 568) {
      $('.share>li>a').addClass('hvr-fade');
    }else{
      $('.share>li>a').removeClass('hvr-fade');
    }
  }




  $(document).on("click", '.watsapp', function() {
        sharing(window.location.href,'whatsapp','');
        var text = $('title').text();
        var url = window.location.href;
        var message = encodeURIComponent(text) + " - " + encodeURIComponent(url);
        var whatsapp_url = "whatsapp://send?text=" + message;
        window.location.href = whatsapp_url;

    });

  $('#btn').attr('data-clipboard-text',window.location.href);
  $('#mb-share').val(decodeURIComponent(window.location.href));
  $('#cur').attr('value',window.location.href);
  var btn = document.getElementById('btn');
  var clipboard = new Clipboard(btn);
  clipboard.on('success', function(e) {
      sharing(window.location.href,'link','');
      //console.log(e);
      $('#linkcopy').trigger('click');
      if(is_safari === true){
        $('#btn-mb').trigger('click');
      }
  });

  /*
  $('#btn-mb').on('click', function() {
      $('#safaricopy').modal('show');
  });*/




  $('#btn-mb').magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,

    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom',
    callbacks : {
      open : function(){
        sharing(window.location.href,'link','');
      }
    }
  });

  $('#thankyou').magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,

    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom',
    callbacks : {
      close : function(){
        location.reload();
      }
    }
  });

  $('#linkcopy').magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,

    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom'
  });


    // Activates the Carousel
    $('.carousel').carousel({
        interval: 5000
    });

    $("#myCarousel").swiperight(function() {
        $("#myCarousel").carousel('prev');
    });
    $("#myCarousel").swipeleft(function() {
        $("#myCarousel").carousel('next');
    });

    $('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function() {
        $(this).toggleClass('open');
    });


    // menu open and hide
    $('.navbar-ex1-collapse').on('show.bs.collapse', function() {
        $('#menu-overlay').fadeIn('Slow');
        $('.navbar').addClass("border-class");
    })
    $('.navbar-ex1-collapse').on('hidden.bs.collapse', function() {
        $('#menu-overlay').fadeOut('Slow');
        $('.navbar').removeClass("border-class");
    })



    // rating
    $('input[name=learn]').change(function () {
       var me = $(this);
       ratingLearn = me.attr('value');
     });
     $('input[name=buy]').change(function () {
        var me = $(this);
        ratingBuy = me.attr('value');
      });



      /*
    $.getJSON("http://jsonip.com/?callback=?", function(data) {
        //console.log(data.ip);
        ipAddress = data.ip;
    });*/
    $.getJSON(baseUrl+"api/getip", function(data) {
        //console.log(data.ip);
        //ipAddress = data.ip;
        ipAddress = data;
    });

    function show_popup(){
      //if(document.cookie.indexOf("qaYes=Yes") == -1)
        //$('#questionnaire').modal('show');
    };
    setTimeout(function(){
      //show_popup();
    },30000);

    //console.log(document.cookie.indexOf("qaYes=Yes"));
    //$('#questionnaire').modal('show');

    //createCookie('qaYes','Yes',30);

    function createCookie(name, value, days) {
        var date, expires;
        if (days) {
            date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    $("#question").submit(function() {
      var buyBefore = $("input[name=buybefore]:checked").val();
      //var chooseBrand = $("input[name=brand]:checked").val();
      var otherBrand = $("#message-text").val();
      var chooseBrand = '';

      if(otherBrand === ''){
        alert('請輸入品牌名稱.');
      }else {
        /*
        $.ajax({
            type: "POST",
            url: baseUrl+"api/questionnaire",
            data: { 'buyBefore': buyBefore, 'chooseBrand': chooseBrand, 'ratingLearn': ratingLearn, 'ratingBuy': ratingBuy, 'otherBrand' : otherBrand, 'ipAddress' : ipAddress},
            cache: false,
            success: function(data)
                {
                  //console.log(data);
                  if(data == '1'){
                    createCookie('qaYes','Yes',30);
                    $('#questionnaire').modal('hide');
                    $('#thankyou').trigger('click');
                    //alert('信息已成功發送');
                    //location.reload();
                  }
                  else
                    alert('Server error, please try again later.');
                },
            error: function(xhr, status, error) {
              alert(error);
            }
            }); */
      }
        return false;
    });


    function sharing(page,type,number){
      $.ajax({
          type: "POST",
          url: baseUrl+"api/sharingupdate",
          data: { 'page': page, 'type': type, 'number': number},
          cache: false,
          success: function(data)
              {
                //console.log(data);
              },
          error: function(xhr, status, error) {
          //  alert(error);
          }
          });
    }

});
