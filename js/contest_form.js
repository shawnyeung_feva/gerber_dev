var submitting = false;

function checkBB() {
	
	var d1 = new Date();
	d1.setMonth(d1.getMonth() - 6);
	
	var d2 = new Date();
	d2.setMonth(d2.getMonth() - 48);
	
	var testyear = $('#bb_year').val();
	var testmonth = $('#bb_month').val();
	var testday = $('#bb_day').val();
	
	if (testyear == "" || testmonth == "") {
		return true;
	}
	
	if (testday == "") {
		testday = 1;
	}
	
	var testd = new Date(testyear, testmonth-1, testday);
	
	if (testd > d1) {

		return true;
	}
	
	if (testd < d2) {
		return true;
	}

	return false;
}

function shareFB(url) {

	var u = url;
	var popup = window.open('http://facebook.com/sharer.php?u='+encodeURIComponent(u),'Facebook','toolbar=0,status=0,width=575,height=436');

}

$(document).ready(function() {

    if($.browser.msie){
        $(".notIE").remove();
      
        $('section#step2 input[type="file"]').change(function() {
            if ($(this).val()) {
                var filename = $(this).val();
                $(this).closest('.file-upload').find('.file-name').html(filename);
            }
        });
    
    } else {
        $(".IE").remove();
    }

    var kv_swiper = new Swiper('section#kv .swiper-container', {
        pagination: 'section#kv .swiper-pagination',
        paginationClickable: true,
        spaceBetween: 30,
        autoPlay: 4000,
        effect: "fade",
        simulateTouch: false,
        breakpoints: {
            991:{
                simulateTouch: true
            }
        }
    });

    $("#step1 .box").on("click",function(){
        $("#step1 .box").removeClass("active");
        $(this).addClass("active");
		$("#photo_type").val($(this).attr("data-val"));
    });

    $("#step4 .box").on("click",function(){
        $("#step4 .box").removeClass("active");
        $(this).addClass("active");
		
		if ($(this).attr("data-val") == 1) {
			$("#newmember").hide();
			$("#oldmember").show();
		} else
		if ($(this).attr("data-val") == 2) {
			$("#oldmember").hide();
			$("#newmember").show();
		}
    });    

    $('section#step4 input[type="file"]').change(function() {
        if ($(this).val()) {
            var filename = $(this).val();
            $(this).closest('.file-upload').find('.file-name').html(filename);
        }
    });

	$('.tnc a').magnificPopup({
		type: 'inline',
		preloader: false
	});
    
    $("#pop-tnc .pop-close").on("click",function(){
        $.magnificPopup.close(); 
    });
    
    var homepage_swiper = new Swiper('#upload .swiper-container', {
        paginationClickable: true,
        spaceBetween: 30,
        freeMode: true,
        autoPlay: 4000,
        slidesPerView: 4,
        slidesPerColumn: 2,
        breakpoints: {
            567: {
                slidesPerView: 2,
                slidesPerColumn: 2,
                spaceBetween: 10
            },
            991: {
                spaceBetween: 10
            }
        }
    });

    var cat_1_swiper = new Swiper('#cat1.cat.swiper-container', {
        paginationClickable: true,
        slidesPerView: 1,
        spaceBetween: 30,
        freeMode: true,
        autoPlay: 4000,
        breakpoints: {
            567: {
                slidesPerView: 'auto',
                spaceBetween: 30
            },
            991: {
                slidesPerView: 'auto',
                spaceBetween: 10
            }
        }
    });
    var cat_2_swiper = new Swiper('#cat2.cat.swiper-container', {
        paginationClickable: true,
        slidesPerView: 1,
        spaceBetween: 30,
        freeMode: true,
        autoPlay: 4000,
        breakpoints: {
            567: {
                slidesPerView: 'auto',
                spaceBetween: 30
            },
            991: {
                slidesPerView: 'auto',
                spaceBetween: 10
            }
        }
    });
    var cat_3_swiper = new Swiper('#cat3.cat.swiper-container', {
        paginationClickable: true,
        slidesPerView: 1,
        spaceBetween: 30,
        freeMode: true,
        autoPlay: 4000,
        breakpoints: {
            567: {
                slidesPerView: 'auto',
                spaceBetween: 30
            },
            991: {
                slidesPerView: 'auto',
                spaceBetween: 10
            }
        }
    });

	
	$("#step1 button").on("click", function(e) {
		e.preventDefault();
		
		if(!submitting) {
			submitting = true;
			
			if ($("#photo_type").val() == "") {
				submitting = false;
				alert("請選擇參賽組別");
			} else {
				submitting = false;
				$("#step1").hide();
				$("html,body").scrollTop(0);
			}
			
			
		}
		
	});
	
	$("#step3 button").on("click", function(e) {
		e.preventDefault();
		
		if(!submitting) {
			submitting = true;
			
			if ($("#upload").val() == "") {
				submitting = false;
				alert("請上傳圖片");
			} else 
			if ($("#bb_name").val() == "") {
				submitting = false;
				alert("請填寫參賽寶寶姓名");
			} else 
			if ($("#bb_year").val() == "") {
				submitting = false;
				alert("請填寫參賽寶寶出生日期 - 年");
			} else 
			if ($("#bb_month").val() == "") {
				submitting = false;
				alert("請填寫參賽寶寶出生日期 - 月");	
			} else 
			if ($("#bb_day").val() == "") {
				submitting = false;
				alert("請填寫參賽寶寶出生日期 - 日");	
			} else 
			if ($("#hkid").val() == "") {
				submitting = false;
				alert("請填寫香港身份證號碼");	
			} else 
			if (!$('#hkid').val().match(/^[0-9]{4}$/i)) {
				submitting = false;
				alert("請填寫正確香港身份證號碼");	
			} else {
				submitting = false;
				$("#step2").hide();
				$("#step3").hide();
				$("#step4 .box").eq(0).click();
				$("html,body").scrollTop(0);
				
				salesforceData();
			}
			
			
		}
		
	});
	
	$("#oldmember button").on("click", function(e) {
		e.preventDefault();
		
		if(!submitting) {
			submitting = true;
			
			if ($("#login_email").val() == "") {
				submitting = false;
				alert("請填寫登入電郵");
			} else 
			if (!$("#login_email").val().match(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)) {
				submitting = false;
				alert("請填寫正確登入電郵");
			} else
			if ($("#login_password").val() == "") {
				submitting = false;
				alert("請填寫登入密碼");
			} else 
			{
				$("#member_type").val("1");
				$("#gerberform").submit();
			}
			
			
		}
		
	});

	$("#newmember button").on("click", function(e) {
		e.preventDefault();
		
		if(!submitting) {
			submitting = true;
			
			if ($("#last_name").val() == "") {
				submitting = false;
				alert("請填寫姓氏");
			} else 
			if ($("#first_name").val() == "") {
				submitting = false;
				alert("請填寫名字");
			} else 
			if ($("#phone").val() == "") {
				submitting = false;
				alert("請填寫聯絡電話");	
			} else 
			if (!$('#phone').val().match(/^[0-9]{8}$/i)) {
				submitting = false;
				alert("請填寫正確聯絡電話");	
			} else 
			if (!$('#phone').val().match(/^(5|6|8|9)/)) {
				submitting = false;
				alert("聯絡電話只接受5, 6, 8, 9 字首");	
			} else 
			if ($('#phone').val().match(/^999/)) {
				submitting = false;
				alert("聯絡電話只接受5, 6, 8, 9 字首");	
			} else 
			if ($("#email").val() == "") {
				submitting = false;
				alert("請填寫電郵地址");	
			} else 
			if (!$("#email").val().match(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)) {
				submitting = false;
				alert("請填寫正確電郵地址");	
			} else 
			if ($("#field_area").val() == "") {
				submitting = false;
				alert("請選擇地區");
			} else 
			if ($("#field_district").val() == "") {
				submitting = false;
				alert("請選擇分區");
			} else 
			if ($("#address").val() == "") {
				submitting = false;
				alert("請填寫通訊地址");
			} else
			if (!$("#agreement").prop("checked")) {
				submitting = false;
				alert("請細閱並接受網站之會籍條款及細則，活動條款及細則及私隱政策");
			} else {
				$("#member_type").val("2");
				$("#gerberform").submit();
			}
			
			
		}
		
	});

});
