<?php
include("database.php");

header('Content-Type: application/json; charset=utf-8');


	// open db connection
	$list_arr = array();

	// District
	$sql = "SELECT * FROM salesforce_district ORDER BY name_EN ASC";
	$result = $conn->query($sql);
	$ary_district = array();
	while($list=$result->fetch_array()){  //判斷是否還有資料沒有取完，如果取完，則停止while迴圈。
		$ary_district[$i]=$list;
		$i++;
	}
	$list_arr['ary_district'] = $ary_district;

	// Hospital
	$sql = "SELECT * FROM salesforce_hospital ORDER BY name_EN ASC";
	$result = $conn->query($sql);

	
	$ary_hospital = array();
	while($list=$result->fetch_array()){  //判斷是否還有資料沒有取完，如果取完，則停止while迴圈。
		$ary_hospital[$i]=$list;
		$i++;
	}
	$list_arr['ary_hospital'] = $ary_hospital;

	// Product
	$sql = "SELECT * FROM salesforce_product ORDER BY name_EN ASC";
	$result = $conn->query($sql);
	$ary_product = array();
	while($list=$result->fetch_array()){  //判斷是否還有資料沒有取完，如果取完，則停止while迴圈。
		$ary_product[$i]=$list;
		$i++;
	}
	$list_arr['ary_product'] = $ary_product;

	echo json_encode($list_arr);

exit;	
?>