<?php
include("database.php");

function htmlencode($msg) {

	if (!get_magic_quotes_gpc()) {
		$msg = addslashes($msg);
	}
	
	return htmlentities($msg, ENT_QUOTES, 'UTF-8');
}

function htmldecode($msg) {

	$msg = html_entity_decode($msg, ENT_QUOTES, 'UTF-8');
	
	return stripslashes($msg);
}

// Function to get the client IP address
function get_client_ip() {
	$ipaddress = '';
	if (isset($_SERVER['HTTP_CLIENT_IP']))
		$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_X_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_FORWARDED'];
	else if(isset($_SERVER['REMOTE_ADDR']))
		$ipaddress = $_SERVER['REMOTE_ADDR'];
	else
		$ipaddress = 'UNKNOWN';

	return $ipaddress;
}

if (stripos($_SERVER['HTTP_HOST'], "gerber.com.hk") !== false) {
	$nestlebb_login_api_url = "https://www.nestlebaby.hk/api/index.php";
} else {
	$nestlebb_login_api_url = "http://prodev01.fevaworks.net/nestle_shsh/dev/api/index.php";
}

print_r($_POST);

$member_type = htmlencode($_POST["member_type"]);

if ($member_type == "1") {
	// old member
	
	$login_email = htmlencode($_POST["login_email"]);
	$login_password = htmlencode($_POST["login_password"]);
	
	// login nestlebaby
	
	$curlObj = curl_init();

	curl_setopt($curlObj, CURLOPT_URL, $nestlebb_login_api_url.'?cmd=checkValidUserNew&email='.$login_email.'&password='.$login_password);
	curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curlObj, CURLOPT_HEADER, 0);
	
	$response = curl_exec($curlObj);
	
	$response_array = json_decode($response,true);
	
	curl_close($curlObj);
	
	$userid = $response_array["uid"];
	
	if ($userid == 0) {
?>
<script>
parent.submitting = false;
parent.alert("會員登入資料錯誤");
</script>
<?php
exit();
	}
	
	print_r($response_array);
}

	$photo_type = htmlencode($_POST["photo_type"]);
	
	$bb_name = htmlencode($_POST["bb_name"]);
	$bb_gender = htmlencode($_POST["bb_gender"]);
	$bb_year = htmlencode($_POST["bb_year"]);
	$bb_month = htmlencode($_POST["bb_month"]);
	$bb_day = htmlencode($_POST["bb_day"]);
	$hkid = htmlencode($_POST["hkid"]);

	if(isset($_FILES["upload"]["name"]) && $_FILES["upload"]["name"] != "")
	{
		$fieldname = "upload";
		
		$validextensions = array("jpeg", "jpg", "png");
		$temporary = explode(".", $_FILES[$fieldname]["name"]);
		$file_extension = strtolower(end($temporary));
		
		if (($_FILES[$fieldname]["size"] <= 10485760) //Approx. 5M files can be uploaded.
		 && in_array($file_extension, $validextensions)) {
			
			if ($_FILES[$fieldname]["error"] > 0)
			{
?>
<script>
parent.submitting = false;
parent.alert("相片錯誤");
</script>
<?php
exit();
			}
			else
			{

				// upload bb photo
		
				$id = time();
				
				$imagePath = "upload/photo/".date("YmdH");
				
				mkdir("../".$imagePath, 0777);

				$imagePath = $imagePath."/".$id;
				
				mkdir("../".$imagePath, 0777);

				$filedate = time().mt_rand(1000,9999);
		
				$sourcePath = $_FILES[$fieldname]["tmp_name"]; // Storing source path of the file in a variable
				$targetPath = $imagePath."/".$filedate.'.'.$file_extension; // Target path where file is to be stored
				
				move_uploaded_file($sourcePath,"../".$targetPath) ; // Moving Uploaded file
				
			}
			
			
		 } 
		 else {
?>
<script>
parent.submitting = false;
parent.alert("相片錯誤");
</script>
<?php
exit();
		 }
				
	} else if ($_POST["uploaddata"] != "") {

		// upload bb photo
		$file_extension = "png";

		$id = time();
		
		$imagePath = "upload/photo/".date("YmdH");
		
		mkdir("../".$imagePath, 0777);

		$imagePath = $imagePath."/".$id;
		
		mkdir("../".$imagePath, 0777);

		$filedate = time().mt_rand(1000,9999);
		
		$targetPath = $imagePath."/".$filedate.'.'.$file_extension; // Target path where file is to be stored
		
		$filteredData = explode(',', $_POST["uploaddata"]);

		$image = imagecreatefromstring(base64_decode($filteredData[1]));
		
		@imagealphablending($image, TRUE);
		@imagesavealpha($image, TRUE);
		imagepng($image, "../".$targetPath);

		@imagedestroy($image);
		
	}
		
		
	if ($member_type == "1") {	
	
		// old member
		
		$login_uid = $response_array["uid"];
		$login_first_name = $response_array["first_name"];
		$login_last_name = $response_array["last_name"];

		$sql = "insert into `gerberfev_game_201710` (`photo_type`, `bb_name`, `bb_gender`, `bb_dob`, `hkid`, `photo`, `member_type`, `login_uid`, `first_name`, `last_name`, `email`, `status`, `created_ip`, `created_date` ) values (";
		$sql .= "'".htmlencode($photo_type)."',";
		$sql .= "'".htmlencode($bb_name)."',";
		$sql .= "'".htmlencode($bb_gender)."',";
		$sql .= "'".htmlencode($bb_year."-".$bb_month."-".$bb_day)."',";
		$sql .= "'".htmlencode($hkid)."',";
		$sql .= "'".htmlencode($targetPath)."',";
		$sql .= "'".htmlencode($member_type)."',";
		$sql .= "'".htmlencode($login_uid)."',";
		$sql .= "'".htmlencode($login_first_name)."',";
		$sql .= "'".htmlencode($login_last_name)."',";
		$sql .= "'".htmlencode($login_email)."',";
		$sql .= "'2',";		
		$sql .= "'".get_client_ip()."', ";
		$sql .= "'".date("Y-m-d H:i:s")."'";
		$sql .= ")";

		$conn->query($sql);
		
	} else if ($member_type == "2") {	
	
		// new member

		$first_name = htmlencode($_POST["first_name"]);
		$last_name = htmlencode($_POST["last_name"]);
		$phone = htmlencode($_POST["phone"]);
		$email = htmlencode($_POST["email"]);
		$field_area = htmlencode($_POST["field_area"]);
		$field_district = htmlencode($_POST["field_district"]);
		$address = htmlencode($_POST["address"]);
		
		$pregnant_mum = htmlencode($_POST["pregnant_mum"]);
		$children_mum = htmlencode($_POST["children_mum"]);
		
		if ($pregnant_mum == "1") {
			
			$fm_status = "1";
			$fm_year = htmlencode($_POST["preg_bb_year"]);
			$fm_month = htmlencode($_POST["preg_bb_month"]);
			$fm_day = htmlencode($_POST["preg_bb_day"]);
			
		} else if ($children_mum == "1") {
			
			$fm_status = "2";
			$fm_year = htmlencode($_POST["children_bb_year"]);
			$fm_month = htmlencode($_POST["children_bb_month"]);
			$fm_day = htmlencode($_POST["children_bb_day"]);
			
		} else {
?>
<script>
parent.submitting = false;
</script>
<?php
exit();
		}

		$promotion = htmlencode($_POST["promotion"]);
		

		$sql = "insert into `gerberfev_game_201710` (`photo_type`, `bb_name`, `bb_gender`, `bb_dob`, `hkid`, `photo`, `member_type`, `first_name`, `last_name`, `email`, `phone`, `area`, `district`, `address`, `fm_status`, `fm_date`, `promotion`, `status`, `created_ip`, `created_date` ) values (";
		$sql .= "'".htmlencode($photo_type)."',";
		$sql .= "'".htmlencode($bb_name)."',";
		$sql .= "'".htmlencode($bb_gender)."',";
		$sql .= "'".htmlencode($bb_year."-".$bb_month."-".$bb_day)."',";
		$sql .= "'".htmlencode($hkid)."',";
		$sql .= "'".htmlencode($targetPath)."',";
		$sql .= "'".htmlencode($member_type)."',";
		$sql .= "'".htmlencode($first_name)."',";
		$sql .= "'".htmlencode($last_name)."',";
		$sql .= "'".htmlencode($email)."',";
		$sql .= "'".htmlencode($phone)."',";
		$sql .= "'".htmlencode($field_area)."',";
		$sql .= "'".htmlencode($field_district)."',";
		$sql .= "'".htmlencode($address)."',";
		$sql .= "'".htmlencode($fm_status)."',";
		$sql .= "'".htmlencode($fm_year."-".$fm_month."-".$fm_day)."',";
		$sql .= "'".htmlencode($promotion)."',";
		$sql .= "'2',";		
		$sql .= "'".get_client_ip()."', ";
		$sql .= "'".date("Y-m-d H:i:s")."'";
		$sql .= ")";

		$conn->query($sql);

	}
	
	$new_id = $conn->insert_id;
?>
<script>
//parent.submitting = false;
parent.window.location = '../contest/questionnaire?id=<?php echo $new_id; ?>';
</script>
<?php
exit();
?>