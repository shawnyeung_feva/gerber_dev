﻿<?php
define('_VALID_INCLUDE', TRUE); // flag to allow include or require files
$dir_level = "../"; //set the required files located

require_once($dir_level.'includes/vars.inc.php');
require_once($dir_level.'includes/common.inc.php');

// clean up old file (before 2 days)
$dir = "../upload/temp/";

/*** cycle through all files in the directory ***/
foreach (glob($dir."*") as $file) {

	/*** if file is 48 hours (86400 * 2 seconds) old then delete it ***/
	if (filemtime($file) < time() - (86400 * 2)) {
		@unlink($file);
	}
}

$output = array("fieldname" => "", "error" => "請上載檔案","url" => "","preview" => "");

if(isset($_POST["fieldname"]))
{
	$fieldname = $_POST["fieldname"];
	$validextensions = array("jpeg", "jpg", "pdf");
	$temporary = explode(".", $_FILES[$fieldname]["name"]);
	$file_extension = strtolower(end($temporary));
	
	$output["fieldname"] = $fieldname;
	
	if (($_FILES[$fieldname]["size"] <= 5242880) //Approx. 5M files can be uploaded.
	 && in_array($file_extension, $validextensions)) {
		
		if ($_FILES[$fieldname]["error"] > 0)
		{
			//echo "<script>parent.alert('您的網路出現異常，請重新上傳');parent.uploadReset();</script>";
			$output["error"] = "您的網路出現異常，請重新上傳";
		}
		else
		{

			$imagePath = "upload/temp/";

			$filedate = time().mt_rand(1000,9999);
	
			$sourcePath = $_FILES[$fieldname]["tmp_name"]; // Storing source path of the file in a variable
			$targetPath = $imagePath.$filedate.'.'.$file_extension; // Target path where file is to be stored
			
			move_uploaded_file($sourcePath,"../".$targetPath) ; // Moving Uploaded file

			//echo "<script>parent.readURLFB('".$cfg["root"].$targetPath."');</script>";
			$output["error"] = "";
			$output["url"] = $targetPath;
			$output["preview"] = "".$_FILES[$fieldname]["name"]." (".round($_FILES[$fieldname]["size"] / 1024, 1)."KB)<br>[<a href=\"".str_ireplace("/php","",$cfg["root"]).$targetPath."\" target=\"_blank\">預覽</a>]  [<a href=\"javascript:void(0);\" onclick=\"ajax_upload_reset('".$fieldname."');\" >移除</a>]";
		}
		
	}
	else
	{
		//echo "<script>parent.alert('相片格式為JPG或PNG及檔案大小為1-2MB');parent.uploadReset();</script>";
		$output["error"] = "只接受JPG或PDF檔及檔案大小少於5MB";
	}
}

header('Content-Type: text/html; charset=utf-8');
echo "<script>parent.ajax_upload_cb(".json_encode($output).");</script>";
exit();
?>