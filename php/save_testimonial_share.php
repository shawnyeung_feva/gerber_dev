<?php
define('_VALID_INCLUDE', TRUE); // flag to allow include or require files
$dir_level = "../"; //set the required files located

require_once($dir_level.'includes/vars.inc.php');
require_once($dir_level.'includes/common.inc.php');

header('Content-Type: application/json; charset=utf-8');

$id = trim($_POST["id"]);
if (!is_numeric($id)) {
	$id = "";
}

$type = trim($_POST["type"]);

if ($id != "" && $type != "") {
	
	if ($type == "fb") {
		
		$field_type="share_fb_count";
		
	} else if ($type == "wa") {
		
		$field_type="share_wa_count";
		
	} else {
		echo json_encode(array('status'=>'busy'));
		exit();
	}
	
	

	// open db connection
	$conn = openConnection($conn);
/*	
	$sql = " select `id` from `gerberfev_game_201710_like_count_log` where 1 ";
	$sql.= " and `bb_id` = '".htmlencode($id)."' ";
	$sql.= " and `created_ip` = '".get_client_ip()."' ";
	$sql.= " and `status` = '1' ";

	$result = mysql_query($sql, $conn);
	$num = mysql_num_rows($result);

	if ($num <= 0) {

	} else {
		echo json_encode(array('status'=>'repeat','id'=>$id));
		exit();
	}
	
	$sql = "insert into `gerberfev_game_201710_like_count_log` (`bb_id`, `status`, `created_ip`, `created_date` ) values (";
	$sql .= "'".htmlencode($id)."',";
	$sql .= "'1',";		
	$sql .= "'".get_client_ip()."', ";
	$sql .= "'".date("Y-m-d H:i:s")."' ";
	$sql .= ")";

	mysql_query($sql, $conn);
*/	

	$sql = " update `gerberfev_game_201710` SET `".$field_type."` = (`".$field_type."` + 1) where 1 ";
	$sql.= " and `id` = '".htmlencode($id)."' ";
	$sql.= " and `status` = '1' ";

	mysql_query($sql, $conn);
	
	
	/*
	
	$sql = " select `".$field_type."` from `gerberfev_game_201710` where 1 ";
	$sql.= " and `id` = '".htmlencode($id)."' ";
	$sql.= " and `status` = '1' ";

	$result = mysql_query($sql, $conn);
	
	if ($row = mysql_fetch_array($result)) {

		echo json_encode(array('status'=>'success','id'=>$id);
	} else {
		echo json_encode(array('status'=>'busy'));
	}
	
	*/
	
	echo json_encode(array('status'=>'success','id'=>$id));

	// close db connection
	$conn = closeConnection($conn);


} else {
	echo json_encode(array('status'=>'busy'));
}

exit();	
?>