<?php
$servername = "prodev01.fevaworks.net";
$username = "root";
$password = "p@ssw0rd";
$dbname = "nestlebaby_hk_dev";
$dbLink = mysql_connect($servername, $username, $password) or die("cannot connect");
mysql_select_db($dbname) or die("cannot select DB");
mysql_query("SET character_set_results=utf8", $dbLink);
mysql_query("set names 'utf8'",$dbLink);

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
$conn->set_charset("utf8");
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if (function_exists('date_default_timezone_set')) {
	date_default_timezone_set("Asia/Hong_Kong"); // Sets the default timezone used by all date/time functions in a script
}

?>
