<?php
include("database.php");

function htmlencode($msg) {

	if (!get_magic_quotes_gpc()) {
		$msg = addslashes($msg);
	}
	
	return htmlentities($msg, ENT_QUOTES, 'UTF-8');
}

function htmldecode($msg) {

	$msg = html_entity_decode($msg, ENT_QUOTES, 'UTF-8');
	
	return stripslashes($msg);
}

// Function to get the client IP address
function get_client_ip() {
	$ipaddress = '';
	if (isset($_SERVER['HTTP_CLIENT_IP']))
		$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_X_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_FORWARDED'];
	else if(isset($_SERVER['REMOTE_ADDR']))
		$ipaddress = $_SERVER['REMOTE_ADDR'];
	else
		$ipaddress = 'UNKNOWN';

	return $ipaddress;
}

print_r($_POST);

?>
<script>
//parent.submitting = false;
parent.window.location = '../contest/success';
</script>
<?php
exit();
?>