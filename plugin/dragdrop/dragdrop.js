$(document).ready(function() {

	/*****************************
		Variables
	*****************************/
	var imgWidth = 300,
		imgHeight = 300,
		zindex = 0;
		dropzone = $('#droparea'),
		uploadBtn = $('#uploadbtn'),
		defaultUploadBtn = $('#upload');
		

	/*****************************
		Events Handler
	*****************************/
	dropzone.on('dragover', function() {
		//add hover class when drag over
		dropzone.addClass('hover');
		return false;
	});
	dropzone.on('dragleave', function() {
		//remove hover class when drag out
		dropzone.removeClass('hover');
		return false;
	});
	dropzone.on('drop', function(e) {
		//prevent browser from open the file when drop off
		e.stopPropagation();
		e.preventDefault();
        $('#preview').html('');
		dropzone.removeClass('hover');
		
		//retrieve uploaded files data
		var files = e.originalEvent.dataTransfer.files;
		processFiles(files);

		return false;
	});
		
	uploadBtn.on('click', function(e) {
		e.stopPropagation();
		e.preventDefault();
		//trigger default file upload button
		defaultUploadBtn.click();
	});
	defaultUploadBtn.on('change', function() {
		//retrieve selected uploaded files data
		var files = $(this)[0].files;
		processFiles(files);
		
		return false;
	});
	
	
	/***************************** 
		internal functions
	*****************************/	
	//Bytes to KiloBytes conversion
	function convertToKBytes(number) {
		return (number / 1024).toFixed(1);
	}
	
	function compareWidthHeight(width, height) {
		var diff = [];
		if(width > height) {
			diff[0] = width - height;
			diff[1] = 0;
		} else {
			diff[0] = 0;
			diff[1] = height - width;
		}
		return diff;
	}
	
	/*********
	BlobBuilder is deprecated
	**********/
	/*
	//convert datauri to blob
	function dataURItoBlob(dataURI) {
		var BlobBuilder = window.WebKitBlobBuilder || window.MozBlobBuilder || window.BlobBuilder;
	
		//skip if browser doesn't support BlobBuilder object
		if(typeof BlobBuilder === "undefined") {
			$('#err').html('Ops! There have some limited with your browser! <br/>New image produced from canvas can\'t be upload to the server...');
			return dataURI;
		}
		
		// convert base64 to raw binary data held in a string
		// doesn't handle URLEncoded DataURIs
		var byteString;
		if (dataURI.split(',')[0].indexOf('base64') >= 0)
			byteString = atob(dataURI.split(',')[1]);
		else
			byteString = unescape(dataURI.split(',')[1]);

		// separate out the mime component
		var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

		// write the bytes of the string to an ArrayBuffer
		var ab = new ArrayBuffer(byteString.length);
		var ia = new Uint8Array(ab);
		for (var i = 0; i < byteString.length; i++) {
			ia[i] = byteString.charCodeAt(i);
		}

		// write the ArrayBuffer to a blob, and you're done
        var bb = new BlobBuilder();
		bb.append(ab);
		return bb.getBlob(mimeString);
	}
	*/
	function dataURItoBlob(dataURI) {

		// convert base64 to raw binary data held in a string
		// doesn't handle URLEncoded DataURIs
		var byteString;
		if (dataURI.split(',')[0].indexOf('base64') >= 0)
			byteString = atob(dataURI.split(',')[1]);
		else
			byteString = unescape(dataURI.split(',')[1]);

		// separate out the mime component
		var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

		// write the bytes of the string to an ArrayBuffer
		var ab = new ArrayBuffer(byteString.length);
		var ia = new Uint8Array(ab);
		for (var i = 0; i < byteString.length; i++) {
			ia[i] = byteString.charCodeAt(i);
		}

		//Passing an ArrayBuffer to the Blob constructor appears to be deprecated, 
		//so convert ArrayBuffer to DataView
		var dataView = new DataView(ab);
		var blob = new Blob([dataView], {type: mimeString});

		return blob;
    }
	
	
	
	/***************************** 
		Process FileList 
	*****************************/	
	var processFiles = function(files) {
		if(files && typeof FileReader !== "undefined") {
            if(files.length>1){
                $('#err').html('每次只可上載一張圖片');
                return false;
            } else {
                readFile(files[0]);
            }
		} else {
			
		}
	}
	
	
	/***************************** 
		Read the File Object
	*****************************/	
	var readFile = function(file) {
		if( (/image/i).test(file.type) ) {
			//define FileReader object
			var reader = new FileReader();
			
			//init reader onload event handlers
			reader.onload = function(e) {	
				var image = $('<img/>')
				.load(function() {
					enquire.register("screen and (max-width:567px)", {//mobile
						match : function() {
							$('html, body').animate({
								scrollTop: $("#preview").offset().top
							}, 2000);
						},      
						unmatch : function() {},    
						setup : function() {},    
						deferSetup : true
					});
					//when image fully loaded
					var newimageurl = getCanvasImage(this);
					createPreview(file, newimageurl);
					//console.log(file);
					uploadToServer(file, dataURItoBlob(newimageurl));
				})
				.attr('src', e.target.result);	
			};
			
			//begin reader read operation
			reader.readAsDataURL(file);
			
			$('#err').text('');
		} else {
			//some message for wrong file format
			$('#err').text('請上載以下格式(.jpg/.jpeg/.png)的圖片');
		}
	}
	
	
	/***************************** 
		Get New Canvas Image URL
	*****************************/	
	var getCanvasImage = function(image) {
		//define canvas
		var canvas = document.createElement('canvas');
		imgWidth = image.width;
		imgHeight = image.height;
		canvas.width = imgWidth;
		canvas.height = imgHeight;
		var ctx = canvas.getContext('2d');
		
		//default resize variable
		var diff = [0, 0];
		
		//draw canvas image	
		ctx.drawImage(image, diff[0]/2, diff[1]/2, image.width-diff[0], image.height-diff[1], 0, 0, imgWidth, imgHeight);
		
		//convert canvas to jpeg url
		return canvas.toDataURL("image/jpeg");
	}
	
	
	/***************************** 
		Draw Image Preview
	*****************************/	
	var createPreview = function(file, newURL) {	
		//populate jQuery Template binding object
		var imageObj = {};
		imageObj.filePath = newURL;
		imageObj.fileName = file.name.substr(0, file.name.lastIndexOf('.')); //subtract file extension
		imageObj.fileOriSize = convertToKBytes(file.size);
		imageObj.fileUploadSize = convertToKBytes(dataURItoBlob(newURL).size); //convert new image URL to blob to get file.size
		
		$("#uploaddata").val(newURL);
		
		$('#preview').html('');
		var img = $("#imageTemplate").tmpl(imageObj).prependTo("#preview")
		.hide()
		.css({
			'z-index': zindex++
		})
		.show();
		
		if(isNaN(imageObj.fileUploadSize)) {
			$('.imageholder span').last().hide();
		}
	}
	
	
	
	/****************************
		Upload Image to Server
	****************************/

	var uploadToServer = function(oldFile, newFile) {
		// prepare FormData
		var formData = new FormData();  
		//we still have to use back old file
		//since new file doesn't contains original file data
		formData.append('filename', oldFile.name);
		formData.append('filetype', oldFile.type);
		formData.append('file', newFile); 

		/*		
		//submit formData using $.ajax			
		$.ajax({
			url: '',
			type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
			success: function(data) {
				console.log(data);
			}
		});	
		*/
	}
	
	/****************************
		Browser compatible text
	****************************/
	if (typeof FileReader === "undefined") {
		//$('.extra').hide();
		$('#err').html('瀏覽器不支援，請使用Chrome或Firefox');
	} else if (!Modernizr.draganddrop) {
        $('#droparea .dropfiletext span').remove();
	} else {
		$('#err').text('');
	}
});