<?php

include 'route.php';

$route = new Route();

$route->add('/', function() {
    include 'templates/home.php';
});

$route->add('/home', function() {
    include 'templates/home.php';
});

$route->add('/productinformation', function() {
    include 'templates/list.php';
});

$route->add('/aboutus', function() {
    include 'templates/aboutus.php';
});

$route->add('/contactus', function() {
    include 'templates/contactus.php';
});
$route->add('/contest', function() {
    include 'templates/contest.php';
});
$route->add('/contest/step1', function() {
    include 'templates/contest/contest_page1.php';
});
$route->add('/contest/questionnaire', function() {
    include 'templates/contest/questionnaire.php';
});
$route->add('/contest/success', function() {
    include 'templates/contest/success.php';
});
$route->add('/contest/album', function() {
    include 'templates/contest/album.php';
});

$route->add('/safetyandresearch', function() {
    include 'templates/safety_research.php';
});

$route->add('/officialgood', function() {
    include 'templates/real_goods.php';
});

$route->add('/productinformation/ricecereal', function() {
    include 'templates/product/1.php';
});
$route->add('/productinformation/organicoatmeal', function() {
    include 'templates/product/2.php';
});
$route->add('/productinformation/bananaappleic', function() {
    include 'templates/product/sitter_0.php';
});
$route->add('/productinformation/applejuice', function() {
    include 'templates/product/sitter_1.php';
});
$route->add('/productinformation/蘋果汁', function() {
    include 'templates/product/sitter_2.php';
});
$route->add('/productinformation/第二階段食品', function() {
    include 'templates/product/sitter_3.php';
});
$route->add('/productinformation/蘋果藍莓蓉', function() {
    include 'templates/product/sitter_4.php';
});
$route->add('/productinformation/蘋果蓉', function() {
    include 'templates/product/sitter_5.php';
});
$route->add('/productinformation/chickengravy', function() {
    include 'templates/product/sitter_6.php';
});
$route->add('/productinformation/organicveggies', function() {
    include 'templates/product/crawler_0.php';
});
$route->add('/productinformation/organicapple', function() {
    include 'templates/product/crawler_1.php';
});
$route->add('/productinformation/peachpuff', function() {
    include 'templates/product/crawler_2.php';
});
$route->add('/productinformation/bananapuff', function() {
    include 'templates/product/crawler_3.php';
});
$route->add('/productinformation/sweetpotatopuff', function() {
    include 'templates/product/crawler_4.php';
});
$route->add('/productinformation/strawberryapplepuff', function() {
    include 'templates/product/crawler_5.php';
});
$route->add('/productinformation/arrowrootcookies', function() {
    include 'templates/product/crawler_6.php';
});
$route->add('/productinformation/ymmixedberries', function() {
    include 'templates/product/crawler_7.php';
});
$route->add('/productinformation/ympeach', function() {
    include 'templates/product/crawler_8.php';
});
$route->add('/productinformation/lilbiscuit', function() {
    include 'templates/product/toddler_0.php';
});
$route->add('/productinformation/animalcrackers', function() {
    include 'templates/product/toddler_1.php';
});
$route->add('/productinformation/bittybitesstrawberry', function() {
    include 'templates/product/toddler_2.php';
});
/* 2018-01-03 */
$route->add('/productinformation/gerber-dha-probiotic-cereal-rice', function() {
    include 'templates/product/3.php';
});
$route->add('/productinformation/gerber-lil-bits-cereal-oatmeal-banana-strawberry', function() {
    include 'templates/product/crawler_9.php';
});
$route->add('/productinformation/gerber-organic-yogurt-melts-banana-strawberry', function() {
    include 'templates/product/crawler_10.php';
});
/* 2018-01-03 */




$route->add('/ouringredient', function() {
    include 'templates/our_food.php';
});

$route->add('/choosegerber', function() {
    include 'templates/why_geber.php';
});

$route->add('/sitemap', function() {
    include 'templates/sitemap.php';
});


// function
$route->add('/admin', function() {
    include 'admin/data/server_side/login.php';
});

$route->add('/api/contactus', function() {
    include 'php/contactus.php';
});
$route->add('/api/questionnaire', function() {
    include 'php/questionnaire.php';
});
$route->add('/api/sharingupdate', function() {
    include 'php/sharing.php';
});
$route->add('/api/videoupdate', function() {
    include 'php/videoplay.php';
});
$route->add('/api/getip', function() {
    include 'php/getip.php';
});


$route->submit();




?>
