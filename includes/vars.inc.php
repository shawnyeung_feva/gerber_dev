<?php
/* turn on output buffer */
ob_start();

/* check valid include */
defined('_VALID_INCLUDE') or die('Direct access not allowed.');

/* set the cache limiter to 'private_no_expire' */
session_cache_limiter('private');

/* set the cache expire to 3hours */
session_cache_expire(180);

/* set timeout to 3hours */
ini_set("session.gc_maxlifetime", "10800"); 

/* allow cross sub domain session */
//ini_set('session.cookie_domain','prodev03.fevaworks.com');

/* start the session */
@session_start();

/* default  Cache-control*/
header("Cache-control: private");
header('Expires: '.gmdate('D, d M Y H:i:s',strtotime('now')-1800) . ' GMT');
header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');

// facebook iframe
header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
// auto get url
include_once(dirname(__FILE__) . '/../bootstrap.inc.php');

if (function_exists('date_default_timezone_set')) {
	date_default_timezone_set("Asia/Hong_Kong"); // Sets the default timezone used by all date/time functions in a script 
}

/*****************************************
 General Configuration
*****************************************/
$cfg['site_name'] = "Gerber";
$cfg['charset'] = "utf-8";
$cfg['meta_keywords'] = "";
$cfg['meta_description'] = "";
$cfg['meta_generator'] = "1.0 BETA by Fevaworks";

$cfg['root'] = 'http://'.$_SERVER['SERVER_NAME'].substr($_SERVER['PHP_SELF'], 0, strrpos($_SERVER['PHP_SELF'], '/')+1);
$cfg['path'] = PHP_REAL_BASE_DIR;

$cfg['server_type'] = "1"; // 0 - *nix, 1 - windows

$cfg['prefix_session'] = "gerber_";

$cfg['admin_id'] = "admin"; // Report page login id
$cfg['admin_password'] = "gerberp@ss2017"; // Report page login password

/*****************************************
 Database Configuration
*****************************************/
global $db_host,$db_username,$db_password,$db_name,$db_encoding;

$db_host = "127.0.0.1";
$db_username = "root";
$db_password = "p@ssw0rd";
$db_name = "nestlebaby_hk_dev";
$db_encoding = "utf8";
?>