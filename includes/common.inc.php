<?php
/*****************************************
Functions
- DO NOT MODIFY -
unless you know what you are doing
*****************************************/

mb_internal_encoding('UTF-8'); 
 
function utf8_split($pattern, $string, $limit = null) 
{ 
	if (is_null($limit)) 
	{ 
		return mb_split($pattern, $string); 
	} 
	else 
	{ 
		return mb_split($pattern, $string, $limit); 
	} 
} 
 
function utf8_stripos($string, $needle, $offset = null) 
{ 
	if (is_null($offset)) 
	{ 
		return mb_stripos($string, $needle); 
	} 
	else 
	{ 
		return mb_stripos($string, $needle, $offset); 
	} 
} 
 
function utf8_stristr($string, $needle, $offset = null) 
{ 
	if (is_null($offset)) 
	{ 
		return mb_stristr($string, $needle); 
	} 
	else 
	{ 
		return mb_stristr($string, $needle, $offset); 
	} 
} 

function utf8_strlen($text) 
{ 
	return mb_strlen($text, 'utf-8'); 
} 
 
function utf8_strpos($string, $needle, $offset = null) 
{ 
	if (is_null($offset)) 
	{ 
		return mb_strpos($string, $needle); 
	} 
	else 
	{ 
		return mb_strpos($string, $needle, $offset); 
	} 
} 
 
function utf8_strrpos($string,    $needle, $offset = null) 
{ 
	if (is_null($offset)) 
	{ 
		if (empty($string)) 
		{ 
			return false; 
		} 
		return mb_strrpos($string, $needle); 
	} 
	else 
	{ 
		if (!is_int($offset)) 
		{ 
			trigger_error('utf8_strrpos expects parameter 3 to be long', E_USER_ERROR); 
			return false; 
		} 
		$string = mb_substr($string, $offset); 
		if (false !== ($pos = mb_strrpos($string, $needle))) 
		{ 
			return $pos + $offset; 
		} 
		return false; 
	} 
} 
 
function utf8_strstr($string, $needle, $offset = null) 
{ 
	if (is_null($offset)) 
	{ 
		return mb_strstr($string, $needle); 
	} 
	else 
	{ 
		return mb_strstr($string, $needle, $offset); 
	} 
} 
 
//USED 
function utf8_strtolower($stringing) 
{ 
	return mb_strtolower($stringing); 
} 

//USED 
function utf8_strtoupper($stringing) 
{ 
	return mb_strtoupper($stringing); 
} 

function utf8_substr($string, $needle, $length = null) 
{ 
	if (is_null($length)) 
	{ 
		return mb_substr($string, $needle); 
	} 
	else 
	{ 
		return mb_substr($string, $needle, $length); 
	} 
} 
 
function utf8_substr_count($string, $needle, $offset = null, $length = null) 
{ 
	if (is_null($offset)) 
	{ 
		return mb_substr_count($string, $needle); 
	} 
	else 
	{ 
		return mb_substr_count($string, $needle, $offset, $length); 
	} 
} 
 
 function utf8_ireplace($search, $replace, $str, $count = NULL) 
{ 
	if (!is_array($search)) 
	{ 
		$slen = strlen($search); 
		if ($slen == 0) 
		{ 
			return $str; 
		} 
		$lendif = strlen($replace) - strlen($search); 
		$search = utf8_strtolower($search); 
		$search = preg_quote($search); 
		$lstr = utf8_strtolower($str); 
		$i = 0; 
		$matched = 0; 
		while (preg_match('/(.*)' . $search . '/Us', $lstr, $matches)) 
		{ 
			if ($i === $count) 
			{ 
				break; 
			} 
			$mlen = strlen($matches[0]); 
			$lstr = substr($lstr, $mlen); 
			$str = substr_replace($str, $replace, $matched+strlen($matches[1]), $slen); 
			$matched += $mlen + $lendif; 
			$i++; 
		} 
		return $str; 
	} 
	else 
	{ 
		foreach (array_keys($search) as $k) 
		{ 
			if (is_array($replace)) 
			{ 
				if (array_key_exists($k,$replace)) 
				{ 
					$str = utf8_ireplace($search[$k], $replace[$k], $str, $count); 
				} 
				else 
				{ 
					$str = utf8_ireplace($search[$k], '', $str, $count); 
				} 
			} 
			else 
			{ 
				$str = utf8_ireplace($search[$k], $replace, $str, $count); 
			} 
		} 
		return $str; 
	} 
} 
 
function utf8_ord($chr) 
{ 
	$ord0 = ord($chr); 
	if ($ord0 >= 0 && $ord0 <= 127) 
	{ 
		return $ord0; 
	} 
	if (!isset($chr{1})) 
	{ 
		trigger_error('Short sequence - at least 2 bytes expected, only 1 seen'); 
		return FALSE; 
	} 
	$ord1 = ord($chr{1}); 
	if ($ord0 >= 192 && $ord0 <= 223) 
	{ 
		return ($ord0 - 192) * 64 + ($ord1 - 128); 
	} 
	if (!isset($chr{2})) 
	{ 
		trigger_error('Short sequence - at least 3 bytes expected, only 2 seen'); 
		return FALSE; 
	} 
	$ord2 = ord($chr{2}); 
	if ($ord0 >= 224 && $ord0 <= 239) 
	{ 
		return ($ord0 - 224) * 4096 + ($ord1 - 128) * 64 + ($ord2 - 128); 
	} 
	if (!isset($chr{3})) 
	{ 
		trigger_error('Short sequence - at least 4 bytes expected, only 3 seen'); 
		return FALSE; 
	} 
	$ord3 = ord($chr{3}); 
	if ($ord0 >= 240 && $ord0 <= 247) 
	{ 
		return ($ord0 - 240) * 262144 + ($ord1 - 128) * 4096 + ($ord2 - 128) * 64 + ($ord3 - 128); 
	} 
	if (!isset($chr{4})) 
	{ 
		trigger_error('Short sequence - at least 5 bytes expected, only 4 seen'); 
		return FALSE; 
	} 
	$ord4 = ord($chr{4}); 
	if ($ord0 >= 248 && $ord0 <= 251) 
	{ 
		return ($ord0 - 248) * 16777216 + ($ord1 - 128) * 262144 + ($ord2 - 128) * 4096 + ($ord3 - 128) * 64 + ($ord4 - 128); 
	} 
	if (!isset($chr{5})) 
	{ 
		trigger_error('Short sequence - at least 6 bytes expected, only 5 seen'); 
		return FALSE; 
	} 
	if ($ord0 >= 252 && $ord0 <= 253) 
	{ 
		return ($ord0 - 252) * 1073741824 + ($ord1 - 128) * 16777216 + ($ord2 - 128) * 262144 + ($ord3 - 128) * 4096 + ($ord4 - 128) * 64 + (ord($c{5}) - 128); 
	} 
	if ($ord0 >= 254 && $ord0 <= 255) 
	{  
		trigger_error('Invalid UTF-8 with surrogate ordinal '.$ord0); 
		return FALSE; 
	} 
} 
 
function utf8_str_pad($input, $length, $padStr = ' ', $type = STR_PAD_RIGHT) 
{ 
	$inputLen = utf8_strlen($input); 
	if ($length <= $inputLen) 
	{ 
		return $input; 
	} 
	$padStrLen = utf8_strlen($padStr); 
	$padLen = $length - $inputLen; 
	if ($type == STR_PAD_RIGHT) 
	{ 
		$repeatTimes = ceil($padLen / $padStrLen); 
		return utf8_substr($input . str_repeat($padStr, $repeatTimes), 0, $length); 
	} 
	if ($type == STR_PAD_LEFT) 
	{ 
		$repeatTimes = ceil($padLen / $padStrLen); 
		return utf8_substr(str_repeat($padStr, $repeatTimes), 0, floor($padLen)) . $input; 
	} 
	if ($type == STR_PAD_BOTH) 
	{ 
		$padLen /= 2; 
		$padAmountLeft = floor($padLen); 
		$padAmountRight = ceil($padLen); 
		$repeatTimesLeft = ceil($padAmountLeft / $padStrLen); 
		$repeatTimesRight = ceil($padAmountRight / $padStrLen); 
		$paddingLeft = utf8_substr(str_repeat($padStr, $repeatTimesLeft), 0, $padAmountLeft); 
		$paddingRight = utf8_substr(str_repeat($padStr, $repeatTimesRight), 0, $padAmountLeft); 
		return $paddingLeft . $input . $paddingRight; 
	} 
	trigger_error('utf8_str_pad: Unknown padding type (' . $type . ')', E_USER_ERROR); 
} 
 
function utf8_ucfirst($str) 
{ 
	switch (utf8_strlen($str)) 
	{ 
		case 0: 
			return ''; 
			break; 
		case 1: 
			return utf8_strtoupper($str); 
			break; 
		default: 
			preg_match('/^(.{1})(.*)$/us', $str, $matches); 
			return utf8_strtoupper($matches[1]).$matches[2]; 
			break; 
	} 
} 
 
//USED 
function utf8_ucwords($str) 
{ 
	$pattern = '/(^|([\x0c\x09\x0b\x0a\x0d\x20]+))([^\x0c\x09\x0b\x0a\x0d\x20]{1})[^\x0c\x09\x0b\x0a\x0d\x20]*/u'; 
	return preg_replace_callback($pattern, 'utf8_ucwords_callback', $str); 
} 

function utf8_ucwords_callback($matches) 
{ 
	$leadingws = $matches[2]; 
	$ucfirst = utf8_strtoupper($matches[3]); 
	$ucword = utf8_substr_replace(ltrim($matches[0]), $ucfirst, 0, 1); 
	return $leadingws . $ucword; 
} 
 
function utf8_substr_replace($str, $repl, $start, $length = NULL) 
{ 
	preg_match_all('/./us', $str, $ar); 
	preg_match_all('/./us', $repl, $rar); 
	if ($length === NULL) 
	{ 
		$length = utf8_strlen($str); 
	} 
	array_splice($ar[0], $start, $length, $rar[0]); 
	return join('', $ar[0]); 
} 
 
function utf8_htmlentities($text) 
{ 
	return htmlentities($text, ENT_QUOTES, 'UTF-8'); 
} 
 
function utf8_ltrim($str, $charlist = FALSE) 
{ 
	if ($charlist === FALSE) 
	{ 
		return ltrim($str); 
	} 
	$charlist = preg_replace('!([\\\\\\-\\]\\[/^])!','\\\${1}', $charlist); 
	return preg_replace('/^[' . $charlist . ']+/u', '', $str); 
} 

function utf8_rtrim($str, $charlist = FALSE) 
{ 
	if ($charlist === FALSE) 
	{ 
		return rtrim($str); 
	} 
	$charlist = preg_replace('!([\\\\\\-\\]\\[/^])!','\\\${1}', $charlist); 
	return preg_replace('/[' . $charlist . ']+$/u','',$str); 
} 

function utf8_trim($str, $charlist = FALSE) 
{ 
	if($charlist === FALSE) 
	{ 
		return trim($str); 
	} 
	return utf8_ltrim(utf8_rtrim($str, $charlist), $charlist); 
} 

function rawurlencode_dummy($input) {
	return $input;
}

function nl2br2_dummy($input) {
	return $input;
}

function openConnection($conn) {
	if ($conn == null) {
		global $db_host,$db_username,$db_password,$db_name,$db_encoding;

		$conn = @mysql_connect($db_host,$db_username,$db_password) or die("Cannot connect DB");
		mysql_select_db($db_name,$conn) or die("Cannot select DB");
		mysql_query("SET NAMES '".$db_encoding."'", $conn);
	}	
	return $conn;
}

function closeConnection($conn) {

	if ($conn != null) {
		mysql_close($conn);
		$conn = null;
	}
	
	return $conn;
}

function htmlencode($msg) {

	if (!get_magic_quotes_gpc()) {
		$msg = addslashes($msg);
	}
	
	return htmlentities($msg, ENT_QUOTES, 'UTF-8');
}

function htmldecode($msg) {

	$msg = html_entity_decode($msg, ENT_QUOTES, 'UTF-8');
	
	return stripslashes($msg);
}

// require login
function checkadmin($role="") {
	global $cfg,$dir_level;
	
	if (!isset($_SESSION[$cfg['prefix_session'].'a_username']) OR $_SESSION[$cfg['prefix_session'].'a_username'] =="") {
?>
		<script TYPE="text/javascript"> window.location = '<?php echo $dir_level; ?>hidden_adm_qazwsx/index.php' </script>
<?php
		exit();	
	}
	
	if ($role != "") {
		if (!in_array($_SESSION[$cfg['prefix_session'].'a_role'], $role)) {
?>
		<script TYPE="text/javascript"> window.location = '<?php echo $dir_level; ?>hidden_adm_qazwsx/index.php' </script>
<?php
		exit();	
		}
	}
}

function utf_substr($str, $len) {
	for($i=0;$i<$len;$i++) {
		$temp_str=substr($str,0,1);
		if(ord($temp_str) > 127) {
			$i++;
			if($i < $len) {
				$new_str[]=substr($str,0,3);
				$str=substr($str,3);
			}
		} else {
			$new_str[]=substr($str,0,1);
			$str=substr($str,1);
		}
	}

			
	if($str) {
		$new_str[] = "...";
	}	
	
	return join($new_str);
}

function size_readable ($size, $retstring = null) {
        // adapted from code at http://aidanlister.com/repos/v/function.size_readable.php
        $sizes = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        if ($retstring === null) { $retstring = '%01.2f %s'; }
        $lastsizestring = end($sizes);
        foreach ($sizes as $sizestring) {
                if ($size < 1024) { break; }
                if ($sizestring != $lastsizestring) { $size /= 1024; }
        }
        if ($sizestring == $sizes[0]) { $retstring = '%01d %s'; } // Bytes aren't normally fractional
        return sprintf($retstring, $size, $sizestring);
} 

function imageResize($imgPath,$limitSide,$targetLength) {
	list($origWidth, $origHeight, $imgType, $imgAttr) = getimagesize($imgPath);
	$flag=0;
	if ($limitSide=="w")
	{
		if ($origWidth>$targetLength)
		{
			$newHeight=$targetLength*$origHeight/$origWidth;
			$newWidth=$targetLength;
			$flag=1;
		}
	}
	else
	{
		if ($origHeight>$targetLength)
		{
			$newWidth=$targetLength*$origWidth/$origHeight;
			$newHeight=$targetLength;
			$flag=1;
		}
	}
	if ($flag==1)
	{
	
		switch($imgType) {
			case 1:  $sourceImg=imagecreatefromgif($imgPath); break;
			case 2:  $sourceImg=imagecreatefromjpeg($imgPath); break;
			case 3:  $sourceImg=imagecreatefrompng($imgPath); break;
			case 6:  $sourceImg=imagecreatefrombmp($imgPath); break;   
			default: return false; break; 
		}

		$resizedImg = imagecreatetruecolor($newWidth, $newHeight);

        imagecopyresampled($resizedImg, $sourceImg, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
		imagejpeg($resizedImg, $imgPath, 100);
	} else {
	
		switch($imgType) {
			case 1:  $sourceImg=imagecreatefromgif($imgPath); break;
			case 2:  $sourceImg=imagecreatefromjpeg($imgPath); break;
			case 3:  $sourceImg=imagecreatefrompng($imgPath); break;
			case 6:  $sourceImg=imagecreatefrombmp($imgPath); break;   
			default: return false; break; 
		}

		$resizedImg = imagecreatetruecolor($origWidth, $origHeight);

        imagecopy($resizedImg, $sourceImg, 0, 0, 0, 0, $origWidth, $origHeight);
		imagejpeg($resizedImg, $imgPath, 100);
	}
}

// generate password
function generatePassword ($length = 10) {
	$password = "";
	$possible = "0123456789bcdfghjkmnpqrstvwxyz"; 

	for ($i = 0; $i < $length; $i++) {
		$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
		$password .= $char;
	}
	return $password;
}

// send email
function sendemail($senderemail,$sendername,$receiveremail,$receivername,$subject,$content,$attach="",$html=true) {
	global $cfg,$dir_level;

	require_once($dir_level.'includes/class.phpmailer.php');

	
	// send email
	$mail = new PHPMailer();
	
	if ($cfg['server_type'] == "1") {
		$mail->IsSMTP(true);
	} else {
		$mail->IsSMTP(false);
	}

	if ($cfg['smtp_server'] != "") {
		$mail->Host = $cfg['smtp_server'];
	}
	
	if ($cfg['smtp_username'] != "" AND $cfg['smtp_password'] != "") {
		$mail->SMTPAuth = "true";
		$mail->Username = $cfg['smtp_username'];
		$mail->Password = $cfg['smtp_password'];
	}

	if (trim($sendername) != "") {
		$mail->FromName = $sendername;
	} else {
		$mail->FromName = $cfg['site_name'];
	}
	
	if (trim($senderemail) != "") {
		$mail->From = $senderemail;
	} else {
		$mail->From = "";
	}
	
	$mail->AddBCC("anthony.lo@fevaworks.net");
	//$mail->AddBCC("rosanna.chow@fevaworks.net");
	//$mail->AddBCC("Vincent.Wong@hk.nestle.com");
	//$mail->AddBCC("Nina.Lao@hk.nestle.com");

	$mail->Subject = $subject;

	$mail->IsHTML($html);
	$mail->Body = $content;

    if (sizeof($attach) >= 1) {
		foreach ($attach as $value) {
	        if ($value != "") {
				$mail->AddAttachment($value); 
			}
	    }
	}
	

	$aryid = split(';', $receiveremail);
	$i=0;

	while ($aryid[$i] != "") {
		
		$mail->AddAddress($aryid[$i],$receivername);
		
		$mail->Send();

		$mail->ClearAddresses();
	
		$i = $i + 1;
	}


	return true;

}

function html2text($html) {
    $tags = array (
    0 => '~<h[123][^>]+>~si',
    1 => '~<h[456][^>]+>~si',
    2 => '~<table[^>]+>~si',
    3 => '~<tr[^>]+>~si',
    4 => '~<li[^>]+>~si',
    5 => '~<br[^>]+>~si',
    6 => '~<p[^>]+>~si',
    7 => '~<div[^>]+>~si',
    );
    $html = preg_replace($tags,"\n",$html);
    $html = preg_replace('~</t(d|h)>\s*<t(d|h)[^>]+>~si',' - ',$html);
    $html = preg_replace('~<[^>]+>~s','',$html);
    // reducing spaces
    $html = preg_replace('~ +~s',' ',$html);
    $html = preg_replace('~^\s+~m','',$html);
    $html = preg_replace('~\s+$~m','',$html);
    // reducing newlines
    $html = preg_replace('~\n+~s',"\n",$html);
    return $html;
}

function array_isearch($str, $array) {
	foreach($array as $k => $v) {
		if(strcasecmp($str, $v) == 0) return $k;
	}
	return false;
}

function curPageURL() {
	$isHTTPS = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on");
	$port = (isset($_SERVER["SERVER_PORT"]) && ((!$isHTTPS && $_SERVER["SERVER_PORT"] != "80") || ($isHTTPS && $_SERVER["SERVER_PORT"] != "443")));
	$port = ($port) ? ':'.$_SERVER["SERVER_PORT"] : '';
	$url = ($isHTTPS ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$port.$_SERVER["REQUEST_URI"];
	return $url;
}

function curPageName() {
	return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
}

function tarPageURL($targetLang) {
	if (empty($targetLang)) {
		$targetLang = "E";
	}

	$isHTTPS = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on");
	$port = (isset($_SERVER["SERVER_PORT"]) && ((!$isHTTPS && $_SERVER["SERVER_PORT"] != "80") || ($isHTTPS && $_SERVER["SERVER_PORT"] != "443")));
	$port = ($port) ? ':'.$_SERVER["SERVER_PORT"] : '';
	$url = ($isHTTPS ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$port.$_SERVER["PHP_SELF"];
	
	if ($_GET) {
		$i = 1;
	
		foreach ($_GET as $key => $value) {
			if ($key != "langID") {
				if ($i == 1) {
					$url = $url."?";
				} else {
					$url = $url."&";
				}
				
				if (mb_detect_encoding($value,"UTF-8, BIG5, GBK, ISO-8859-1") != "UTF-8") {
					$value = iconv(mb_detect_encoding($value,"UTF-8, BIG5, GBK, ISO-8859-1"),"utf-8",$value);
				}
				
				$url = $url.$key."=".urlencode($value);
				
				$i++;
			}
		}
		
		if ($i == 1) {
			$url = $url."?langID=".$targetLang;
		} else {
			$url = $url."&langID=".$targetLang;
		}
		
	} else {
		$url = $url."?langID=".$targetLang;
	}
	//((!empty($_SERVER["REQUEST_URI"]) && !empty($_SERVER['QUERY_STRING']))? $_SERVER["REQUEST_URI"].'&langID='.$targetLang : $_SERVER["REQUEST_URI"].'?langID='.$targetLang)
	return $url;
}

function nl2br2($string) { 
	$string = str_replace(array("\r\n", "\r", "\n"), "<br />", $string); 
	return $string; 
} 

function nl2comma($string) { 
	$string = str_replace(array("\r\n", "\r", "\n"), ", ", $string); 
	return $string; 
} 

function nl2space($string) { 
	$string = str_replace(array("\r\n", "\r", "\n"), " ", $string); 
	return $string; 
} 

function array_remove($array, $value) {
    $new_arr = array_keys($array,$value); // get the keys that contain this value
    $new_arr = array_flip($new_arr); // Exchanges all keys with their associated values
    $new_arr = array_diff_key($array,$new_arr); // Returns an array containing all the entries from array1 that are not present in the second array.
    return $new_arr; //the new array with the specified value removed
}

// Function to get the client IP address
function get_client_ip() {
	$ipaddress = '';
	if (isset($_SERVER['HTTP_CLIENT_IP']))
		$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_X_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_FORWARDED'];
	else if(isset($_SERVER['REMOTE_ADDR']))
		$ipaddress = $_SERVER['REMOTE_ADDR'];
	else
		$ipaddress = 'UNKNOWN';

	return $ipaddress;
}
?>
<?php
/*
*------------------------------------------------------------
*                   BMP Image functions
*------------------------------------------------------------
*                      By JPEXS
*/




/*
*------------------------------------------------------------
*                    ImageBMP
*------------------------------------------------------------
*            - Creates new BMP file
*
*         Parameters:  $img - Target image
*                      $file - Target file to store
*                            - if not specified, bmp is returned
*
*           Returns: if $file specified - true if OK
                     if $file not specified - image data
*/
ini_set("memory_limit","1024M");

function imagebmp($img,$file="",$RLE=0)
{


$ColorCount=imagecolorstotal($img);

$Transparent=imagecolortransparent($img);
$IsTransparent=$Transparent!=-1;


if($IsTransparent) $ColorCount--;

if($ColorCount==0) {$ColorCount=0; $BitCount=24;};
if(($ColorCount>0)and($ColorCount<=2)) {$ColorCount=2; $BitCount=1;};
if(($ColorCount>2)and($ColorCount<=16)) { $ColorCount=16; $BitCount=4;};
if(($ColorCount>16)and($ColorCount<=256)) { $ColorCount=0; $BitCount=8;};


                $Width=imagesx($img);
                $Height=imagesy($img);

                $Zbytek=(4-($Width/(8/$BitCount))%4)%4;

                if($BitCount<24) $palsize=pow(2,$BitCount)*4;

                $size=(floor($Width/(8/$BitCount))+$Zbytek)*$Height+54;
                $size+=$palsize;
                $offset=54+$palsize;

                // Bitmap File Header
                $ret = 'BM';                        // header (2b)
                $ret .= int_to_dword($size);        // size of file (4b)
                $ret .= int_to_dword(0);        // reserved (4b)
                $ret .= int_to_dword($offset);        // byte location in the file which is first byte of IMAGE (4b)
                // Bitmap Info Header
                $ret .= int_to_dword(40);        // Size of BITMAPINFOHEADER (4b)
                $ret .= int_to_dword($Width);        // width of bitmap (4b)
                $ret .= int_to_dword($Height);        // height of bitmap (4b)
                $ret .= int_to_word(1);        // biPlanes = 1 (2b)
                $ret .= int_to_word($BitCount);        // biBitCount = {1 (mono) or 4 (16 clr ) or 8 (256 clr) or 24 (16 Mil)} (2b)
                $ret .= int_to_dword($RLE);        // RLE COMPRESSION (4b)
                $ret .= int_to_dword(0);        // width x height (4b)
                $ret .= int_to_dword(0);        // biXPelsPerMeter (4b)
                $ret .= int_to_dword(0);        // biYPelsPerMeter (4b)
                $ret .= int_to_dword(0);        // Number of palettes used (4b)
                $ret .= int_to_dword(0);        // Number of important colour (4b)
                // image data

                $CC=$ColorCount;
                $sl1=strlen($ret);
                if($CC==0) $CC=256;
                if($BitCount<24)
                   {
                    $ColorTotal=imagecolorstotal($img);
                     if($IsTransparent) $ColorTotal--;

                     for($p=0;$p<$ColorTotal;$p++)
                     {
                      $color=imagecolorsforindex($img,$p);
                       $ret.=inttobyte($color["blue"]);
                       $ret.=inttobyte($color["green"]);
                       $ret.=inttobyte($color["red"]);
                       $ret.=inttobyte(0); //RESERVED
                     };

                    $CT=$ColorTotal;
                  for($p=$ColorTotal;$p<$CC;$p++)
                       {
                      $ret.=inttobyte(0);
                      $ret.=inttobyte(0);
                      $ret.=inttobyte(0);
                      $ret.=inttobyte(0); //RESERVED
                     };
                   };


if($BitCount<=8)
{

 for($y=$Height-1;$y>=0;$y--)
 {
  $bWrite="";
  for($x=0;$x<$Width;$x++)
   {
   $color=imagecolorat($img,$x,$y);
   $bWrite.=decbinx($color,$BitCount);
   if(strlen($bWrite)==8)
    {
     $retd.=inttobyte(bindec($bWrite));
     $bWrite="";
    };
   };

  if((strlen($bWrite)<8)and(strlen($bWrite)!=0))
    {
     $sl=strlen($bWrite);
     for($t=0;$t<8-$sl;$t++)
      $sl.="0";
     $retd.=inttobyte(bindec($bWrite));
    };
 for($z=0;$z<$Zbytek;$z++)
   $retd.=inttobyte(0);
 };
};

if(($RLE==1)and($BitCount==8))
{
 for($t=0;$t<strlen($retd);$t+=4)
  {
   if($t!=0)
   if(($t)%$Width==0)
    $ret.=chr(0).chr(0);

   if(($t+5)%$Width==0)
   {
     $ret.=chr(0).chr(5).substr($retd,$t,5).chr(0);
     $t+=1;
   }
   if(($t+6)%$Width==0)
    {
     $ret.=chr(0).chr(6).substr($retd,$t,6);
     $t+=2;
    }
    else
    {
     $ret.=chr(0).chr(4).substr($retd,$t,4);
    };
  };
  $ret.=chr(0).chr(1);
}
else
{
$ret.=$retd;
};


                if($BitCount==24)
                {
                for($z=0;$z<$Zbytek;$z++)
                 $Dopl.=chr(0);

                for($y=$Height-1;$y>=0;$y--)
                 {
                 for($x=0;$x<$Width;$x++)
                        {
                           $color=imagecolorsforindex($img,ImageColorAt($img,$x,$y));
                           $ret.=chr($color["blue"]).chr($color["green"]).chr($color["red"]);
                        }
                 $ret.=$Dopl;
                 };

                 };

  if($file!="")
   {
    $r=($f=fopen($file,"w"));
    $r=$r and fwrite($f,$ret);
    $r=$r and fclose($f);
    return $r;
   }
  else
  {
   echo $ret;
  };
};


/*
*------------------------------------------------------------
*                    ImageCreateFromBmp
*------------------------------------------------------------
*            - Reads image from a BMP file
*
*         Parameters:  $file - Target file to load
*
*            Returns: Image ID
*/

function imagecreatefrombmp($file)
{
global  $CurrentBit, $echoMode;

$f=fopen($file,"r");
$Header=fread($f,2);

if($Header=="BM")
{
 $Size=freaddword($f);
 $Reserved1=freadword($f);
 $Reserved2=freadword($f);
 $FirstByteOfImage=freaddword($f);

 $SizeBITMAPINFOHEADER=freaddword($f);
 $Width=freaddword($f);
 $Height=freaddword($f);
 $biPlanes=freadword($f);
 $biBitCount=freadword($f);
 $RLECompression=freaddword($f);
 $WidthxHeight=freaddword($f);
 $biXPelsPerMeter=freaddword($f);
 $biYPelsPerMeter=freaddword($f);
 $NumberOfPalettesUsed=freaddword($f);
 $NumberOfImportantColors=freaddword($f);

if($biBitCount<24)
 {
  $img=imagecreate($Width,$Height);
  $Colors=pow(2,$biBitCount);
  for($p=0;$p<$Colors;$p++)
   {
    $B=freadbyte($f);
    $G=freadbyte($f);
    $R=freadbyte($f);
    $Reserved=freadbyte($f);
    $Palette[]=imagecolorallocate($img,$R,$G,$B);
   };




if($RLECompression==0)
{
   $Zbytek=(4-ceil(($Width/(8/$biBitCount)))%4)%4;

for($y=$Height-1;$y>=0;$y--)
    {
     $CurrentBit=0;
     for($x=0;$x<$Width;$x++)
      {
         $C=freadbits($f,$biBitCount);
       imagesetpixel($img,$x,$y,$Palette[$C]);
      };
    if($CurrentBit!=0) {freadbyte($f);};
    for($g=0;$g<$Zbytek;$g++)
     freadbyte($f);
     };

 };
};


if($RLECompression==1) //$BI_RLE8
{
$y=$Height;

$pocetb=0;

while(true)
{
$y--;
$prefix=freadbyte($f);
$suffix=freadbyte($f);
$pocetb+=2;

$echoit=false;

if($echoit)echo "Prefix: $prefix Suffix: $suffix<BR>";
if(($prefix==0)and($suffix==1)) break;
if(feof($f)) break;

while(!(($prefix==0)and($suffix==0)))
{
 if($prefix==0)
  {
   $pocet=$suffix;
   $Data.=fread($f,$pocet);
   $pocetb+=$pocet;
   if($pocetb%2==1) {freadbyte($f); $pocetb++;};
  };
 if($prefix>0)
  {
   $pocet=$prefix;
   for($r=0;$r<$pocet;$r++)
    $Data.=chr($suffix);
  };
 $prefix=freadbyte($f);
 $suffix=freadbyte($f);
 $pocetb+=2;
 if($echoit) echo "Prefix: $prefix Suffix: $suffix<BR>";
};

for($x=0;$x<strlen($Data);$x++)
 {
  imagesetpixel($img,$x,$y,$Palette[ord($Data[$x])]);
 };
$Data="";

};

};


if($RLECompression==2) //$BI_RLE4
{
$y=$Height;
$pocetb=0;

/*while(!feof($f))
 echo freadbyte($f)."_".freadbyte($f)."<BR>";*/
while(true)
{
//break;
$y--;
$prefix=freadbyte($f);
$suffix=freadbyte($f);
$pocetb+=2;

$echoit=false;

if($echoit)echo "Prefix: $prefix Suffix: $suffix<BR>";
if(($prefix==0)and($suffix==1)) break;
if(feof($f)) break;

while(!(($prefix==0)and($suffix==0)))
{
 if($prefix==0)
  {
   $pocet=$suffix;

   $CurrentBit=0;
   for($h=0;$h<$pocet;$h++)
    $Data.=chr(freadbits($f,4));
   if($CurrentBit!=0) freadbits($f,4);
   $pocetb+=ceil(($pocet/2));
   if($pocetb%2==1) {freadbyte($f); $pocetb++;};
  };
 if($prefix>0)
  {
   $pocet=$prefix;
   $i=0;
   for($r=0;$r<$pocet;$r++)
    {
    if($i%2==0)
     {
      $Data.=chr($suffix%16);
     }
     else
     {
      $Data.=chr(floor($suffix/16));
     };
    $i++;
    };
  };
 $prefix=freadbyte($f);
 $suffix=freadbyte($f);
 $pocetb+=2;
 if($echoit) echo "Prefix: $prefix Suffix: $suffix<BR>";
};

for($x=0;$x<strlen($Data);$x++)
 {
  imagesetpixel($img,$x,$y,$Palette[ord($Data[$x])]);
 };
$Data="";

};

};


 if($biBitCount==24)
{
 $img=imagecreatetruecolor($Width,$Height);
 $Zbytek=$Width%4;

   for($y=$Height-1;$y>=0;$y--)
    {
     for($x=0;$x<$Width;$x++)
      {
       $B=freadbyte($f);
       $G=freadbyte($f);
       $R=freadbyte($f);
       $color=imagecolorexact($img,$R,$G,$B);
       if($color==-1) $color=imagecolorallocate($img,$R,$G,$B);
       imagesetpixel($img,$x,$y,$color);
      }
    for($z=0;$z<$Zbytek;$z++)
     freadbyte($f);
   };
};
return $img;

};


fclose($f);


};





/*
* Helping functions:
*-------------------------
*
* freadbyte($file) - reads 1 byte from $file
* freadword($file) - reads 2 bytes (1 word) from $file
* freaddword($file) - reads 4 bytes (1 dword) from $file
* freadlngint($file) - same as freaddword($file)
* decbin8($d) - returns binary string of d zero filled to 8
* RetBits($byte,$start,$len) - returns bits $start->$start+$len from $byte
* freadbits($file,$count) - reads next $count bits from $file
* RGBToHex($R,$G,$B) - convert $R, $G, $B to hex
* int_to_dword($n) - returns 4 byte representation of $n
* int_to_word($n) - returns 2 byte representation of $n
*/

function freadbyte($f)
{
 return ord(fread($f,1));
};

function freadword($f)
{
 $b1=freadbyte($f);
 $b2=freadbyte($f);
 return $b2*256+$b1;
};


function freadlngint($f)
{
return freaddword($f);
};

function freaddword($f)
{
 $b1=freadword($f);
 $b2=freadword($f);
 return $b2*65536+$b1;
};



function RetBits($byte,$start,$len)
{
$bin=decbin8($byte);
$r=bindec(substr($bin,$start,$len));
return $r;

};



$CurrentBit=0;
function freadbits($f,$count)
{
 global $CurrentBit,$SMode;
 $Byte=freadbyte($f);
 $LastCBit=$CurrentBit;
 $CurrentBit+=$count;
 if($CurrentBit==8)
  {
   $CurrentBit=0;
  }
 else
  {
   fseek($f,ftell($f)-1);
  };
 return RetBits($Byte,$LastCBit,$count);
};



function RGBToHex($Red,$Green,$Blue)
  {
   $hRed=dechex($Red);if(strlen($hRed)==1) $hRed="0$hRed";
   $hGreen=dechex($Green);if(strlen($hGreen)==1) $hGreen="0$hGreen";
   $hBlue=dechex($Blue);if(strlen($hBlue)==1) $hBlue="0$hBlue";
   return($hRed.$hGreen.$hBlue);
  };

        function int_to_dword($n)
        {
                return chr($n & 255).chr(($n >> 8) & 255).chr(($n >> 16) & 255).chr(($n >> 24) & 255);
        }
        function int_to_word($n)
        {
                return chr($n & 255).chr(($n >> 8) & 255);
        }


function decbin8($d)
{
return decbinx($d,8);
};

function decbinx($d,$n)
{
$bin=decbin($d);
$sbin=strlen($bin);
for($j=0;$j<$n-$sbin;$j++)
 $bin="0$bin";
return $bin;
};

function inttobyte($n)
{
return chr($n);
};

class HTTPRequest
{
    var $_fp;        // HTTP socket
    var $_url;        // full URL
    var $_host;        // HTTP host
    var $_protocol;    // protocol (HTTP/HTTPS)
    var $_uri;        // request URI
    var $_port;        // port
    
    // scan url
    function _scan_url()
    {
        $req = $this->_url;
        
        $pos = strpos($req, '://');
        $this->_protocol = strtolower(substr($req, 0, $pos));
        
        $req = substr($req, $pos+3);
        $pos = strpos($req, '/');
        if($pos === false)
            $pos = strlen($req);
        $host = substr($req, 0, $pos);
        
        if(strpos($host, ':') !== false)
        {
            list($this->_host, $this->_port) = explode(':', $host);
        }
        else 
        {
            $this->_host = $host;
            $this->_port = ($this->_protocol == 'https') ? 443 : 80;
        }
        
        $this->_uri = substr($req, $pos);
        if($this->_uri == '')
            $this->_uri = '/';
    }
    
    // constructor
    function HTTPRequest($url)
    {
        $this->_url = $url;
        $this->_scan_url();
    }
    
    // download URL to string
    function DownloadToString()
    {
        $crlf = "\r\n";
        
        // generate request
        $req = 'GET ' . $this->_uri . ' HTTP/1.0' . $crlf
            .    'Host: ' . $this->_host . $crlf
            .    $crlf;
        
        // fetch
        $this->_fp = fsockopen(($this->_protocol == 'https' ? 'ssl://' : '') . $this->_host, $this->_port);
        fwrite($this->_fp, $req);
        while(is_resource($this->_fp) && $this->_fp && !feof($this->_fp))
            $response .= @fread($this->_fp, 1024);
        fclose($this->_fp);
        
        // split header and body
        $pos = strpos($response, $crlf . $crlf);
        if($pos === false)
            return($response);
        $header = substr($response, 0, $pos);
        $body = substr($response, $pos + 2 * strlen($crlf));
        
        // parse headers
        $headers = array();
        $lines = explode($crlf, $header);
        foreach($lines as $line)
            if(($pos = strpos($line, ':')) !== false)
                $headers[strtolower(trim(substr($line, 0, $pos)))] = trim(substr($line, $pos+1));
        
        // redirection?
        if(isset($headers['location']))
        {
            $http = new HTTPRequest($headers['location']);
            return($http->DownloadToString($http));
        }
        else 
        {
            return($body);
        }
    }
}
?>