<?php include 'templates/commonvar.php'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>GERBER - Gerber® 寶寶食「相」大賽</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content=" ">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="GERBER - Gerber® 寶寶食「相」大賽" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/aboutus/video_img.jpg" />
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="../css/hover.css">
    <link rel="stylesheet" type="text/css" href="../css/animate.css">
    <link rel="stylesheet" type="text/css" href="../plugin/swiper/v3/swiper.min.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/layout.css">
    <script src="../js/jquery.min.js"></script>

    <!-- Vendor: Javascripts -->
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mobile.custom.min.js"></script>
    <script src="../plugin/swiper/v3/swiper.min.js"></script>
    <script src="../plugin/checkUserAgent/checkUserAgent.js"></script>
    <script src="https://use.fontawesome.com/60efdea8b0.js"></script>
    <!-- Our Website Javascripts -->
    <script src="../js/main.js"></script>
    <?php include 'templates/preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<?php include 'templates/header_contest.php'; ?>
<span id="btn" style="dispaly:none"></span>
<div class="container">
    <section id="success">
		<h1></h1>
        <p>
            恭喜您已成功遞交問卷！<br>
            您可獲得$30購物禮券乙張，詳情請查閱電子郵件。
        </p>
        <img src="../images/contest/success.png">

        <div class="btn-wrap row">
            <div class="link col col-xs-12 col-sm-6"><a class="hvr-fade" href="javascript:void(0);" onclick="shareFB('<?php echo $baseUrl."contest"; ?>');">分享至Facebook</a></div>
            <div class="link col col-xs-12 col-sm-6"><a class="hvr-fade" href="album">前往「回味每刻」相簿</i></a></div>
        </div>
    </section>
</div>

<?php include 'templates/footer.php'; ?>
<link rel="stylesheet" href="../css/contest_form.css">
<script src="../js/contest.js"></script>

</body>
</html>
