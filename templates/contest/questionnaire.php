<?php include 'templates/commonvar.php'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>GERBER - Gerber® 寶寶食「相」大賽</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content=" ">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="GERBER - Gerber® 寶寶食「相」大賽" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/aboutus/video_img.jpg" />
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="../css/hover.css">
    <link rel="stylesheet" type="text/css" href="../css/animate.css">
    <link rel="stylesheet" type="text/css" href="../plugin/swiper/v3/swiper.min.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/layout.css">
    <script src="../js/jquery.min.js"></script>

    <!-- Vendor: Javascripts -->
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mobile.custom.min.js"></script>
    <script src="../plugin/swiper/v3/swiper.min.js"></script>
    <script src="../plugin/checkUserAgent/checkUserAgent.js"></script>
    <script src="https://use.fontawesome.com/60efdea8b0.js"></script>
    <!-- Our Website Javascripts -->
    <script src="../js/main.js"></script>
    <?php include 'templates/preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<?php include 'templates/header_contest.php'; ?>
<span id="btn" style="dispaly:none"></span>
<div class="container">
	<form name="gerberform" id="gerberform" method="post" action="<?php echo $baseUrl ?>php/save_Questionnaire.php" enctype="multipart/form-data" target="process">
    <section id="questionnaire" class="form-step">
        <h1>上傳成功!</h1>
        <hr>
        <p>
            謝謝您和寶寶的參與！<br>
            最後一步：只要填妥問卷即可獲得$30購物禮券乙張。
        </p>
        <img src="../images/contest/success.png">
	
		<div class="seperate"><span>問卷調查</span></div>
		<div class="remarks">必須完成問卷方可獲得$30購物禮券，詳情請查閱電子郵件。</div>
		<div class="form">
                <div class="form-group row">
                    <div class="col-sm-12">
                        1. 您有沒有見過GERBER® Enjoy Moment 短片？
                        <br>
                        <input type="checkbox" name="q1[]" id="q1_1" class="css-checkbox" value="1" />
                        <label for="q1_1" class="css-label">有
                            <span class="error error-checkbox"></span>
                        </label>
                        <input type="checkbox" name="q1[]" id="q1_2" class="css-checkbox" value="2" />
                        <label for="q1_2" class="css-label">沒有
                            <span class="error error-checkbox"></span>
                        </label>
                    </div>
                    <div class="col-sm-12">
                        2. 整體來說，您有多喜歡這個GERBER®的短片呢？
                        <br>
                        <input type="checkbox" name="q2[]" id="q2_1" class="css-checkbox" value="1" />
                        <label for="q2_1" class="css-label">完全不喜歡
                            <span class="error error-checkbox"></span>
                        </label>
                        <input type="checkbox" name="q2[]" id="q2_2" class="css-checkbox" value="2" />
                        <label for="q2_2" class="css-label">不喜歡
                            <span class="error error-checkbox"></span>
                        </label>
                        <input type="checkbox" name="q2[]" id="q2_3" class="css-checkbox" value="3" />
                        <label for="q2_3" class="css-label">不知道
                            <span class="error error-checkbox"></span>
                        </label>
                        <input type="checkbox" name="q2[]" id="q2_4" class="css-checkbox" value="4" />
                        <label for="q2_4" class="css-label">喜歡
                            <span class="error error-checkbox"></span>
                        </label>
                        <input type="checkbox" name="q2[]" id="q2_5" class="css-checkbox" value="5" />
                        <label for="q2_5" class="css-label">極之喜歡
                            <span class="error error-checkbox"></span>
                        </label>
                    </div>
                    <div class="col-sm-12">
                        3. 請問您有多同意這短片和相片分享活動能夠傳遞GERBER®是寶寶最享受的嬰幼兒食品這個訊息呢？？
                        <br>
                        <input type="checkbox" name="q3[]" id="q3_1" class="css-checkbox" value="1" />
                        <label for="q3_1" class="css-label">完全不同意
                            <span class="error error-checkbox"></span>
                        </label>
                        <input type="checkbox" name="q3[]" id="q3_2" class="css-checkbox" value="2" />
                        <label for="q3_2" class="css-label">不同意
                            <span class="error error-checkbox"></span>
                        </label>
                        <input type="checkbox" name="q3[]" id="q3_3" class="css-checkbox" value="3" />
                        <label for="q3_3" class="css-label">不知道
                            <span class="error error-checkbox"></span>
                        </label>
                        <input type="checkbox" name="q3[]" id="q3_4" class="css-checkbox" value="4" />
                        <label for="q3_4" class="css-label">同意
                            <span class="error error-checkbox"></span>
                        </label>
                        <input type="checkbox" name="q3[]" id="q3_5" class="css-checkbox" value="5" />
                        <label for="q3_5" class="css-label">極之同意
                            <span class="error error-checkbox"></span>
                        </label>
                    </div>
                    <div class="col-sm-12">
                        4. GERBER® Enjoy Moment 短片和相片分享活動有多影響到你購買GERBER®的嬰幼兒食品的興趣呢？
                        <br>
                        <input type="checkbox" name="q4[]" id="q4_1" class="css-checkbox" value="1" />
                        <label for="q4_1" class="css-label">下降了很多
                            <span class="error error-checkbox"></span>
                        </label>
                        <input type="checkbox" name="q4[]" id="q4_2" class="css-checkbox" value="2" />
                        <label for="q4_2" class="css-label">下降
                            <span class="error error-checkbox"></span>
                        </label>
                        <input type="checkbox" name="q4[]" id="q4_3" class="css-checkbox" value="3" />
                        <label for="q4_3" class="css-label">不知道
                            <span class="error error-checkbox"></span>
                        </label>
                        <input type="checkbox" name="q4[]" id="q4_4" class="css-checkbox" value="4" />
                        <label for="q4_4" class="css-label">增加
                            <span class="error error-checkbox"></span>
                        </label>
                        <input type="checkbox" name="q4[]" id="q4_5" class="css-checkbox" value="5" />
                        <label for="q4_5" class="css-label">增加了很多
                            <span class="error error-checkbox"></span>
                        </label>
                    </div>
                </div>
		</div>
			
        <div class="btn-wrap row">
            <div class="link col col-sm-12"><a class="hvr-fade" href="" id="q_submit">提交問卷</a></div>
        </div>
    </section>
	</form>
</div>

<div style="visibility:hidden;">
<iframe name="process" style="width:1px;height:1px;"></iframe>
</div>

<?php include 'templates/footer.php'; ?>
<link rel="stylesheet" href="../css/contest_form.css">
<script src="../js/contest.js"></script>

</body>
</html>
