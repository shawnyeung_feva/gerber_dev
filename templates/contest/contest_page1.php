<?php include 'templates/commonvar.php'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>GERBER - Gerber® 寶寶食「相」大賽</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content=" ">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="GERBER - Gerber® 寶寶食「相」大賽" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/aboutus/video_img.jpg" />
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="../css/hover.css">
    <link rel="stylesheet" type="text/css" href="../css/animate.css">
    <link rel="stylesheet" type="text/css" href="../plugin/swiper/v3/swiper.min.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/layout.css">
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery.tmpl.min.js"></script>
    <script src="../plugin/checkUserAgent/checkUserAgent.js"></script>
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.min.js"></script>
    <![endif]-->
    <!-- Vendor: Javascripts -->
    <script id="imageTemplate" type="text/x-jquery-tmpl"> 
        <div class="imageholder">
            <figure>
                <img src="${filePath}" alt="${fileName}"/>
                <figcaption>
                    ${fileName} <br/>
                    <span>Original Size: ${fileOriSize} KB</span><br/>
                    <span>Upload Size: ${fileUploadSize} KB</span>
                </figcaption>
            </figure>
        </div>
    </script>
    <script src="../plugin/dragdrop/modernizr.custom.js"></script>
    <script src="../plugin/dragdrop/dragdrop.js"></script>

    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mobile.custom.min.js"></script>
    <script src="../plugin/swiper/v3/swiper.min.js"></script>
	<script src="../plugin/enquire/enquire.min.js"></script>
    <script src="../plugin/checkUserAgent/checkUserAgent.js"></script>
    <script src="https://use.fontawesome.com/60efdea8b0.js"></script>
    <!-- Our Website Javascripts -->
    <script src="../js/main.js"></script>
    <?php include 'templates/preframe.php'; ?>
	
	<script>
function salesforceData(){
	$.post("<?php echo $baseUrl; ?>php/ajax_get_form_data.php", function(data){

		var areaObj = {}
		areaObj.hongkong = [];
		areaObj.kowloon = [];
		areaObj.newterritories = [];
		areaObj.outlyingisland = [];
		areaObj.macau = [];
		areaObj.china = [];
		areaObj.others = [];
		
		$.each(data.ary_district, function(index, value) {
			
			if (value.area_EN == "Hong Kong") {
				areaObj.hongkong.push([value.id,value.name_TC]);
			} else if (value.area_EN == "Kowloon") {
				areaObj.kowloon.push([value.id,value.name_TC]);
			} else if (value.area_EN == "New Territories") {
				areaObj.newterritories.push([value.id,value.name_TC]);
			} else if (value.area_EN == "Outlying Island") {
				areaObj.outlyingisland.push([value.id,value.name_TC]);
			} else if (value.area_EN == "Macau") {
				areaObj.macau.push([value.id,value.name_TC]);
			} else if (value.area_EN == "China") {
				areaObj.china.push([value.id,value.name_TC]);
			} else if (value.area_EN == "Others") {
				areaObj.others.push([value.id,value.name_TC]);
			}
		}); 

		var field_district = $('#field_district')
		$('#field_area').on('change', function() {
			$('option', field_district).each(function(index) {
				if (index>0) {
					$(this).remove()
				}
			})
			var area = $(this).val();
			area = area.replace(' ','').toLowerCase();
			var district = areaObj[area]
			var len = district.length;
			for (var i=0;i<len;i++) {
				field_district.append($('<option>', { 
					value:district[i][0],
					text:district[i][1]
				}));
			}
		})
		
	});
}
	</script>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<?php include 'templates/header_contest.php'; ?>
<span id="btn" style="dispaly:none"></span>
<div class="container">
	<form name="gerberform" id="gerberform" method="post" action="<?php echo $baseUrl ?>php/save_Form.php" enctype="multipart/form-data" target="process">
		<input type="hidden" name="photo_type"id="photo_type" value="">
		<input type="hidden" name="member_type" id="member_type" value="">
        <section id="step1" class="form-step">
            <div class="form-desc">
                <div class="step"><div>1</div></div>
                <div class="head">選擇參賽組別</div>
                <p>
                    參加者可自由選擇參賽組別，<br>
                    每張參賽作品中必須出現屬於該組別的Gerber®產品，否則將被取消資格。
                </p>
            </div>
            <div class="row">
                <div class="col col-xs-12 col-sm-4">
                    <div class="box" data-val="1">
                        <div class="top">
                            <img class="sty1" src="../images/contest/class1.png">
                        </div>
                        <div class="bottom">
                            <div class="title">享受第一口固體食物</div>
                            <div class="desc">
                            寶寶6個月起，初嚐固體食物，<br>
                            感覺新鮮、好奇
                            </div>
                        </div>
                        <div class="tick"><img src="../images/contest/tick.png"></div>
                    </div>
                </div>
                <div class="col col-xs-12 col-sm-4">
                    <div class="box" data-val="2">
                        <div class="top">
                            <img class="sty1" src="../images/contest/class2.png">
                        </div>
                        <div class="bottom">
                            <div class="title">享受新口味</div>
                            <div class="desc">
                                寶寶開始探索更多味道，<br>
                                例如是水果味
                            </div>
                        </div>
                        <div class="tick"><img src="../images/contest/tick.png"></div>
                    </div>
                </div>
                <div class="col col-xs-12 col-sm-4">
                    <div class="box" data-val="3">
                        <div class="top">
                            <img class="sty2" src="../images/contest/class3.png">
                        </div>
                        <div class="bottom">
                            <div class="title">享受有機食物</div>
                            <div class="desc">
                                有機產品系列獲美國農業部認可，<br>
                                為寶寶提供豐富營養
                            </div>
                        </div>
                        <div class="tick"><img src="../images/contest/tick.png"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 submit-button">
                    <button type="submit" class="btn btn-secondary hvr-fade">下一步&nbsp;&nbsp;<i class="fa fa-angle-right"></i></button>
                </div>
            </div>
            <!--div class="link"><a class="hvr-fade" href="step2">下一步&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a></div-->
        </section>

        <section id="step2" class="form-step" style="display:none;">
            <div class="form-desc">
                <div class="step"><div>2</div></div>
                <div class="head">上傳圖片</div>
                <p>
                    每位寶寶只能參加一個組別，<br>
                    每張參賽作品中必須出現屬於該組別的Gerber®產品，否則將被取消資格。
                </p>
            </div>
            <div class="row notIE">
                <div class="col col-xs-12 col-sm-6">
                    <section id="dragdrop-wrapper">
                        <div id="droparea">
                            <div class="dropareainner">
                                <div><img id="dragdrop" src="../images/contest/dragdrop.png"></div>
                                <div class="dropfiletext">
                                    <span>
                                    拖拉檔案上載<br>
                                    或<br>
                                    </span>
                                    <input id="uploadbtn" class="uploadbtn" type="button" value="選擇檔案"/>
                                </div>
                                <p id="err"></p>
                            </div>
                            <input name="upload" id="upload" type="file" accept=".jpg,.jpeg,.png"/>
                            <input name="uploaddata" id="uploaddata" type="hidden" value=""/>
                        </div>
                        <div id="result"></div>
                    </section>
                </div>
                <div class="col col-xs-12 col-sm-6">
                    <div id="preview">
                    </div>
                </div>
            </div>
            <div class="row IE">
                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="file-upload">
                            <label for="upload"></label>
                            <input type="file" name="upload" id="upload" name="upload">
                            <span class="file-name">
                                選擇檔案
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="step3" class="form-step" style="display:none;">
            <div class="form-desc">
                <div class="step"><div>3</div></div>
                <div class="head">填寫表格</div>
                <p>
                    每位寶寶只能參加一個組別，請確保資料必須正確
                </p>
            </div>
            <div class="seperate"><span>參賽寶寶資料</span></div>
            <div class="remarks"><sup>*</sup>此乃必需填寫之欄目，否則將無法處理閣下的申請。</div>
            <div class="form">
                <div class="form-group row">
                    <div class="col col-xs-12 col-sm-6">
                        <label class="form-control-label">姓名<sup>*</sup></label>
                        <input type="text" class="form-control" name="bb_name" id="bb_name" placeholder="">
                        <span class="error"></span>
                    </div>
                    <div class="col col-xs-12 col-sm-6">
                        <label class="form-control-label">性別<sup>*</sup></label>
                        <div>
                            <select name="bb_gender" id="bb_gender" class="form-control c-select">
                                <option value="" selected>請選擇</option>
                                <option value="M">男</option>
                                <option value="F">女</option>
                            </select>
                            <span class="error"></span>                     
                        </div>               
                    </div>
                    <div class="col col-xs-12" style="padding: 0;">
                        <label class="form-control-label" style="padding-left:15px;padding-right:15px;">出生日期<sup>*</sup></label>
                        <div class="form-group row">
                            <div class="col-xs-4">
                                <select name="bb_year" id="bb_year" class="form-control c-select">
                                <option value="" selected>年</option>
<?php
for ($i = (date("Y"));$i>=(date("Y") - 4);$i--) {
?>
<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
<?php
}
?>
                                </select>
                                <span class="error"></span>                                
                            </div>
                            <div class="col-xs-4">
                                <select name="bb_month" id="bb_month" class="form-control c-select">
                                <option value="" selected>月</option>
<option value="01">01</value>
<option value="02">02</value>
<option value="03">03</value>
<option value="04">04</value>
<option value="05">05</value>
<option value="06">06</value>
<option value="07">07</value>
<option value="08">08</value>
<option value="09">09</value>
<option value="10">10</value>
<option value="11">11</value>
<option value="12">12</value>
                                </select>
                                <span class="error"></span>                                
                            </div>
                            <div class="col-xs-4">
                                <select name="bb_day" id="bb_day" class="form-control c-select">
                                <option value="" selected>日</option>
<option value="01">01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
                                </select>
                                <span class="error"></span>                                
                            </div>
                        </div>
                    </div>
                    <div class="col col-xs-12">
                        <label class="form-control-label">香港身份證號碼<sup>*</sup><br class="visible-xs visible-sm">（首四位數字，不包括英文字母）</label>
                        <input type="text" class="form-control" name="hkid" id="hkid" placeholder="">
                        <span class="error"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 submit-button">
                        <button type="submit" class="btn btn-secondary hvr-fade">下一步&nbsp;&nbsp;<i class="fa fa-angle-right"></i></button>
                    </div>
                </div>
            </div>
        </section>
        <section id="step4" class="form-step" style="display:none;">
            <div class="form-desc">
                <div class="step"><div>4</div></div>
                <div class="head">填寫表格</div>
                <p>填妥及成功遞交表格，將自動成為「雀巢營養天地」媽咪會會員。</p>
                <div class="row">
                    <div class="col col-xs-12 col-sm-6">
                        <div class="box" data-val="1">
                            <div class="title">會員媽媽</div>
                            <div class="tick"><img src="../images/contest/tick.png"></div>
                        </div>
                    </div>
                    <div class="col col-xs-12 col-sm-6">
                        <div class="box" data-val="2">
                            <div class="title">非會員媽媽</div>
                            <div class="tick"><img src="../images/contest/tick.png"></div>
                        </div>
                    </div>
                </div>
                <p style="margin-top: 30px;">提交所需文件後更可獲贈精美入會禮物﹑定期收到網上營養資訊﹑健康講座﹑產品訂貨及送貨服務。 </p>
            </div>

            <div id="oldmember">
            <div class="seperate"><span>會員登入</span></div>
			<div class="form">
                <div class="form-group row">
                    <div class="col col-xs-12 col-sm-6">
                        <label class="form-control-label">登入電郵</label>
                        <input type="text" class="form-control" name="login_email" id="login_email" placeholder="">
                        <span class="error"></span>
                    </div>
                    <div class="col col-xs-12 col-sm-6">
                        <label class="form-control-label">登入密碼</label>
                        <input type="password" class="form-control" name="login_password" id="login_password" placeholder="">
                        <span class="error"></span>             
                    </div>
				</div>
                <div class="form-group row">
                    <div class="col-sm-12 submit-button">
                        <button type="submit" class="btn btn-secondary hvr-fade">提交</button>
                    </div>
                </div>
			</div>
			</div>
			
			<div id="newmember">
            <div class="seperate"><span>家長資料</span></div>
            <div class="remarks"><sup>*</sup>此乃必需填寫之欄目，否則將無法處理閣下的申請。</div>
            <div class="form">
                <div class="form-group row">
                    <div class="col col-xs-12 col-sm-6">
                        <label class="form-control-label">姓氏<sup>*</sup></label>
                        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="">
                        <span class="error"></span>
                    </div>
                    <div class="col col-xs-12 col-sm-6">
                        <label class="form-control-label">名字<sup>*</sup></label>
                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="">
                        <span class="error"></span>             
                    </div>
                    <div class="col col-xs-12 col-sm-6">
                        <label class="form-control-label">聯絡電話<sup>*</sup></label>
                        <input type="text" class="form-control" name="phone" id="phone" placeholder="" maxlength="8">
                        <span class="error"></span>
                    </div>
                    <div class="col col-xs-12 col-sm-6">
                        <label class="form-control-label">電郵地址<sup>*</sup></label>
                        <input type="text" class="form-control" name="email" id="email" placeholder="">
                        <span class="error"></span>             
                    </div>
                    <div class="col col-xs-12 col-sm-6">
                        <label class="form-control-label">地區<sup>*</sup></label>
                        <select name="field_area" id="field_area" class="form-control c-select">
							<option value="">請選擇</option>
							<option value="Hong Kong">香港</option>
							<option value="Kowloon">九龍</option>
							<option value="New Territories">新界</option>
							<option value="Outlying Island">離島</option>
							<option value="China">中國</option>
							<option value="Macau">澳門</option>
							<option value="Others">其他</option>
                        </select>
                        <span class="error"></span>                
                    </div>
                    <div class="col col-xs-12 col-sm-6">
                        <label class="form-control-label">分區<sup>*</sup></label>
                        <select name="field_district" id="field_district" class="form-control c-select">
							<option value="">請選擇</option>
                        </select>
                        <span class="error"></span>                
                    </div>
                    <div class="col col-xs-12">
                        <label class="form-control-label">通訊地址<sup>*</sup></label>
                        <input type="text" class="form-control" name="address" id="address" placeholder="">
                        <span class="error"></span>             
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        你是<sup>*</sup>
                        <br class="visible-xs">
                        <input type="checkbox" name="pregnant_mum" id="pregnant_mum" class="css-checkbox" value="1" />
                        <label for="pregnant_mum" class="css-label">懷孕中的媽媽
                            <span class="error error-checkbox"></span>
                        </label>
                        <input type="checkbox" name="children_mum" id="children_mum" class="css-checkbox" value="1" />
                        <label for="children_mum" class="css-label">已有孩子的媽媽
                            <span class="error error-checkbox"></span>
                        </label>
                    </div>
                </div>
                <div id="pregnant_date" class="form-group row" style="display:none;">
                     <div class="col-xs-12">
                        <label class="form-control-label">BB的預產期<sup>*</sup></label>
                    </div>
                    <div class="col-xs-4">
                        <select name="preg_bb_year" id="preg_bb_year" class="form-control c-select">
                        <option value="" selected>年</option>
<?php for ($year = date("Y"); $year < date("Y")+2; $year++): ?>
	<option value="<?php echo $year; ?>"><?php echo $year; ?></option>
<?php endfor; ?>
                        </select>
                        <span class="error"></span>                                
                    </div>
                    <div class="col-xs-4">
                        <select name="preg_bb_month" id="preg_bb_month" class="form-control c-select">
                        <option value="" selected>月</option>
<option value="01">01</value>
<option value="02">02</value>
<option value="03">03</value>
<option value="04">04</value>
<option value="05">05</value>
<option value="06">06</value>
<option value="07">07</value>
<option value="08">08</value>
<option value="09">09</value>
<option value="10">10</value>
<option value="11">11</value>
<option value="12">12</value>
                        </select>
                        <span class="error"></span>                                
                    </div>
                    <div class="col-xs-4">
                        <select name="preg_bb_day" id="preg_bb_day" class="form-control c-select">
                        <option value="" selected>日</option>
<option value="01">01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
                        </select>
                        <span class="error"></span>                                
                    </div>
                </div>
                <div id="children_date" class="form-group row" style="display:none;">
                     <div class="col-xs-12">
                        <label class="form-control-label">BB的出生日期<sup>*</sup></label>
                    </div>
                    <div class="col-xs-4">
                        <select name="children_bb_year" id="children_bb_year" class="form-control c-select">
                        <option value="" selected>年</option>
<?php for ($year = date("Y"); $year >= date("Y")-4; $year--): ?>
	<option value="<?php echo $year; ?>"><?php echo $year; ?></option>
<?php endfor; ?>
                        </select>
                        <span class="error"></span>                                
                    </div>
                    <div class="col-xs-4">
                        <select name="children_bb_month" id="children_bb_month" class="form-control c-select">
                        <option value="" selected>月</option>
<option value="01">01</value>
<option value="02">02</value>
<option value="03">03</value>
<option value="04">04</value>
<option value="05">05</value>
<option value="06">06</value>
<option value="07">07</value>
<option value="08">08</value>
<option value="09">09</value>
<option value="10">10</value>
<option value="11">11</value>
<option value="12">12</value>
                        </select>
                        <span class="error"></span>                                
                    </div>
                    <div class="col-xs-4">
                        <select name="children_bb_day" id="children_bb_day" class="form-control c-select">
                        <option value="" selected>日</option>
<option value="01">01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>
<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
                        </select>
                        <span class="error"></span>                                
                    </div>
                </div>
				
                <div class="form-group row">
                    <div class="col-xs-12">提交您BB的出生證明書</div>
                    <div class="col-xs-12">
                        <div class="file-upload">
							<span id="proof_text" class="file-name">
							
							</span>
							<div id="proof_select">
                            <label for="proof"></label>
                            <input type="file" id="proof" accept=".jpg,.pdf" name="proof">
							<input id="proof_url" name="proof_url" type="hidden" maxlength="255">
                            <span class="file-name">
                                從電腦上載（JPG 或 PDF檔，上限5MB）
                            </span>
							</div>
							
							
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-12">
                        <input type="checkbox" name="agreement" id="agreement" class="css-checkbox" value="1" />
                        <label for="agreement" class="css-label">我已細閱並接受網站之<a href="#" target="_blank" class="tnc">會籍條款及細則</a>，<a href="#" target="_blank" class="tnc">活動條款及細則</a>及<a href="#" target="_blank" class="tnc">私隱政策</a>。
                            <span class="error error-checkbox"></span>
                        </label>
                    </label>
                </div>
                <div class="form-group row">
                    <div class="col-xs-12">如不同意我們或第三方使用閣下的個人資料向閣下提供有關嬰幼兒營養﹑ 食品及護理的服務資訊及產品，請於方格內加“ ✔ ”，入會之申請將無法處理。</div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-12">
                        <input type="checkbox" name="promotion" id="promotion" class="css-checkbox" value="0" />
                        <label for="promotion" class="css-label">我不同意雀巢香港有限公司及其關聯公司或協助其作直接營銷之第三方使用 我的個人資料向我提供有關嬰幼兒營養﹑食品及護理的服務資訊及產品。<br><br>
                            成功申請後，我明白我可以於任何時候查詢或更改我的個人資料或取消有關同意，但須給予雀巢營養顧問（聯絡資料見下述）通告。<br><br>
                            <span>香港東九龍郵政局郵政信箱68867號 <a href="https://www.nestle.com.hk/" target="_blank">www.nestle.com.hk</a></span>
                        </label>
                    </label>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 submit-button">
                        <button type="submit" class="btn btn-secondary hvr-fade">提交</button>
                    </div>
                </div>
            </div>
			</div>
        </section>
	</form>
</div>

<div style="visibility:hidden;">
<form name="uploadfile" id="uploadfile" method="post" action="<?php echo $baseUrl ?>php/ajax_temp_upload.php" enctype="multipart/form-data" target="ajaxupload">
<input type="hidden" id="ajaxfieldname" name="fieldname" value="">
</form>
<iframe name="ajaxupload" style="width:1px;height:1px;"></iframe>
<iframe name="process" style="width:1px;height:1px;"></iframe>
</div>

<?php include 'templates/footer.php'; ?>
<link rel="stylesheet" href="../css/contest_form.css">
<script src="../js/contest.js"></script>

<script>
function ajax_upload_cb(obj) {
	$('#uploading').hide();
	
	if (obj.error) {
		
		ajax_upload_reset(obj.fieldname);
		alert(obj.error);
		
	} else {
		//console.log(obj);
		$('#'+obj.fieldname+'_select').hide();
		$('#'+obj.fieldname+'_select').siblings('div.note').hide();
		$('#'+obj.fieldname+'_text').html(obj.preview);
		$('#'+obj.fieldname+'_url').val(obj.url);
		
	}
}

function ajax_upload_reset(fieldname) {

	$('#'+fieldname).appendTo( $('#'+fieldname+'_div') );
	$('#'+fieldname+'_select').show();
	$('#'+fieldname+'_select').siblings('div.note').show();
	$('#'+fieldname+'_text').html("");
	$('#'+fieldname+'_url').val("");
	$('#'+fieldname).val("");
	
}

$(document).ready(function() {	

	$( window ).load(function() {
		ajax_upload_reset('proof');
	});

	$('#proof').on('change', function (e) {

		e.preventDefault();
		
		if ($('#proof').val()) {
			
			$('#uploading').show();
			$('#ajaxfieldname').val('proof');
			$('#proof').appendTo( $('#uploadfile') );
			$("#uploadfile").submit();
			
		}

	});
	
});
</script>
</body>
</html>
