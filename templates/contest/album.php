<?php
define('_VALID_INCLUDE', TRUE); // flag to allow include or require files
$dir_level = "./"; //set the required files located

require_once($dir_level.'includes/vars.inc.php');
require_once($dir_level.'includes/common.inc.php');

?>
<?php include 'templates/commonvar.php'; ?>
<?php
$cat = isset($_GET['cat'])?$_GET['cat']:'1';
if($cat == '1'){
    $class = 'first';
} else if ($cat == '2'){
    $class = 'new';
} else if ($cat == '3'){
    $class = 'organic';
} else {
    $class = '';
}

$pagesize = 13;

$pageno = trim(htmlencode($_GET["page"]));
$bb_name = trim(htmlencode($_GET["bb_name"]));
   
if ($pageno == "") {
	$pageno = 1;
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>GERBER - Gerber® 寶寶食「相」大賽</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content=" ">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="GERBER - Gerber® 寶寶食「相」大賽" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/aboutus/video_img.jpg" />
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="../css/hover.css">
    <link rel="stylesheet" type="text/css" href="../css/animate.css">
    <link rel="stylesheet" type="text/css" href="../plugin/swiper/v3/swiper.min.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/layout.css">
    <script src="../js/jquery.min.js"></script>

    <!-- Vendor: Javascripts -->
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mobile.custom.min.js"></script>
    <script src="../plugin/swiper/v3/swiper.min.js"></script>
    <script src="../plugin/checkUserAgent/checkUserAgent.js"></script>
    <script src="https://use.fontawesome.com/60efdea8b0.js"></script>
    <!-- Our Website Javascripts -->
    <script src="../js/main.js"></script>
    <?php include 'templates/preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<?php include 'templates/header_contest.php'; ?>
<span id="btn" style="dispaly:none"></span>
<?php

// open db connection
$conn = openConnection($conn);

?>
<section id="album">
    <div class="container">
        <div class="row">
            <div class="col col-xs-12 col-sm-7"><img id="headline" src="../images/contest/album.png"></div>
            <div class="col col-xs-12 col-sm-5"><input type="text" name="bb_name" id="bb_name" value="<?php echo $bb_name; ?>" maxlength="255" placeholder="以寶寶名字搜尋" ><input style="display:none;" id="btn_search" type="button" value="Search" onclick="javascript:window.location = '<?php echo $cfg["root"]; ?>contest/album?cat=<?php echo $cat; ?>&bb_name='+encodeURIComponent(document.getElementById('bb_name').value)"></div>
        </div>
    </div>
    <div class="visible-xs visible-sm">
        <div class="category sty1">
            <div class="cat-wrap">
                <div class="left">
                    <img src="../images/contest/album_cat1.png">
                </div>
                <div class="right">
                    <div class="head">享受第一口固體食物</div>
                    <div class="desc">寶寶6個月起，初嚐固體食物，<br>感覺新鮮、好奇</div>
                </div>
            </div>
        </div>
        <div id="cat1" class="cat swiper-container">
            <div class="swiper-wrapper">
<?php
$sql = " select * from `gerberfev_game_201710` where 1 ";
$sql.= " and `photo_type` = '1' ";
if ($bb_name != "") {
	$sql.= " and `bb_name` like ('%".$bb_name."%') ";
}
$sql.= " and `status` = '1' ";
$sql.= " order by `created_date` desc, `id` desc ";

$result = mysql_query($sql, $conn);
$num = mysql_num_rows($result);

$rcount = 1;

while ($row = mysql_fetch_array($result)) {
	
	if ($row["photo_type"] == "1") {
		$photo_type = "first";
	} else
	if ($row["photo_type"] == "2") {
		$photo_type = "new";
	} else
	if ($row["photo_type"] == "3") {
		$photo_type = "organic";
	}
	
	$sql = " select `id` from `gerberfev_game_201710_like_count_log` where 1 ";
	$sql.= " and `bb_id` = '".htmlencode($row["id"])."' ";
	$sql.= " and `created_ip` = '".get_client_ip()."' ";
	$sql.= " and `status` = '1' ";

	$result2 = mysql_query($sql, $conn);
	$num2 = mysql_num_rows($result2);

	if ($num2 <= 0) {
		$bb_like_class = "hvr-fade";
	} else {
		$bb_like_class = "";
	}
?>
                <div class="swiper-slide">
                    <div class="thumb">
                        <div class="candidate <?php echo $photo_type; ?>"><div class="photo" style="background-image: url('<?php echo $baseUrl.$row["photo_thumb"] ?>');"></div></div>
                        <div class="desc">
                            <div class="name"><?php echo $row["bb_name"] ?></div>
                            <div class="icon-wrap">
                                <div class="like" bb-id="<?php echo $row["id"]; ?>" ><i class="fa fa-thumbs-up <?php echo $bb_like_class; ?>" aria-hidden="true"></i><span class="count"><?php echo $row["like"] ?></span></div>
                                <div class="share" onclick="shareFB('<?php echo $baseUrl."contest?id=".$row["id"] ?>');"><i class="fa fa-share-alt hvr-fade" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
<?php
	
	$rcount++;
	
	if ($rcount == 8) {
?>
                <div class="swiper-slide promo">
                    <div class="promotion">
                        <a href="step1"><img src="../images/contest/ad1.png"></img></a>
                    </div>
                </div>
<?php
	}
}
?>
            </div>
        </div>
        <div class="category sty2">
            <div class="cat-wrap">
                <div class="left">
                    <img src="../images/contest/album_cat2.png">
                </div>
                <div class="right">
                    <div class="head">享受新口味</div>
                    <div class="desc">寶寶開始探索更多味道，<br>例如是水果味</div>
                </div>
            </div>
        </div>
        <div id="cat2" class="cat swiper-container">
            <div class="swiper-wrapper">
<?php
$sql = " select * from `gerberfev_game_201710` where 1 ";
$sql.= " and `photo_type` = '2' ";
if ($bb_name != "") {
	$sql.= " and `bb_name` like ('%".$bb_name."%') ";
}
$sql.= " and `status` = '1' ";
$sql.= " order by `created_date` desc, `id` desc ";

$result = mysql_query($sql, $conn);
$num = mysql_num_rows($result);

$rcount = 1;

while ($row = mysql_fetch_array($result)) {
	
	if ($row["photo_type"] == "1") {
		$photo_type = "first";
	} else
	if ($row["photo_type"] == "2") {
		$photo_type = "new";
	} else
	if ($row["photo_type"] == "3") {
		$photo_type = "organic";
	}
	
	$sql = " select `id` from `gerberfev_game_201710_like_count_log` where 1 ";
	$sql.= " and `bb_id` = '".htmlencode($row["id"])."' ";
	$sql.= " and `created_ip` = '".get_client_ip()."' ";
	$sql.= " and `status` = '1' ";

	$result2 = mysql_query($sql, $conn);
	$num2 = mysql_num_rows($result2);

	if ($num2 <= 0) {
		$bb_like_class = "hvr-fade";
	} else {
		$bb_like_class = "";
	}
?>
                <div class="swiper-slide">
                    <div class="thumb">
                        <div class="candidate <?php echo $photo_type; ?>"><div class="photo" style="background-image: url('<?php echo $baseUrl.$row["photo_thumb"] ?>');"></div></div>
                        <div class="desc">
                            <div class="name"><?php echo $row["bb_name"] ?></div>
                            <div class="icon-wrap">
                                <div class="like" bb-id="<?php echo $row["id"]; ?>" ><i class="fa fa-thumbs-up <?php echo $bb_like_class; ?>" aria-hidden="true"></i><span class="count"><?php echo $row["like"] ?></span></div>
                                <div class="share" onclick="shareFB('<?php echo $baseUrl."contest?id=".$row["id"] ?>');"><i class="fa fa-share-alt hvr-fade" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
<?php
	
	$rcount++;
	
	if ($rcount == 8) {
?>
                <div class="swiper-slide promo">
                    <div class="promotion">
                        <a href="step1"><img src="../images/contest/ad1.png"></img></a>
                    </div>
                </div>
<?php
	}
}
?>
            </div>
        </div>
        <div class="category sty3">
            <div class="cat-wrap">
                <div class="left">
                    <img src="../images/contest/album_cat3.png">
                </div>
                <div class="right">
                    <div class="head">享受有機食物</div>
                    <div class="desc">有機產品系列獲美國農業部認可，為寶寶提供豐富營養</div>
                </div>
            </div>
        </div>
        <div id="cat3" class="cat swiper-container">
            <div class="swiper-wrapper">
<?php
$sql = " select * from `gerberfev_game_201710` where 1 ";
$sql.= " and `photo_type` = '3' ";
if ($bb_name != "") {
	$sql.= " and `bb_name` like ('%".$bb_name."%') ";
}
$sql.= " and `status` = '1' ";
$sql.= " order by `created_date` desc, `id` desc ";

$result = mysql_query($sql, $conn);
$num = mysql_num_rows($result);

$rcount = 1;

while ($row = mysql_fetch_array($result)) {
	
	if ($row["photo_type"] == "1") {
		$photo_type = "first";
	} else
	if ($row["photo_type"] == "2") {
		$photo_type = "new";
	} else
	if ($row["photo_type"] == "3") {
		$photo_type = "organic";
	}
	
	$sql = " select `id` from `gerberfev_game_201710_like_count_log` where 1 ";
	$sql.= " and `bb_id` = '".htmlencode($row["id"])."' ";
	$sql.= " and `created_ip` = '".get_client_ip()."' ";
	$sql.= " and `status` = '1' ";

	$result2 = mysql_query($sql, $conn);
	$num2 = mysql_num_rows($result2);

	if ($num2 <= 0) {
		$bb_like_class = "hvr-fade";
	} else {
		$bb_like_class = "";
	}
?>
                <div class="swiper-slide">
                    <div class="thumb">
                        <div class="candidate <?php echo $photo_type; ?>"><div class="photo" style="background-image: url('<?php echo $baseUrl.$row["photo_thumb"] ?>');"></div></div>
                        <div class="desc">
                            <div class="name"><?php echo $row["bb_name"] ?></div>
                            <div class="icon-wrap">
                                <div class="like" bb-id="<?php echo $row["id"]; ?>" ><i class="fa fa-thumbs-up <?php echo $bb_like_class; ?>" aria-hidden="true"></i><span class="count"><?php echo $row["like"] ?></span></div>
                                <div class="share" onclick="shareFB('<?php echo $baseUrl."contest?id=".$row["id"] ?>');"><i class="fa fa-share-alt hvr-fade" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
<?php
	
	$rcount++;
	
	if ($rcount == 8) {
?>
                <div class="swiper-slide promo">
                    <div class="promotion">
                        <a href="step1"><img src="../images/contest/ad1.png"></img></a>
                    </div>
                </div>
<?php
	}
}
?>
            </div>
        </div>
    </div>
    <div class="visible-md visible-lg">
        <div class="container">
            <div class="row cat">
                <div class="col col-md-4 <?=($cat=='1')?'active':'';?>">
                    <a href="album?cat=1">
                        <div class="category sty1">
                            <div class="cat-wrap">
                                <div class="left">
                                    <img src="../images/contest/album_cat1.png">
                                </div>
                                <div class="right">
                                    <div class="head">享受第一口固體食物</div>
                                    <div class="desc">寶寶6個月起，初嚐固體食物，<br>感覺新鮮、好奇</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col col-md-4 <?=($cat=='2')?'active':'';?>">
                    <a href="album?cat=2">
                        <div class="category sty2">
                            <div class="cat-wrap">
                                <div class="left">
                                    <img src="../images/contest/album_cat2.png">
                                </div>
                                <div class="right">
                                    <div class="head">享受新口味</div>
                                    <div class="desc">寶寶開始探索更多味道，<br>例如是水果味</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col col-md-4 <?=($cat=='3')?'active':'';?>">
                    <a href="album?cat=3">
                        <div class="category sty3">
                            <div class="cat-wrap">
                                <div class="left">
                                    <img src="../images/contest/album_cat3.png">
                                </div>
                                <div class="right">
                                    <div class="head">享受有機食物</div>
                                    <div class="desc">有機產品系列獲美國農業部認可，為寶寶提供豐富營養</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="album-wrap">
<?php
// sql
// get total record
$sql = "SELECT count(*)/".$pagesize." AS page_count, count(*) AS record_count ";
$sql .= "FROM gerberfev_game_201710 where 1 ";
$sql.= " and `photo_type` = '".$cat."' ";
if ($bb_name != "") {
	$sql.= " and `bb_name` like ('%".$bb_name."%') ";
}
$sql.= " and status = '1' ";

$result = mysql_query($sql,$conn);
if ($row=mysql_fetch_array($result)) {

	$page_count = ceil($row["page_count"]);
	$record_count = $row["record_count"];

} else {
	$page_count = 1;
	$record_count = 0;
}

$totcount = $record_count;
$pagecount = ceil($totcount/$pagesize);

mysql_free_result($result);

$sql = " select * from `gerberfev_game_201710` where 1 ";
$sql.= " and `photo_type` = '".$cat."' ";
if ($bb_name != "") {
	$sql.= " and `bb_name` like ('%".$bb_name."%') ";
}
$sql.= " and `status` = '1' ";
$sql.= " order by `created_date` desc, `id` desc ";

$result = mysql_query($sql, $conn);
$num = mysql_num_rows($result);

$rcount = 1;

while ($row = mysql_fetch_array($result)) {
	
	if ($row["photo_type"] == "1") {
		$photo_type = "first";
	} else
	if ($row["photo_type"] == "2") {
		$photo_type = "new";
	} else
	if ($row["photo_type"] == "3") {
		$photo_type = "organic";
	}
	
	$sql = " select `id` from `gerberfev_game_201710_like_count_log` where 1 ";
	$sql.= " and `bb_id` = '".htmlencode($row["id"])."' ";
	$sql.= " and `created_ip` = '".get_client_ip()."' ";
	$sql.= " and `status` = '1' ";

	$result2 = mysql_query($sql, $conn);
	$num2 = mysql_num_rows($result2);

	if ($num2 <= 0) {
		$bb_like_class = "hvr-fade";
	} else {
		$bb_like_class = "";
	}
?>
                <div class="thumb">
                    <div class="candidate <?php echo $photo_type; ?>"><div class="photo" style="background-image: url('<?php echo $baseUrl.$row["photo_thumb"] ?>');"></div></div>
                    <div class="desc">
                        <div class="name"><?php echo $row["bb_name"] ?></div>
                        <div class="icon-wrap">
                            <div class="like" bb-id="<?php echo $row["id"]; ?>" ><i class="fa fa-thumbs-up <?php echo $bb_like_class; ?>" aria-hidden="true"></i><span class="count"><?php echo $row["like"] ?></span></div>
                            <div class="share" onclick="shareFB('<?php echo $baseUrl."contest?id=".$row["id"] ?>');"><i class="fa fa-share-alt hvr-fade" aria-hidden="true"></i></div>
                        </div>
                    </div>
                </div>
<?php
	
	$rcount++;
	
	if ($rcount == 13) {
?>
                <div class="promotion">
                    <a href="step1"><img src="../images/contest/ad1.png"></img></a>
                </div>
<?php
	}
}
?>
			</div>
<?php
if ($pagecount >= 1) {
?>
            <div class="pagination">
<?php
if ($pageno > 1) {
?>
				<a href="album?cat=<?php echo $cat; ?>&page=<?php echo $pageno-1; ?>&bb_name=<?php echo $bb_name; ?>"><img class="prev" src="../images/contest/prev.png"></a>
<?php
}
?>
<?php
	for ($i = 1; $i <= $pagecount ; $i++) {
?>
                <a href="album?cat=<?php echo $cat; ?>&page=<?php echo $i; ?>&bb_name=<?php echo $bb_name; ?>" class="<?php if  ($pageno == $i) { echo " active"; } ?>"><?php echo $i; ?></a>
<?php
	}
?>
<?php
if ($pageno < $pagecount) {
?>
                <a href="album?cat=<?php echo $cat; ?>&page=<?php echo $pageno+1; ?>&bb_name=<?php echo $bb_name; ?>"><img class="next" src="../images/contest/next.png"></a>
<?php
}
?>
                <div class="detail"><span><?php echo $pageno; ?></span>/<?php echo $pagecount; ?>頁</div>
            </div>
<?php
}
?>
        </div>
    </div>
</section>
<?php

// close db connection
$conn = closeConnection($conn);

?>

<?php include 'templates/footer.php'; ?>
<link rel="stylesheet" href="../css/contest.css">
<script src="../js/contest.js"></script>

<script>
$(document).ready(function() {	
	// Album
	
	$("#album #bb_name").on('keyup keypress', function(e) {
		var keyCode = e.keyCode || e.which;
		if (keyCode === 13) { 
			$("#btn_search").click();
		}
	});

	$(document).on("click", '#album .icon-wrap div.like', function(e) {
		e.preventDefault();
		
		if (!$(this).hasClass("active")) {
			
			
			var that = $(this);
				
			$.post("<?php echo $baseUrl; ?>php/save_testimonial_like.php","id="+$(this).attr("bb-id")+"&r="+new Date().getTime(), function(data) {
			
				submitting = false;
				
				if (data.status == "success") {

					that.find("span.count").text(data.like_count);
					that.addClass("active");
					that.find("i").removeClass("hvr-fade");
				
				} else if (data.status == "repeat") {
					
					that.addClass("active");
					that.find("i").removeClass("hvr-fade");
					
				} else {
					
					//alert("系統繁忙，請再嘗試");
					
				}
				
			},"json");
			
		}
		
		//ga('send', 'event', 'Button', 'click', 'Like');

	});

	$(document).on("click", '#album .icon-wrap div.share', function(e) {
		//e.preventDefault();
		
		$.post("<?php echo $baseUrl; ?>php/save_testimonial_share.php","id="+$(this).siblings("div.like").attr("bb-id")+"&type=fb&r="+new Date().getTime(), function(data) {
			
		},"json");	
		
		//ga('send', 'event', 'Share', 'click', 'Facebook');

	});
});	
</script>
</body>
</html>
