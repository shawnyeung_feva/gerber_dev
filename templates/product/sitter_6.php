<?php include 'templates/commonvar.php'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>雞肉蓉</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="讓寶寶享受新味道和食物組合">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶,產品資訊,蘋果味">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="雞肉蓉" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="讓寶寶享受新味道和食物組合" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/product/stage2/7b.jpg" />
    <!-- google -->
    <meta itemprop="name" content="雞肉蓉">
    <meta itemprop="description" content="讓寶寶享受新味道和食物組合">
    <meta itemprop="image" content="<?php echo $baseUrl ?>images/product/stage2/7b.jpg" />
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
    <link href="../css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="../css/hover.css">
    <link rel="stylesheet" type="text/css" href="../css/animate.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/layout.css">
    <link rel="stylesheet" href="../css/product.css">
    <!-- Vendor: Javascripts -->

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mobile.custom.min.js"></script>

    <!-- Our Website Javascripts -->
    <script src="../js/main.js"></script>
    <?php include 'templates/preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<?php include 'templates/header_product.php'; ?>

<div class="container">

    <div class="row">

        <div class="col-lg-12 header-type">
          <img class="mobileOn" id="Image-Maps-Com-image-maps-2016-06-01-054237" src="../images/menu-tab/mobile/stage2full.png" border="0" width="100%" usemap="#image-maps-2016-06-01-054237"  />
          <map name="image-maps-2016-06-01-054237" id="ImageMapsCom-image-maps-2016-06-01-054237">
          <area  alt="supported stage" title="supported stage" href="ricecereal" shape="rect" coords="0,0,86,111" style="outline:none;" target="_self"     />
          <area  alt="sitter stage" title="sitter stage" href="bananaappleic" shape="rect" coords="92,0,163,111" style="outline:none;" target="_self"     />
          <area  alt="crawler stage" title="crawler stage" href="organicveggies" shape="rect" coords="171,0,238,111" style="outline:none;" target="_self"     />
          <area  alt="toddler stage" title="toddler stage" href="lilbiscuit" shape="rect" coords="246,0,322,111" style="outline:none;" target="_self"     />
          <area shape="rect" coords="320,109,322,111" alt="Image Map" style="outline:none;" title="Image Map" href="#" />
          </map>

            <img class="img-responsive mobileOff" id="Image-Maps-Com-image-maps-2016-06-01-052803" src="../images/menu-tab/desktop/stage2full.png" border="0" usemap="#image-maps-2016-06-01-052803" alt="" />
            <map name="image-maps-2016-06-01-052803" id="ImageMapsCom-image-maps-2016-06-01-052803">
            <area title="supported stage" href="ricecereal" shape="rect" coords="16,2,273,106" style="outline:none;" target="_self"     />
            <area  alt="sitter stage" title="sitter stage" href="bananaappleic" shape="rect" coords="271,2,521,104" style="outline:none;" target="_self"     />
            <area  alt="crawler stage" title="crawler stage" href="organicveggies" shape="rect" coords="517,0,764,106" style="outline:none;" target="_self"     />
            <area  alt="toddler stage" title="toddler stage" href="lilbiscuit" shape="rect" coords="760,0,989,107" style="outline:none;" target="_self"     />
            <area shape="rect" coords="998,110,1000,112" alt="Image Map" style="outline:none;" title="Image Map" href="#" />
            </map>

        </div>

    </div>
</div>

<div class="container article">
    <div class="row title">
        <div class="col-lg-12">
            <h1 class="page-header">雞肉蓉
            </h1>
            <ul class="share mobileOff">
              <li><a class="pop tw hvr-grow icon-twitter-logo-silhouette" name="twi"></a></li>
              <li><a class="pop gplus hvr-grow icon-google-plus" name="google"></a></li>
              <li><a class="pop fb hvr-grow icon-facebook-logo" name="fbook"></a></li>
              <li><a id="btn" class="link hvr-grow icon-unlink"></a></li>
            </ul>
            <div class="stage-icon mobileOff">
              <img src="../images/list/sitter.png" alt="" />
              <span>6個月起</span>
            </div>
        </div>
    </div>

    <div class="row info">

        <div class="col-md-5 product-img">
            <img class="img-responsive hvr-buzz-out" src="../images/product/stage2/7b.jpg">
            <div class="learn-more mobileOff">
                <a href="https://www.nestlebaby.hk/sitter?utm_source=sitter&utm_medium=ChickenGravy&utm_campaign=Gerberhk

" target="_blank">了解更多端坐期育兒資訊<i class="fa fa-angle-right"></i></a>
            </div>
        </div>
        <div class="col-md-7 info-title">
            <div class="top-title no-top">
                <h2>產品詳情</h2>
            </div>
            <div class="scroll-pane mobileOff">
                <div class="info-head">
                    <p>讓寶寶享受新味道和食物組合</p>
                </div>
                <div class="special">
                    <h4>產品特色</h4>
                    <ul>
                      <li>切合年齡需要的營養及口感</li>
                      <li>以新鮮雞肉製成</li>
                      <li>不添加人工調味料、色素、甜味劑及防腐劑</li>
                    </ul>
                </div><!--
                <div class="top-title">
                    <h2>食用、調配及貯存方法</h2>
                    <ul>
                      <li>開啟後請冷藏</li>
                      <li>請於開封後兩天內食用</li>
                    </ul>
                    <br><br><br>
                </div>-->
            </div>
            <div class="mobileOn">
              <div class="info-head">
                  <p>GERBER<sup>®</sup>雞肉蓉為寶寶帶來多種不同口味及食材組合，有助他們接觸新口味。</p>
              </div>
              <div class="special">
                  <h4>產品特色</h4>
                    <ul>
                      <li>切合年齡需要的營養及口感</li>
                      <li>以新鮮雞肉製成</li>
                      <li>不添加人工調味料、色素、甜味劑及防腐劑</li>
                    </ul>
              </div><!--
              <div class="top-title">
                  <h2>食用、調配及貯存方法</h2>
                  <ul>
                    <li>開啟後請冷藏</li>
                    <li>請於開封後兩天內食用</li>
                  </ul>
              </div>-->
                <div class="learn-more">
                    <a href="https://www.nestlebaby.hk/sitter?utm_source=sitter&utm_medium=ChickenGravy&utm_campaign=Gerberhk

" target="_blank">了解更多端坐期育兒資訊<i class="fa fa-angle-right"></i></a>
                </div>
            </div>

        </div>

    </div>

    <div class="row" style="">
        <div class="col-lg-12 line">
        </div>
        <h1 class="page-header related-title">其他相同階段產品
            </h1>
    </div>

    <div class="row related-product" style="">
             <ul id="lightSlider">
              <li>
                  <a class="hvr-buzz-out" href="bananaappleic" style="background: url(../images/product/stage2/1b.jpg)">
                  </a>
                  <h3>穀物及水果米粉<br><span class="desc">香蕉蘋果味</span></h3>
              </li>
              <li style="border-right:0">
                  <a class="hvr-buzz-out" href="applejuice" style="background: url(../images/product/stage2/2.png)">
                  </a>
                  <h3>100%果汁</h3>
              </li><!--
              <li>
                  <a class="hvr-buzz-out" href="蘋果汁" style="background: url(../images/product/stage2/3.png)">
                  </a>
                  <h3>蘋果汁<sup>®</sup></h3>
              </li>
              <li>
                  <a class="hvr-buzz-out" href="第二階段食品" style="background: url(../images/product/stage2/4.png)">
                  </a>
                  <h3>第二階段食品<sup>®</sup></h3>
              </li>
              <li>
                  <a class="hvr-buzz-out" href="蘋果藍莓蓉" style="background: url(../images/product/stage2/5.png)">
                  </a>
                  <h3>蘋果藍莓蓉</h3>
              </li>
              <li>
                  <a class="hvr-buzz-out" href="蘋果蓉" style="background: url(../images/product/stage2/6.png)">
                  </a>
                  <h3>蘋果蓉</h3>
              </li>-->
            </ul>
            <div class="lSAction">
            <a class="lSPrev"><i class="fa fa-angle-left"></i></a>
            <a class="lSNext"><i class="fa fa-angle-right"></i></a>
            </div>
    </div>
</div>
<!-- /.container -->

<?php include 'templates/product/form.php'; ?>
<?php include 'templates/footer.php'; ?>

<script src="../plugin/jscroll/jquery.jscrollpane.min.js"></script>
<script src="../plugin/jscroll/jquery.mousewheel.js"></script>
<link rel="stylesheet" href="../plugin/jscroll/jquery.jscrollpane.css">
<script src="../plugin/lightslider/lightslider.min.js"></script>
<script src="../js/imageMapResizer.js"></script>
<script src="../js/product.js"></script>
<link rel="stylesheet" href="../plugin/lightslider/lightslider.css">

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BabyFood_Gerber_Product information - chickengravy
URL of the webpage where the tag is expected to be placed:
https://www.gerber.com.hk/productinformation/chickengravy
This tag must be placed between the <body> and </body> tags, as close as possible to the
opening tag.
Creation Date: 07/25/2017
-->
<iframe
src="https://8039665.fls.doubleclick.net/activityi;src=8039665;type=gerber00;cat=ger_13;dc_l
at=;dc_rdid=;tag_for_child_directed_treatment=;ord=<?php session_start(); echo str_replace(array(",","-","_"),"",session_id()); ?>?" width="1" height="1"
frameborder="0" style="display:none"></iframe>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</body>
</html>
