<?php include 'templates/commonvar.php'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>星星餅 - 蜜桃味</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="以優質全穀物製成的嬰幼兒零食，讓爬行寶寶享受自行進食的樂趣">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶,產品資訊,蘋果味">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="星星餅 - 蜜桃味" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="以優質全穀物製成的嬰幼兒零食，讓爬行寶寶享受自行進食的樂趣" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/product/stage3/3b.jpg" />
    <!-- google -->
    <meta itemprop="name" content="星星餅 - 蜜桃味">
    <meta itemprop="description" content="以優質全穀物製成的嬰幼兒零食，讓爬行寶寶享受自行進食的樂趣">
    <meta itemprop="image" content="<?php echo $baseUrl ?>images/product/stage3/3b.jpg" />
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
    <link href="../css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="../css/hover.css">
    <link rel="stylesheet" type="text/css" href="../css/animate.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/layout.css">
    <link rel="stylesheet" href="../css/product.css">
    <!-- Vendor: Javascripts -->

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mobile.custom.min.js"></script>

    <!-- Our Website Javascripts -->
    <script src="../js/main.js"></script>
    <?php include 'templates/preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<?php include 'templates/header_product.php'; ?>

<div class="container">

    <div class="row">

        <div class="col-lg-12 header-type">
          <img class="mobileOn" id="Image-Maps-Com-image-maps-2016-06-01-054237" src="../images/menu-tab/mobile/stage3full.png" border="0" width="100%" usemap="#image-maps-2016-06-01-054237"  />
          <map name="image-maps-2016-06-01-054237" id="ImageMapsCom-image-maps-2016-06-01-054237">
          <area  alt="supported stage" title="supported stage" href="ricecereal" shape="rect" coords="0,0,86,111" style="outline:none;" target="_self"     />
          <area  alt="sitter stage" title="sitter stage" href="bananaappleic" shape="rect" coords="92,0,163,111" style="outline:none;" target="_self"     />
          <area  alt="crawler stage" title="crawler stage" href="organicveggies" shape="rect" coords="171,0,238,111" style="outline:none;" target="_self"     />
          <area  alt="toddler stage" title="toddler stage" href="lilbiscuit" shape="rect" coords="246,0,322,111" style="outline:none;" target="_self"     />
          <area shape="rect" coords="320,109,322,111" alt="Image Map" style="outline:none;" title="Image Map" href="#" />
          </map>

            <img class="img-responsive mobileOff" id="Image-Maps-Com-image-maps-2016-06-01-052803" src="../images/menu-tab/desktop/stage3full.png" border="0" usemap="#image-maps-2016-06-01-052803" alt="" />
            <map name="image-maps-2016-06-01-052803" id="ImageMapsCom-image-maps-2016-06-01-052803">
            <area title="supported stage" href="ricecereal" shape="rect" coords="16,2,273,106" style="outline:none;" target="_self"     />
            <area  alt="sitter stage" title="sitter stage" href="bananaappleic" shape="rect" coords="271,2,521,104" style="outline:none;" target="_self"     />
            <area  alt="crawler stage" title="crawler stage" href="organicveggies" shape="rect" coords="517,0,764,106" style="outline:none;" target="_self"     />
            <area  alt="toddler stage" title="toddler stage" href="lilbiscuit" shape="rect" coords="760,0,989,107" style="outline:none;" target="_self"     />
            <area shape="rect" coords="998,110,1000,112" alt="Image Map" style="outline:none;" title="Image Map" href="#" />
            </map>

        </div>

    </div>
</div>

<div class="container article">
    <div class="row title">
        <div class="col-lg-12">
            <h1 class="page-header">星星餅
            </h1>
            <small>蜜桃味</small>
            <ul class="share mobileOff">
              <li><a class="pop tw hvr-grow icon-twitter-logo-silhouette" name="twi"></a></li>
              <li><a class="pop gplus hvr-grow icon-google-plus" name="google"></a></li>
              <li><a class="pop fb hvr-grow icon-facebook-logo" name="fbook"></a></li>
              <li><a id="btn" class="link hvr-grow icon-unlink"></a></li>
            </ul>
            <div class="stage-icon mobileOff">
              <img src="../images/list/crawler.png" alt="" />
              <!--<span>8個月起</span>-->
            </div>
        </div>
    </div>

    <div class="row info">

        <div class="col-md-5 product-img">
            <img class="img-responsive hvr-buzz-out" src="../images/product/stage3/3b.jpg">
            <div class="learn-more mobileOff">
                <a href="https://www.nestlebaby.hk/crawler?utm_source=crawler&utm_medium=Puffs_Peach&utm_campaign=Gerberhk

" target="_blank">了解更多爬行期育兒資訊<i class="fa fa-angle-right"></i></a>
            </div>
        </div>
        <div class="col-md-7 info-title">
            <div class="top-title no-top">
                <h2>產品詳情</h2>
            </div>
            <div class="scroll-pane mobileOff">
                <div class="info-head">
                    <p>以優質全穀物製成的嬰幼兒零食，讓爬行寶寶享受自行進食的樂趣</p>
                </div>
                <div class="special">
                    <h4>產品特色</h4><!--
                    <p>穀物零食<br></p>-->
                    <ul>
                      <li>以全穀物製成，每食用份量蘊含2克全穀物</li>
                      <li>蘊含5種維他命及礦物質，有助寶寶健康成長</li>
                      <li>豐富鐵質、維他命E、維他命B1、B3及B6</li>
                      <li>入口即溶，使寶寶容易吞嚥</li>
                      <li>大小適中，幫助寶寶訓練小手指和學習自行進食</li>
                      <li>不添加人工調味料、色素、甜味劑及防腐劑</li>
                    </ul>
                </div><!--
                <div class="top-title">
                    <h2>食用、調配及貯存方法</h2>
                    <p class="remark">
                      本產品專為習慣咀嚼固體食物的幼兒而設，只適合幼兒坐起並有成人監管的情況下食用。
                    </p>
                </div>-->
            </div>
            <div class="mobileOn">
              <div class="info-head">
                  <p>以優質全穀物製成的嬰幼兒零食，讓爬行寶寶享受自行進食的樂趣</p>
              </div>
              <div class="special">
                  <h4>產品特色</h4><!--
                  <p>穀物零食<br></p>-->
                  <ul>
                      <li>以全穀物製成，每食用份量蘊含2克全穀物</li>
                      <li>蘊含5種維他命及礦物質，有助寶寶健康成長</li>
                      <li>豐富鐵質、維他命E、維他命B1、B3及B6</li>
                      <li>入口即溶，使寶寶容易吞嚥</li>
                      <li>大小適中，幫助寶寶訓練小手指和學習自行進食</li>
                      <li>不添加人工調味料、色素、甜味劑及防腐劑</li>
                  </ul>
              </div><!--
              <div class="top-title">
                  <h2>食用、調配及貯存方法</h2>
                  <p class="remark">
                    本產品專為習慣咀嚼固體食物的幼兒而設，只適合幼兒坐起並有成人監管的情況下食用。
                  </p>
              </div>-->
                <div class="learn-more">
                    <a href="https://www.nestlebaby.hk/crawler?utm_source=crawler&utm_medium=Puffs_Peach&utm_campaign=Gerberhk

" target="_blank">了解更多爬行期育兒資訊<i class="fa fa-angle-right"></i></a>
                </div>
            </div>

        </div>

    </div>

    <div class="row" style="">
        <div class="col-lg-12 line">
        </div>
        <h1 class="page-header related-title">其他相同階段產品
            </h1>
    </div>

    <div class="row related-product" style="">
             <ul id="lightSlider">
              <li>
                  <a class="hvr-buzz-out" href="organicveggies" style="background: url(../images/product/stage3/1b.jpg)">
                  </a>
                  <h3>有機星星餅<br><span class="desc">蔬菜味<span></h3>
              </li>
              <li>
                  <a class="hvr-buzz-out" href="gerber-lil-bits-cereal-oatmeal-banana-strawberry" style="background: url(../images/product/stage3/10.jpg)">
                  </a>
                  <h3>燕麥粉<br><span class="desc">香蕉士多啤梨味</span></h3>
              </li>
              <li>
                  <a class="hvr-buzz-out" href="gerber-organic-yogurt-melts-banana-strawberry" style="background: url(../images/product/stage3/11.jpg)">
                  </a>
                  <h3>有機乳酪乾<br><span class="desc">香蕉士多啤梨味</span></h3>
              </li>
              <li>
                  <a class="hvr-buzz-out" href="organicapple" style="background: url(../images/product/stage3/2b.jpg)">
                  </a>
                  <h3>有機星星餅<br><span class="desc">蘋果味<span></h3>
              </li>
              <li>
                  <a class="hvr-buzz-out" href="bananapuff" style="background: url(../images/product/stage3/4b.jpg)">
                  </a>
                  <h3>星星餅<br><span class="desc">香蕉味<span></h3>
              </li>
              <li>
                  <a class="hvr-buzz-out" href="sweetpotatopuff" style="background: url(../images/product/stage3/5b.jpg)">
                  </a>
                  <h3>星星餅<br><span class="desc">甜薯味<span></h3>
              </li>
              <li>
                  <a class="hvr-buzz-out" href="strawberryapplepuff" style="background: url(../images/product/stage3/6b.jpg)">
                  </a>
                  <h3>星星餅<br><span class="desc">士多啤梨蘋果味<span></h3>
              </li>
              <li>
                  <a class="hvr-buzz-out" href="arrowrootcookies" style="background: url(../images/product/stage3/7.png)">
                  </a>
                  <h3>薏米曲奇餅<br><span class="desc">&nbsp;<span></h3>
              </li>
              <li>
                  <a class="hvr-buzz-out" href="ymmixedberries" style="background: url(../images/product/stage3/8.png)">
                  </a>
                  <h3>乳酪乾<br><span class="desc">雜莓味<span></h3>
              </li>
              <li>
                  <a class="hvr-buzz-out" href="ympeach" style="background: url(../images/product/stage3/9.png)">
                  </a>
                  <h3>乳酪乾<br><span class="desc">蜜桃味<span></h3>
              </li>
            </ul>
            <div class="lSAction">
            <a class="lSPrev"><i class="fa fa-angle-left"></i></a>
            <a class="lSNext"><i class="fa fa-angle-right"></i></a>
            </div>
    </div>
</div>
<!-- /.container -->

<?php include 'templates/product/form.php'; ?>
<?php include 'templates/footer.php'; ?>
<link rel="stylesheet" href="../plugin/lightslider/lightslider.css">


<script src="../plugin/jscroll/jquery.jscrollpane.min.js"></script>
<script src="../plugin/jscroll/jquery.mousewheel.js"></script>
<link rel="stylesheet" href="../plugin/jscroll/jquery.jscrollpane.css">
<script src="../plugin/lightslider/lightslider.min.js"></script>
<script src="../js/imageMapResizer.js"></script>
<script src="../js/product.js"></script>
<style media="screen">
  .shadow{
    display: none;
  }
</style>

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BabyFood_Gerber_Product information - peachpuff
URL of the webpage where the tag is expected to be placed:
https://www.gerber.com.hk/productinformation/peachpuff
This tag must be placed between the <body> and </body> tags, as close as possible to the
opening tag.
Creation Date: 07/25/2017
-->
<iframe
src="https://8039665.fls.doubleclick.net/activityi;src=8039665;type=gerber00;cat=ger_16;dc_l
at=;dc_rdid=;tag_for_child_directed_treatment=;ord=<?php session_start(); echo str_replace(array(",","-","_"),"",session_id()); ?>?" width="1" height="1"
frameborder="0" style="display:none"></iframe>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</body>
</html>
