<div class="modal fade" id="questionnaire" tabindex="-1" role="dialog" aria-labelledby="questionnaireModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="close-modal hvr-buzz-out" data-dismiss="modal"></span>
        <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        -->
        <h1 class="modal-title" id="exampleModalLabel">GERBER<sup>®</sup>產品市場調查</h1>
      </div>
      <div class="modal-body">
        <form id="question">
          <div class="form-group row line">
            <div class="left">過去一個月有沒有購買GERBER<sup>®</sup>產品？</div>
            <div class="right">
              <div class="checkbox">
                <label>
                  <input type="radio" name="buybefore" id="buyyes" class="css-checkbox" checked="checked" value="1"/><label for="buyyes" class="css-label" >有</label>
                </label>
                <label>
                  <input type="radio" name="buybefore" id="buyno" class="css-checkbox" value="0" /><label for="buyno" class="css-label">沒有</label>
                </label>
              </div>
            </div>
          </div>
          <div class="form-group row line-2">
            <div class="left">這個網站令您了解更多GERBER<sup>®</sup>產品的資訊</div>
            <div class="right">
            <label for="" class="disagree mobileOff">最不同意</label>
            <fieldset class="rating">
                <input type="radio" id="learn5" name="learn" value="5" /><label class="full hvr-grow" for="learn5" title="Awesome - 5 stars"></label>
                <input type="radio" id="learn4" name="learn" value="4" /><label class="full hvr-grow" for="learn4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="learn3" name="learn" value="3" /><label class="full hvr-grow" for="learn3" title="Meh - 3 stars"></label>
                <input type="radio" id="learn2" name="learn" value="2" /><label class="full hvr-grow" for="learn2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="learn1" name="learn" value="1" /><label class="full hvr-grow" for="learn1" title="Sucks big time - 1 star"></label>
            </fieldset>
            <label for="" class="agree mobileOff">最同意</label>
            </div>
          </div>
          <div class="form-group row line-2">
            <div class="left">這個網站提高您購買GERBER<sup>®</sup>產品的意欲</div>
            <div class="right">
            <label for="" class="disagree mobileOff">最不同意</label>
            <fieldset class="rating">
                <input type="radio" id="buy5" name="buy" value="5" /><label class="full hvr-grow" for="buy5" title="Awesome - 5 stars"></label>
                <input type="radio" id="buy4" name="buy" value="4" /><label class="full hvr-grow" for="buy4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="buy3" name="buy" value="3" /><label class="full hvr-grow" for="buy3" title="Meh - 3 stars"></label>
                <input type="radio" id="buy2" name="buy" value="2" /><label class="full hvr-grow" for="buy2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="buy1" name="buy" value="1" /><label class="full hvr-grow" for="buy1" title="Sucks big time - 1 star"></label>
            </fieldset>
            <label for="" class="agree mobileOff">最同意</label>
            </div>
          </div>
        <div class="form-group line-2">
          <div class="form-group row">
            <div class="left full">當提及伴隨BB成長的嬰幼兒食品品牌時，您會即時想起哪一個品牌的產品呢？</div>
            <!--<div class="bottom checkbox last-question">
                 <label class="checkbox-inline">
                  <input type="radio" name="brand" id="brand1" class="css-checkbox" checked="checked"  value="GERBER" /><label for="brand1" class="css-label">GERBER</label>
                </label>
                 <label class="checkbox-inline">
                  <input type="radio" name="brand" id="brand2" class="css-checkbox" value="Heniz"/><label for="brand2" class="css-label" >Heniz</label>
                </label>
                 <label class="checkbox-inline">
                  <input type="radio" name="brand" id="brand3" class="css-checkbox" value="Hipp" /><label for="brand3" class="css-label" >Hipp</label>
                </label>
                 <label class="checkbox-inline">
                  <input type="radio" name="brand" id="brand4" class="css-checkbox" value="Ella'Kitchen" /><label for="brand4" class="css-label" >Ella'Kitchen</label>
                </label>
                 <label class="checkbox-inline">
                  <input type="radio" name="brand" id="brand5" class="css-checkbox" value="other"/><label for="brand5" class="css-label">其他(請註明)</label>
                </label>
            </div>-->
          </div>
          <div class="form-group">
            <textarea rows="8" class="form-control" id="message-text"></textarea>
          </div>
          <div class="form-group submit-button">
            <button type="submit" class="btn btn-secondary hvr-fade">提交</button>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
