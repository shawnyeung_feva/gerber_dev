<?php include 'templates/commonvar.php'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>單一穀物米粉 - DHA及益生菌</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="單一穀物米粉，讓寶寶享受第一口固體食物">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶,產品資訊">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="GERBER®單一穀物米粉" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="單一穀物米粉，讓寶寶享受第一口固體食物" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/product/stage1/3a.jpg" />
    <!-- google -->
    <meta itemprop="name" content="GERBER - 為何選擇Gerber">
    <meta itemprop="description" content="為人父母，總是希望子女能獲得最好的照料，健康成長。不過，隨著寶寶逐漸成長，父母每天都要為寶寶的營養需要而煩惱，面對排山倒海的育兒資訊，要選擇最合適的嬰幼兒食品並不容易。">
    <meta itemprop="image" content="<?php echo $baseUrl ?>images/aboutus/out_food.jpg" />
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
    <link href="../css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="../css/hover.css">
    <link rel="stylesheet" type="text/css" href="../css/animate.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/layout.css">
    <link rel="stylesheet" href="../css/product.css">
    <!-- Vendor: Javascripts -->

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.mobile.custom.min.js"></script>

    <!-- Our Website Javascripts -->
    <script src="../js/main.js"></script>
    <?php include 'templates/preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<?php include 'templates/header_product.php'; ?>

<div class="container">

    <div class="row">

        <div class="col-lg-12 header-type">
          <img class="mobileOn" id="Image-Maps-Com-image-maps-2016-06-01-054237" src="../images/menu-tab/mobile/stage1full.png" border="0" width="100%" usemap="#image-maps-2016-06-01-054237"  />
          <map name="image-maps-2016-06-01-054237" id="ImageMapsCom-image-maps-2016-06-01-054237">
          <area  alt="supported stage" title="supported stage" href="organicoatmeal" shape="rect" coords="0,0,86,111" style="outline:none;" target="_self"     />
          <area  alt="sitter stage" title="sitter stage" href="bananaappleic" shape="rect" coords="92,0,163,111" style="outline:none;" target="_self"     />
          <area  alt="crawler stage" title="crawler stage" href="organicveggies" shape="rect" coords="171,0,238,111" style="outline:none;" target="_self"     />
          <area  alt="toddler stage" title="toddler stage" href="lilbiscuit" shape="rect" coords="246,0,322,111" style="outline:none;" target="_self"     />
          <area shape="rect" coords="320,109,322,111" alt="Image Map" style="outline:none;" title="Image Map" href="#" />
          </map>

            <img class="img-responsive mobileOff" id="Image-Maps-Com-image-maps-2016-06-01-052803" src="../images/menu-tab/desktop/stage1full.png" border="0" usemap="#image-maps-2016-06-01-052803" alt="" />
            <map name="image-maps-2016-06-01-052803" id="ImageMapsCom-image-maps-2016-06-01-052803">
            <area title="supported stage" href="organicoatmeal" shape="rect" coords="16,2,273,106" style="outline:none;" target="_self"     />
            <area  alt="sitter stage" title="sitter stage" href="bananaappleic" shape="rect" coords="271,2,521,104" style="outline:none;" target="_self"     />
            <area  alt="crawler stage" title="crawler stage" href="organicveggies" shape="rect" coords="517,0,764,106" style="outline:none;" target="_self"     />
            <area  alt="toddler stage" title="toddler stage" href="lilbiscuit" shape="rect" coords="760,0,989,107" style="outline:none;" target="_self"     />
            <area shape="rect" coords="998,110,1000,112" alt="Image Map" style="outline:none;" title="Image Map" href="#" />
            </map>

        </div>

    </div>
</div>

<div class="container article">
    <div class="row title">
        <div class="col-lg-12">
            <h1 class="page-header">單一穀物米粉
            </h1>
            <small>DHA及益生菌</small>
            <ul class="share mobileOff">
              <li><a class="pop tw hvr-grow icon-twitter-logo-silhouette" name="twi"></a></li>
              <li><a class="pop gplus hvr-grow icon-google-plus" name="google"></a></li>
              <li><a class="pop fb hvr-grow icon-facebook-logo" name="fbook"></a></li>
              <li><a id="btn" class="link hvr-grow icon-unlink"></a></li>
            </ul>
            <div class="stage-icon mobileOff">
              <img src="../images/list/supported.png" alt="" />
              <span>6個月起</span>
            </div>
        </div>
    </div>

    <div class="row info">

        <div class="col-md-5 product-img">
            <img class="organic_icon" src="../images/organic-icon.png"> <img class="new_icon" src="../images/new-icon.png">
            <img class="img-responsive hvr-buzz-out" src="../images/product/stage1/3a.jpg">
            <div class="learn-more mobileOff">
                <a href="https://www.nestlebaby.hk/supported-sitter?utm_source=supportedsitter&utm_medium=RiceCereal&utm_campaign=Gerberhk" target="_blank">了解更多扶坐期育兒資訊<i class="fa fa-angle-right"></i></a>
            </div>
        </div>
        <div class="col-md-7 info-title">
            <div class="top-title no-top">
                <h2>產品詳情</h2>
            </div>
            <div class="scroll-pane mobileOff">
                <div class="info-head">
                    <p>單一穀物米粉，讓寶寶享受更豐富營養</p>
                </div>
                <div class="special">
                    <h4>產品特色</h4>
                    <ul>
                      <li>蘊含豐富鐵質，有助寶寶認知能力發展</li>
                      <li>DHA 能幫助寶寶眼睛及腦部發展</li>
                      <li>BL 益生菌，幫助維持寶寶腸道健康</li><!--
                      <li>VITABLOCKS<sup>®</sup>蘊含各種豐富營養，包括鐵、鋅、鈣、維他命C、維他命E及6種維他命B</li>-->
                      <li>每日兩份<sup>*</sup>，已能滿足寶寶90%的每日鐵質攝取量<sup>**</sup></li>
                      <li>VITABLOCKS<sup>®</sup>蘊含各種豐富營養，包括鐵、鋅、鈣、維他命C、維他命E及6種維他命B</li>
                      <li>蘊含豐富維他命C、維他命E及6種維他命B，有助寶寶健康發展</li>
                      <li>蘊含豐富鈣質有助保持骨骼及牙齒的健康</li>
                      <li>不添加人工調味料、人工色素、甜味劑及防腐劑</li>
                    </ul>
                   <p class="remark">
                      <sup>*</sup>每食用分量為15克
                    </p>
                   <p class="remark">
                      <sup>**</sup>根據National Institute of Agricultural Science (NAS), RDA, 1989, 嬰兒每日建議鐵質攝取量為15毫克
                    </p>
                </div>

                <!--
                <div class="adv">
                    <h4>其他優點</h4>
                    <ul>
                        <li>不添加人工調味料、色素、甜味劑及防腐劑。單一穀物嬰兒米粉最適合測試嬰兒對固體食物的敏感度，並有助培育寶寶的進食習慣</li>
                    </ul>
                </div>
                <div class="top-title">
                    <h2>食用、調配及貯存方法</h2>
                    <p>
                      米粉已徹底煮熟，只需加入液體即可食用。
                    </p>
                </div>
                <div class="waytouse">
                    <h4>首次食用米粉的寶寶：</h4>
                    <ul>
                      <li>將1湯匙米粉與4至5湯匙母乳或配方奶粉混合。</li>
                    </ul>
                </div>
                <div class="waytouse">
                    <h4>沖調方式：</h4>
                    <ul>
                      <li>將所需份量的米粉倒入或用匙羹舀進碗內。</li>
                      <li>嬰兒：加入母乳或嬰兒配方奶粉，攪拌至合適的黏稠度。</li>
                      <li>學行寶寶：1歲以上的寶寶可加入牛奶、水或GERBER<sup>®</sup>果汁攪拌。</li>
                      <li>可即時享用或先行加熱。</li>
                      <li>未吃完的米粉應倒掉。</li>
                    </ul>
                </div>
                <div class="waytouse">
                    <p class="remark">
                      貯存方法：貯存於陰涼乾爽地方，並於開封後30天內食用。
                    </p>
                    <br><br><br><br>
                </div>-->
            </div>
            <div class="mobileOn">
              <div class="info-head">
                  <p>DHA及益生菌單一穀物米粉，讓寶寶享受更豐富營養</p>
              </div>
              <div class="special">
                  <h4>產品特色</h4>
                    <ul>
                        <li>蘊含豐富鐵質，有助寶寶認知能力發展</li>
                        <li>DHA 能幫助寶寶眼睛及腦部發展</li>
                        <li>BL 益生菌，幫助維持寶寶腸道健康</li><!--
                        <li>VITABLOCKS<sup>®</sup>蘊含各種豐富營養，包括鐵、鋅、鈣、維他命C、維他命E及6種維他命B</li>-->
                        <li>每日兩份<sup>*</sup>，已能滿足寶寶90%的每日鐵質攝取量<sup>**</sup></li>
                        <li>VITABLOCKS<sup>®</sup>蘊含各種豐富營養，包括鐵、鋅、鈣、維他命C、維他命E及6種維他命B</li>
                        <li>蘊含豐富維他命C、維他命E及6種維他命B，有助寶寶健康發展</li>
                        <li>蘊含豐富鈣質有助保持骨骼及牙齒的健康</li>
                        <li>不添加人工調味料、人工色素、甜味劑及防腐劑</li>
                    </ul>
                   <p class="remark">
                      <sup>*</sup>每食用分量為15克
                    </p>
                   <p class="remark">
                      <sup>**</sup>根據National Institute of Agricultural Science (NAS), RDA, 1989, 嬰兒每日建議鐵質攝取量為15毫克
                    </p>
              </div>
              <!--
              <div class="adv">
                  <h4>其他優點</h4>
                  <ul>
                      <li>不添加人工調味料、色素、甜味劑及防腐劑。單一穀物嬰兒米粉最適合測試嬰兒對固體食物的敏感度，並有助培育寶寶的進食習慣</li>
                  </ul>
              </div>
              <div class="top-title hidden-div">
                  <h2>食用、調配及貯存方法</h2>
                  <p>
                    米粉已徹底煮熟，只需加入液體即可食用。
                  </p>
              </div>
              <div class="waytouse hidden-div">
                  <h4>首次食用米粉的寶寶：</h4>
                  <ul>
                    <li>將1湯匙米粉與4至5湯匙母乳或配方奶粉混合。</li>
                  </ul>
              </div>
              <div class="waytouse hidden-div">
                  <h4>沖調方式：</h4>
                  <ul>
                    <li>將所需份量的米粉倒入或用匙羹舀進碗內。</li>
                    <li>嬰兒：加入母乳或嬰兒配方奶粉，攪拌至合適的黏稠度。</li>
                    <li>學行寶寶：1歲以上的寶寶可加入牛奶、水或GERBER<sup>®</sup>果汁攪拌。</li>
                    <li>可即時享用或先行加熱。</li>
                    <li>未吃完的米粉應倒掉。</li>
                  </ul>
              </div>
              <div class="waytouse hidden-div">
                  <p class="remark">
                    貯存方法：貯存於陰涼乾爽地方，並於開封後30天內食用。
                  </p>
              </div>
                <div class="load-more">
                  <i class="fa fa-angle-down"></i>
                </div>-->
                <div class="learn-more">
                    <a href="https://www.nestlebaby.hk/supported-sitter?utm_source=supportedsitter&utm_medium=RiceCereal&utm_campaign=Gerberhk" target="_blank">了解更多扶坐期育兒資訊<i class="fa fa-angle-right"></i></a>
                </div>
            </div>
        </div>

    </div>

    <div class="row" style="">
        <div class="col-lg-12 line">
        </div>
        <h1 class="page-header related-title">其他相同階段產品
            </h1>
    </div>

    <div class="row related-product" style="">
             <ul id="lightSlider">
              <li style="border:0;">
                  <a class="hvr-buzz-out" href="ricecereal" style="background: url(../images/product/stage1/1b.jpg)">
                  </a>
                  <h3>單一穀物米粉<br><span class="desc">純米味</span></h3>
              </li>
              <li style="border:0;">
                  <a class="hvr-buzz-out" href="organicoatmeal" style="background: url(../images/product/stage1/2b.jpg)">
                  </a>
                  <h3>有機燕麥粉</h3>
              </li>
            </ul>
    </div>
<Br><br><br><Br>
</div>
<!-- /.container -->

<?php include 'templates/product/form.php'; ?>
<?php include 'templates/footer.php'; ?>

<script src="../plugin/jscroll/jquery.jscrollpane.min.js"></script>
<script src="../plugin/jscroll/jquery.mousewheel.js"></script>
<link rel="stylesheet" href="../plugin/jscroll/jquery.jscrollpane.css">
<script src="../plugin/lightslider/lightslider.min.js"></script>
<script src="../js/imageMapResizer.js"></script>
<script src="../js/product.js"></script>
<link rel="stylesheet" href="../plugin/lightslider/lightslider.css">

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BabyFood_Gerber_Product information - organicoatmeal
URL of the webpage where the tag is expected to be placed:
https://www.gerber.com.hk/productinformation/organicoatmeal
This tag must be placed between the <body> and </body> tags, as close as possible to the
opening tag.
Creation Date: 07/25/2017
-->
<iframe
src="https://8039665.fls.doubleclick.net/activityi;src=8039665;type=gerber00;cat=ger_10;dc_l
at=;dc_rdid=;tag_for_child_directed_treatment=;ord=<?php session_start(); echo str_replace(array(",","-","_"),"",session_id()); ?>?" width="1" height="1"
frameborder="0" style="display:none"></iframe>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</body>
</html>
