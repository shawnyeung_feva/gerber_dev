<?php include 'commonvar.php'; ?>
<?php $aboutus_page = 'active'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>GERBER - 為何選擇Gerber</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="為人父母，總是希望子女能獲得最好的照料，健康成長。不過，隨著寶寶逐漸成長，父母每天都要為寶寶的營養需要而煩惱，面對排山倒海的育兒資訊，要選擇最合適的嬰幼兒食品並不容易。">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="GERBER - 為什麼選擇GERBER®產品?" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="為人父母，總是希望子女能獲得最好的照料，健康成長。不過，隨著寶寶逐漸成長，父母每天都要為寶寶的營養需要而煩惱，面對排山倒海的育兒資訊，要選擇最合適的嬰幼兒食品並不容易。" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/aboutus/kv_whygerber.jpg" />
    <!-- google -->
    <meta itemprop="name" content="GERBER - 為什麼選擇GERBER®產品?">
    <meta itemprop="description" content="為人父母，總是希望子女能獲得最好的照料，健康成長。不過，隨著寶寶逐漸成長，父母每天都要為寶寶的營養需要而煩惱，面對排山倒海的育兒資訊，要選擇最合適的嬰幼兒食品並不容易。">
    <meta itemprop="image" content="<?php echo $baseUrl ?>images/aboutus/kv_whygerber.jpg" />
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/hover.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/layout.css">

    <!-- Vendor: Javascripts -->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mobile.custom.min.js"></script>

    <!-- Our Website Javascripts -->
    <script src="js/main.js"></script>
    <?php include 'preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<?php include 'header.php'; ?>

<div class="container article">
    <div class="row title">
        <div class="col-lg-12">
            <h1 class="page-header">為什麼選擇GERBER<sup>®</sup>產品?
            </h1>
            <ul class="share mobileOff">
              <li><a class="pop tw hvr-grow icon-twitter-logo-silhouette" name="twi"></a></li>
              <li><a class="pop gplus hvr-grow icon-google-plus" name="google"></a></li>
              <li><a class="pop fb hvr-grow icon-facebook-logo" name="fbook"></a></li>
              <li><a id="btn" class="link hvr-grow icon-unlink"></a></li>
            </ul>
        </div>
    </div>
    <div class="row kv">
        <img src="images/aboutus/kv_whygerber.jpg" class="img-responsive">
    </div>
    <div class="row">
        <div class="col-lg-12 content spilt-pra">
            <p>為人父母，總是希望子女能獲得最好的照料，健康成長。不過，隨著寶寶逐漸成長，父母每天都要為寶寶的營養需要而煩惱，面對排山倒海的育兒資訊，要選擇最合適的嬰幼兒食品並不容易。</p>
            <p>因此，在幫助寶寶健康成長的道路上，GERBER<sup>®</sup>品牌致力成為最值得父母信任的夥伴，協助寶寶在成長階段培養健康的飲食習慣。</p>
            <div class="title-1"><h1>GERBER<sup>®</sup>產品營養之旅</h1></div>
            <p>為了令我們的寶寶能健康成長，GERBER<sup>®</sup>品牌設計了一套營養之旅。GERBER<sup>®</sup>產品營養之旅是一套獨一無二的營養方案，以科研為本，提供專為初生嬰兒至4歲幼兒而設的嬰幼兒食品、營養資訊及指引，協助父母選擇合適的嬰幼兒食品，全方位照顧寶寶的營養需要。GERBER<sup>®</sup>產品營養之旅從餵哺母乳開始，並加入精心研發、營養豐富的嬰幼兒食品，照顧扶坐期以至學齡前期的營養需要。</p>

        </div>
    </div>

</div>


<?php include 'product/form.php'; ?>
<?php include 'footer.php'; ?>
<link rel="stylesheet" href="css/aboutus.css">
<script src="js/wow.js"></script>
<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BabyFood_Gerber_About us - choosegerber
URL of the webpage where the tag is expected to be placed:
https://www.gerber.com.hk/choosegerber
This tag must be placed between the <body> and </body> tags, as close as possible to the
opening tag.
Creation Date: 07/25/2017
-->
<iframe
src="https://8039665.fls.doubleclick.net/activityi;src=8039665;type=gerber00;cat=ger_07;dc_l
at=;dc_rdid=;tag_for_child_directed_treatment=;ord=<?php session_start(); echo str_replace(array(",","-","_"),"",session_id()); ?>?" width="1" height="1"
frameborder="0" style="display:none"></iframe>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</body>
</html>
