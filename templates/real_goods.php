<?php include 'commonvar.php'; ?>
<?php $video_page = 'active'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>GERBER - 分辨正貨</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="認清GERBER®正貨好重要，因為水貨貨源不明，無經過出口檢查，包裝和成份未必符合出口規格和本地營養成 份﹑聲稱及標籤要求。水貨運送期間，有可能發生因溫差﹑濕度等問題令產品變質。為左對寶寶健康有保障，媽媽需要認識如何分辨GERBER®正貨。">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="GERBER - 分辨正貨" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="認清GERBER®正貨好重要，因為水貨貨源不明，無經過出口檢查，包裝和成份未必符合出口規格和本地營養成 份﹑聲稱及標籤要求。水貨運送期間，有可能發生因溫差﹑濕度等問題令產品變質。為左對寶寶健康有保障，媽媽需要認識如何分辨GERBER®正貨。" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/aboutus/real_good_mb.jpg" />
    <!-- google -->
    <meta itemprop="name" content="GERBER - 分辨正貨">
    <meta itemprop="description" content="認清GERBER®正貨好重要，因為水貨貨源不明，無經過出口檢查，包裝和成份未必符合出口規格和本地營養成 份﹑聲稱及標籤要求。水貨運送期間，有可能發生因溫差﹑濕度等問題令產品變質。為左對寶寶健康有保障，媽媽需要認識如何分辨GERBER®正貨。">
    <meta property="og:image" content="<?php echo $baseUrl ?>images/aboutus/real_good_mb.jpg" />
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/hover.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/layout.css">

    <!-- Vendor: Javascripts -->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mobile.custom.min.js"></script>

    <!-- Our Website Javascripts -->
    <script src="js/main.js"></script>
    <?php include 'preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<?php include 'header.php'; ?>

<div class="container article goods">
    <div class="row title">
        <div class="col-lg-12">
            <h1 class="page-header">如何分辨GERBER<sup>®</sup>產品正貨 讓BB健康成長
            </h1>
            <ul class="share mobileOff">
              <li><a class="pop tw hvr-grow icon-twitter-logo-silhouette" name="twi"></a></li>
              <li><a class="pop gplus hvr-grow icon-google-plus" name="google"></a></li>
              <li><a class="pop fb hvr-grow icon-facebook-logo" name="fbook"></a></li>
              <li><a id="btn" class="link hvr-grow icon-unlink"></a></li>
            </ul>
        </div>
        <div class="col-lg-12 content">
            <p>認清GERBER<sup>®</sup>產品正貨好重要，因為水貨貨源不明，無經過出口檢查，包裝和成份未必符合出口規格和本地營養成份﹑聲稱及標籤要求。水貨運送期間，有可能發生因溫差﹑濕度等問題令產品變質。為了對寶寶健康有保障，媽媽需要認識如何分辨GERBER<sup>®</sup>產品正貨。</p>
        </div>
    </div>
    <div id="nav-icon4">
      <span></span>
      <span></span>
      <span></span>
    </div>
    <div class="row kv youtube">
        <iframe width="100%" height="560" src="https://www.youtube.com/embed/-45PA1worcU?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="row">
        <div class="col-lg-12 content no-float">
            <div class="title-1">
                <h1>為BB著想 認明標籤正貨</h1>
            </div>
            <div class="real-good-img mobileOff">
                <img class="img-reponsive" src="images/video_img.png" width="100%"><!--
                <div class="tips-1">認名標籤上有<br>Nestlé Nutrition<br>Services: 2179 8333</div>
                <div class="tips-2">GERBER<sup>®</sup><br>專屬QR Code</div>-->
            </div>
            <div class="real-good-img mobileOn">
                <img class="img-reponsive" src="images/video_img_m.png" width="100%">
                <br><br>
                <ol class="real_list">
                    <li>香港正貨貼紙</li>
                    <li>認名標籤上有 Nestlé Nutrition Services: 2179 8333</li>
                    <li>GERBER<sup>®</sup> 專屬QR Code</li>
                </ol>
            </div>
        </div>
    </div>
</div>




<?php include 'product/form.php'; ?>
<?php include 'footer.php'; ?>
<link rel="stylesheet" href="css/aboutus.css">

<script type="text/javascript">
$(document).ready(function() {
  $.ajax({
      type: "POST",
      url: baseUrl+"api/videoupdate",
      cache: false,
      success: function(data)
          {
            console.log(data);
          },
      error: function(xhr, status, error) {
        alert(error);
      }
      });
});

</script>

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BabyFood_Gerber_Official good
URL of the webpage where the tag is expected to be placed:
https://www.gerber.com.hk/officialgood
This tag must be placed between the <body> and </body> tags, as close as possible to the
opening tag.
Creation Date: 07/25/2017
-->
<iframe
src="https://8039665.fls.doubleclick.net/activityi;src=8039665;type=gerber00;cat=ger_03;dc_l
at=;dc_rdid=;tag_for_child_directed_treatment=;ord=<?php session_start(); echo str_replace(array(",","-","_"),"",session_id()); ?>?" width="1" height="1"
frameborder="0" style="display:none"></iframe>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</body>
</html>
