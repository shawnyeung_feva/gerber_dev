<?php include 'commonvar.php'; ?>

<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>GERBER</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="為您介紹對您的嬰兒而設的有益健康的食物。我們GERBER®的嬰兒食品配方分別與不同的嬰兒進行小組測試，確保它們微小的味蕾而設，而食譜是我們精心精心挑選和質量檢驗而成。">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="GERBER" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="為您介紹對您的嬰兒而設的有益健康的食物。我們GERBER®的嬰兒食品配方分別與不同的嬰兒進行小組測試，確保它們微小的味蕾而設，而食譜是我們精心精心挑選和質量檢驗而成" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/banner/kv-1.jpg" />
    <!-- google -->
    <meta itemprop="name" content="GERBER">
    <meta itemprop="description" content="為您介紹對您的嬰兒而設的有益健康的食物。我們GERBER®的嬰兒食品配方分別與不同的嬰兒進行小組測試，確保它們微小的味蕾而設，而食譜是我們精心精心挑選和質量檢驗而成">
    <meta itemprop="image" content="<?php echo $baseUrl ?>images/banner/kv-1.jpg">
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/hover.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/layout.css">

    <!-- Vendor: Javascripts -->

    <script src="js/jquery.min.js"></script><!--
    <script src="js/jquery-migrate-3.0.0.min.js"></script>-->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mobile.custom.min.js"></script>

    <!-- Our Website Javascripts -->
    <script src="js/main.js"></script>
    <?php include 'preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<?php include 'header.php'; ?>




<div id="disclaimer" class="content_popup  mfp-hide">
    <div class="text">世界衛生組織建議寶寶出生的首六個月完全以母乳餵哺，雀巢公司對此全力支持，而且支持在醫護人員的建議下引入輔食品的同時持續母乳餵哺<br/><br/>
        <div id="closeDisclaimer" class="popup-modal-dismiss button">繼續</div>
    </div>
</div>



<div id="myCarousel" class="carousel slide">
   <!-- Indicators -->
   <ol class="carousel-indicators mobileOn">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    
   </ol>
   <!-- Wrapper for slides -->
   <div class="carousel-inner animated fadeIn">
      <div class="item active">
         <div class="fill">
             <img class="bg-desktop" src="images/banner/905124-Gerber-WebPage-2017-Dec_Organic_OP.JPG" width="100%">
             <img class="bg-mobile" src="images/banner/905124-Gerber-WebPage-2017-Dec_Organic_OP.JPG" width="100%">
         </div>
      </div>
      <div class="item">
         <div class="fill">
         <img class="bg-desktop" src="images/banner/905124-Gerber-WebPage-2017-Dec_cereal_OP.JPG" width="100%">
         <img class="bg-mobile" src="images/banner/905124-Gerber-WebPage-2017-Dec_cereal_OP.JPG" width="100%">
         </div>
      </div>
      <!--
      <div class="item">
         <div class="fill" style="background-image:url('images/banner/kv-1.jpg');"></div>
 
            <div class="carousel-caption">
                <h1>Reduce costs of finishing operations by 65%</h1>
            </div>
      </div>-->
      <!--
      <div class="item">
         <div class="fill" style="background-image:url('images/banner/kv-2.jpg');"></div>
      </div>
      <div class="item">
         <div class="fill" style="background-image:url('images/banner/kv-3.jpg');"></div>
      </div>
      -->
   </div>
   <!-- Controls -->
   <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <i class="hvr-grow fa fa-angle-left"></i>
   </a>
   <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <i class="hvr-grow fa fa-angle-right"></i>
   </a>
</div>
<div class="section s1" id="s1">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12">
            <div class="">
              <h3>GERBER<sup>®</sup>嬰幼兒食品</h3>
              <ul class="share mobileOff">
                <li><a class="pop tw hvr-grow icon-twitter-logo-silhouette" name="twi"></a></li>
                <li><a class="pop gplus hvr-grow icon-google-plus" name="google"></a></li>
                <li><a class="pop fb hvr-grow icon-facebook-logo" name="fbook"></a></li>
                <li><a id="btn" class="link hvr-grow icon-unlink"></a></li>
              </ul>
            </div>
            <p>GERBER<sup>®</sup>品牌致力成為值得父母信賴的夥伴，協助寶寶在不同成長階段建立健康的飲食習慣。</p>
         </div>
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container -->
</div>
<!-- /.section -->
<div class="section s2" id="s2">
   <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/gerber-dha-probiotic-cereal-rice">
            <img class="img-responsive img-home-portfolio" src="images/list/home/supported_3_home.jpg" width="100%">
            <span class="title color1">單一穀物米粉</span>
            <span class="short-desc">DHA及益生菌</span>
            </a>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/ricecereal">
            <img class="img-responsive img-home-portfolio" src="images/list/home/supported_1_home.jpg" width="100%">
            <span class="title color1">單一穀物米粉</span>
            <span class="short-desc">純米味</span>
            </a>
         </div>
        <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/organicoatmeal">
            <img class="organic_icon" src="images/organic-icon.png">
            <img class="new_icon" src="images/new-icon.png">
            <img class="img-responsive img-home-portfolio" src="images/list/home/supported_2_home.jpg" width="100%">
            <span class="title color1">有機燕麥粉</span>
            <span class="short-desc"></span>
            </a>
         </div>
         <!-- sitter -->
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/bananaappleic">
            <img class="img-responsive img-home-portfolio" src="images/list/home/sitter_0_home.jpg" width="100%">
            <span class="title color2">穀物及水果米粉</span>
            <span class="short-desc">香蕉蘋果味</span>
            </a>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/applejuice">
            <img class="img-responsive img-home-portfolio" src="images/list/home/sitter_1_home.jpg" width="100%">
            <span class="title color2">100%蘋果汁</span>
            <span class="short-desc"></span>
            </a>
         </div><!--
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/蘋果汁">
            <img class="img-responsive img-home-portfolio" src="images/list/home/sitter_2_home.jpg" width="100%">
            <span class="title color2">蘋果汁</span>
            <span class="short-desc"></span>
            </a>
         </div>-->
         <!--
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/第二階段食品">
            <img class="img-responsive img-home-portfolio" src="images/list/home/sitter_3_home.jpg" width="100%">
            <span class="title color2">第二階段食品<sup>®</sup></span>
            <span class="short-desc">蘋果味</span>
          </a>
        </div>--><!--
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/蘋果藍莓蓉">
            <img class="img-responsive img-home-portfolio" src="images/list/home/sitter_4_home.jpg" width="100%">
            <span class="title color2">蘋果藍莓蓉</span>
            <span class="short-desc"></span>
            </a>
         </div>--><!--
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/蘋果蓉">
            <img class="img-responsive img-home-portfolio" src="images/list/home/sitter_5_home.jpg" width="100%">
            <span class="title color2">蘋果蓉</span>
            <span class="short-desc"></span>
            </a>
         </div>-->
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/chickengravy">
            <img class="img-responsive img-home-portfolio" src="images/list/home/sitter_6_home_v2.jpg" width="100%">
            <span class="title color2">雞肉蓉</span>
            <span class="short-desc"></span>
            </a>
         </div>
         <!-- end sitter -->
         <!-- crawler -->
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/gerber-lil-bits-cereal-oatmeal-banana-strawberry">
            <img class="organic_icon" src="images/organic-icon.png">
            <img class="img-responsive img-home-portfolio" src="images/list/home/crawler_9_home.jpg" width="100%">
            <span class="title color3">燕麥粉</span>
            <span class="short-desc">香蕉士多啤梨味</span>
            </a>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/gerber-organic-yogurt-melts-banana-strawberry">
            <img class="organic_icon" src="images/organic-icon.png">
            <img class="img-responsive img-home-portfolio" src="images/list/home/crawler_10_home.jpg" width="100%">
            <span class="title color3">有機乳酪乾</span>
            <span class="short-desc">香蕉士多啤梨味</span>
            </a>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/organicveggies">
            <img class="organic_icon" src="images/organic-icon.png">
            <img class="img-responsive img-home-portfolio" src="images/list/home/crawler_0_home.jpg" width="100%">
            <span class="title color3">有機星星餅</span>
            <span class="short-desc">蔬菜味</span>
            </a>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/organicapple">
            <img class="organic_icon" src="images/organic-icon.png">
            <img class="img-responsive img-home-portfolio" src="images/list/home/crawler_1_home.jpg" width="100%">
            <span class="title color3">有機星星餅</span>
            <span class="short-desc">蘋果味</span>
            </a>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/peachpuff">
            <img class="img-responsive img-home-portfolio" src="images/list/home/crawler_2_home.jpg" width="100%">
            <span class="title color3">星星餅</span>
            <span class="short-desc">蜜桃味</span>
            </a>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/bananapuff">
            <img class="img-responsive img-home-portfolio" src="images/list/home/crawler_3_home.jpg" width="100%">
            <span class="title color3">星星餅</span>
            <span class="short-desc">香蕉味</span>
            </a>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/sweetpotatopuff">
            <img class="img-responsive img-home-portfolio" src="images/list/home/crawler_4_home.jpg" width="100%">
            <span class="title color3">星星餅</span>
            <span class="short-desc">甜薯味</span>
            </a>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/strawberryapplepuff">
            <img class="img-responsive img-home-portfolio" src="images/list/home/crawler_5_home.jpg" width="100%">
            <span class="title color3">星星餅</span>
            <span class="short-desc">士多啤梨蘋果味</span>
            </a>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/arrowrootcookies">
            <img class="img-responsive img-home-portfolio" src="images/list/home/crawler_6_home.jpg" width="100%">
            <span class="title color3">薏米曲奇餅</span>
            <span class="short-desc"></span>
            </a>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/ymmixedberries">
            <img class="img-responsive img-home-portfolio" src="images/list/home/crawler_7_home.jpg" width="100%">
            <span class="title color3">乳酪乾</span>
            <span class="short-desc">雜莓味</span>
            </a>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/ympeach">
            <img class="img-responsive img-home-portfolio" src="images/list/home/crawler_8_home.jpg" width="100%">
            <span class="title color3">乳酪乾</span>
            <span class="short-desc">蜜桃味</span>
            </a>
         </div>
         <!-- end crawler -->
         <!-- toddler -->
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/lilbiscuit">
            <img class="img-responsive img-home-portfolio" src="images/list/home/toddler_1_home.jpg" width="100%">
            <span class="title color4">牙仔餅</span>
            <span class="short-desc">小麥呍呢嗱味</span>
            </a>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/animalcrackers">
            <img class="img-responsive img-home-portfolio" src="images/list/home/toddler_2_home.jpg" width="100%">
            <span class="title color4">動物餅</span>
            <span class="short-desc">肉桂味</span>
            </a>
         </div>
        <div class="col-lg-4 col-md-4 col-sm-6 wow item fadeIn hvr-grow" data-wow-duration="0.5s" data-wow-delay="0.1s">
            <a href="productinformation/bittybitesstrawberry">
            <img class="new_icon top-p" src="images/new-icon.png">
            <img class="img-responsive img-home-portfolio" src="images/list/home/toddler_3_home.jpg" width="100%">
            <span class="title color4">士多啤梨方脆</span>
            <span class="short-desc"></span>
            </a>
         </div>
         <!-- end toddle -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container -->
</div>
<!-- /.section -->
<div class="section s3" id="s3">
   <div class="container">
      <div class="row no-gutter flex-row">
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 wow fadeInIn panel learn-more mobileOff" data-wow-duration="0.5s" data-wow-delay="0.1s" style="background:url(images/home_learn_more_img.jpg);">
         </div>
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 wow fadeInIn learn-more info" data-wow-duration="0.5s" data-wow-delay="0.1s" style="background-color:#ffffff">
            <a class="desc" href="officialgood">
              <h4>做個精明媽媽，選擇GERBER<sup>®</sup>產品正貨，讓寶寶健康成長。</h4>
            </a>
            <a class="go-link " href="officialgood" >
              <button class="hvr-fade">了解更多</button>
            </a>
         </div>
      </div><!--
      <div class="row mobileOn">
        <div class="col-md-12">
          <a href="分辨正貨" class="mb-learn-more">了解更多</a>
        </div>
      </div>-->
      <!-- /.row -->
   </div>
   <!-- /.container -->
</div>
<!-- /.section -->


<?php include 'product/form.php'; ?>
<?php include 'footer.php'; ?>
<link rel="stylesheet" href="css/home.css">

<script src="js/home.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    wow = new WOW({
        boxClass: 'wow', // default
        animateClass: 'animated', // default
        offset: 0, // default
        mobile: false, // default
        live: true // default
    });
    wow.init();

  });
</script>

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BabyFood_Gerber_Homepage
URL of the webpage where the tag is expected to be placed: https://www.gerber.com.hk/
This tag must be placed between the <body> and </body> tags, as close as possible to the
opening tag.
Creation Date: 07/25/2017
-->
<iframe
src="https://8039665.fls.doubleclick.net/activityi;src=8039665;type=gerber00;cat=ger_01;dc_l
at=;dc_rdid=;tag_for_child_directed_treatment=;ord=<?php session_start(); echo str_replace(array(",","-","_"),"",session_id()); ?>?" width="1" height="1"
frameborder="0" style="display:none"></iframe>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</body>
</html>
