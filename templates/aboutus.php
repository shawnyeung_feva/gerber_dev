<?php include 'commonvar.php'; ?>
<?php $aboutus_page = 'active'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>GERBER - 關於我們</title>
    <meta charset="utf-8">
    <base href="">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="GERBER®的目標與天下父母一樣：讓寶寶健康快樂地成長。因此，在寶寶的早期成長階段，我們致力成為最值得父母信賴的夥伴。GERBER 的產品能配合孩子在各個成長階段的發展及營養需要，加上完善的服務和工具，扶助寶寶踏出健康的第一步。在引領寶寶健康快樂成長的道路上，我們一直相伴在旁。">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <!-- face book -->
    <meta property="og:title" content="GERBER - 關於我們" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="GERBER®的目標與天下父母一樣：讓寶寶健康快樂地成長。因此，在寶寶的早期成長階段，我們致力成為最值得父母信賴的夥伴。GERBER 的產品能配合孩子在各個成長階段的發展及營養需要，加上完善的服務和工具，扶助寶寶踏出健康的第一步。在引領寶寶健康快樂成長的道路上，我們一直相伴在旁" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/aboutus/kv_aboutus.jpg" />
    <!-- google -->
    <html itemscope itemtype="http://schema.org/Product">
    <meta itemprop="name" content="GERBER - 關於我們">
    <meta itemprop="description" content="GERBER®的目標與天下父母一樣：讓寶寶健康快樂地成長。因此，在寶寶的早期成長階段，我們致力成為最值得父母信賴的夥伴。GERBER 的產品能配合孩子在各個成長階段的發展及營養需要，加上完善的服務和工具，扶助寶寶踏出健康的第一步。在引領寶寶健康快樂成長的道路上，我們一直相伴在旁">
    <meta itemprop="image" content="<?php echo $baseUrl ?>images/aboutus/kv_aboutus.jpg">
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/hover.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/layout.css">

    <!-- Vendor: Javascripts -->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mobile.custom.min.js"></script>

    <!-- Our Website Javascripts -->
    <script src="js/main.js"></script>
    <?php include 'preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<?php include 'header.php'; ?>

<div class="container article">
    <div class="row title">
        <div class="col-lg-12">
            <h1 class="page-header">為何選擇GERBER<sup>®</sup>產品？
            </h1>
            <ul class="share mobileOff">
              <li><a class="pop tw hvr-grow icon-twitter-logo-silhouette" name="twi"></a></li>
              <li><a class="pop gplus hvr-grow icon-google-plus" name="google"></a></li>
              <li><a class="pop fb hvr-grow icon-facebook-logo" name="fbook"></a></li>
              <li><a id="btn" data-clipboard-text="" class="link hvr-grow icon-unlink"></a></li>
            </ul>
        </div>
    </div>
    <div id="nav-icon4">
      <span></span>
      <span></span>
      <span></span>
    </div>
    <div class="row kv">
        <img src="images/aboutus/kv_aboutus.jpg" width="100%">
    </div>
    <div class="row">
        <div class="col-lg-12 content">
            <p>GERBER<sup>®</sup>品牌的目標與天下父母一樣：讓寶寶健康快樂地成長。因此，在寶寶的早期成長階段，我們致力成為最值得父母信賴的夥伴。GERBER<sup>®</sup>產品能配合孩子在各個成長階段的發展及營養需要，加上完善的服務和工具，扶助寶寶踏出健康的第一步。在引領寶寶健康快樂成長的道路上，我們一直相伴在旁。</p>
        </div>
    </div>
</div>


<div class="container s2">
    <div class="row">
        <div class="col-sm-4 col-xs-12 nopadding">
            <div class="col-md-12 left">
                <div class="title">GERBER<sup>®</sup>品牌的使命</div>
                <div class="desc">了解GERBER<sup>®</sup>產品營養之旅對寶寶健康成長與發展的重要性。</div>
                <div class="pic" onclick="location.href='choosegerber';"><img src="images/aboutus/logo.png"></div>
                <div class="pic-title" onclick="location.href='choosegerber';">為什麼選擇GERBER<sup>®</sup>產品?</div>
                <div class="link"><a class="mobileOff hvr-fade" href="choosegerber">詳情&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a><a class="mobileOn" href="choosegerber"><img src="images/aboutus/arrow-right.jpg" alt="" /></a></div>
            </div>
        </div>
        <div class="col-sm-8 col-xs-12 nopadding">
            <div class="col-md-12 right">
                <div class="title">GERBER<sup>®</sup>品牌的承諾</div>
                <div class="desc">為您帶來GERBER<sup>®</sup>優質嬰兒食品</div>
            </div>
            <div class="col-lg-12 right-bottom" style="float: left;width: 100%;">
                <div class="col-sm-6 col-xs-12 border-margin">
                    <div class="pic" onclick="location.href='safetyandresearch';"><img class="zoom" src="images/aboutus/zoom.jpg"></div>
                    <div class="pic-title" onclick="location.href='safetyandresearch';">安全與科研</div>
                    <div class="link"><a class="mobileOff hvr-fade" href="safetyandresearch">詳情&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a><a class="mobileOn" href="safetyandresearch"><img src="images/aboutus/arrow-right.jpg" alt="" /></a></div>
                </div>
                <div class="col-sm-6 col-xs-12 border-margin">
                    <div class="pic" onclick="location.href='ouringredient';"><img class="book" src="images/aboutus/book.jpg"></div>
                    <div class="pic-title" onclick="location.href='ouringredient';">我們的食材</div>
                    <div class="link"><a class="mobileOff hvr-fade" href="ouringredient">詳情&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a><a class="mobileOn" href="ouringredient"><img src="images/aboutus/arrow-right.jpg" alt="" /></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container -->



<?php include 'footer.php'; ?>
<link rel="stylesheet" href="css/aboutus.css">

<script type="text/javascript">

</script>
<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BabyFood_Gerber_About us
URL of the webpage where the tag is expected to be placed:
https://www.gerber.com.hk/aboutus
This tag must be placed between the <body> and </body> tags, as close as possible to the
opening tag.
Creation Date: 07/25/2017
-->
<iframe
src="https://8039665.fls.doubleclick.net/activityi;src=8039665;type=gerber00;cat=ger_04;dc_l
at=;dc_rdid=;tag_for_child_directed_treatment=;ord=<?php session_start(); echo str_replace(array(",","-","_"),"",session_id()); ?>?" width="1" height="1"
frameborder="0" style="display:none"></iframe>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</body>
</html>
