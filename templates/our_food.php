<?php include 'commonvar.php'; ?>
<?php $aboutus_page = 'active'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>GERBER - 我們的食材</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="作為父母，您有權知道寶寶的食物是否安全健康。因此，我們會提供有關GERBER®食品及原材料的清晰資訊，讓您為寶寶挑選最合適的產品。">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="GERBER - 我們的食材" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="作為父母，您有權知道寶寶的食物是否安全健康。因此，我們會提供有關GERBER®食品及原材料的清晰資訊，讓您為寶寶挑選最合適的產品。" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/aboutus/out_food.jpg" />
    <!-- google -->
    <meta itemprop="name" content="GERBER - 我們的食材">
    <meta itemprop="description" content="作為父母，您有權知道寶寶的食物是否安全健康。因此，我們會提供有關GERBER®食品及原材料的清晰資訊，讓您為寶寶挑選最合適的產品。">
    <meta itemprop="image" content="<?php echo $baseUrl ?>images/aboutus/out_food.jpg" />
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/hover.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/layout.css">

    <!-- Vendor: Javascripts -->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mobile.custom.min.js"></script>

    <!-- Our Website Javascripts -->
    <script src="js/main.js"></script>
    <?php include 'preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<?php include 'header.php'; ?>

<div class="container article">
    <div class="row title">
        <div class="col-lg-12">
            <h1 class="page-header">我們的食材
            </h1>
            <ul class="share mobileOff">
              <li><a class="pop tw hvr-grow icon-twitter-logo-silhouette" name="twi"></a></li>
              <li><a class="pop gplus hvr-grow icon-google-plus" name="google"></a></li>
              <li><a class="pop fb hvr-grow icon-facebook-logo" name="fbook"></a></li>
              <li><a id="btn" class="link hvr-grow icon-unlink"></a></li>
            </ul>
        </div>
    </div>
    <div class="row kv">
        <img src="images/aboutus/out_food.jpg" class="img-responsive">
    </div>
    <div class="row">
        <div class="col-lg-12 content spilt-pra">
            <p>作為父母，您有權知道寶寶的食物是否安全健康。因此，我們會提供有關GERBER<sup>®</sup>食品及原材料的清晰資訊，讓您為寶寶挑選最合適的產品。</p>
            <p>我們為所有產品制定嚴謹的品質及安全標準，而大部分標準更比政府的規定更嚴格，只因我們相信，孩子應該得到最好的營養。</p>
            <p>另一方面，我們也與供應商緊密合作，部分供應商更是GERBER<sup>®</sup>品牌合作數十年的可靠夥伴。</p>
            <p>GERBER<sup>®</sup>品牌承諾保証產品符合環境與顧客的需要。如對GERBER<sup>®</sup>嬰幼兒食品成份有任何疑問或意見，歡迎致電2179 8333與我們的營養顧問聯絡。</p>
        </div>
    </div>

</div>


<?php include 'product/form.php'; ?>
<?php include 'footer.php'; ?>
<link rel="stylesheet" href="css/aboutus.css">
<script src="js/wow.js"></script>

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BabyFood_Gerber_About us - ouringredient
URL of the webpage where the tag is expected to be placed:
https://www.gerber.com.hk/ouringredient
This tag must be placed between the <body> and </body> tags, as close as possible to the
opening tag.
Creation Date: 07/25/2017
-->
<iframe
src="https://8039665.fls.doubleclick.net/activityi;src=8039665;type=gerber00;cat=ger_09;dc_l
at=;dc_rdid=;tag_for_child_directed_treatment=;ord=<?php session_start(); echo str_replace(array(",","-","_"),"",session_id()); ?>?" width="1" height="1"
frameborder="0" style="display:none"></iframe>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</body>
</html>
