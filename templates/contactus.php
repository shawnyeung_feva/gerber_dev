<?php include 'commonvar.php'; ?>
<?php $contactus_page = 'active'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>GERBER - 聯絡我們</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content=" ">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="GERBER - 聯絡我們" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/aboutus/video_img.jpg" />
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/hover.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/layout.css">

    <!-- Vendor: Javascripts -->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mobile.custom.min.js"></script>

    <!-- Our Website Javascripts -->
    <script src="js/main.js"></script>
    <?php include 'preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<?php include 'header.php'; ?>
<span id="btn" style="dispaly:none"></span>
<div class="container">
    <div class="row title">
        <div class="col-lg-12 header">
            <h1 class="page-header">聯絡我們<span>&nbsp;*此乃必須填寫之項目，否則將無法處理閣下之查詢。</span>
            </h1>
        </div>
        <div class="col-lg-12 content">
            <form id="contactus" role="form" data-toggle="validator">
              <div class="form-group row">
                <label for="contact-type" class="col-sm-2 form-control-label">查詢類別*</label>
                <div class="col-sm-3">
                    <select id="contact-type" class="form-control c-select">
                      <option selected>請選擇</option>
                      <option value="會員查詢">會員查詢</option>
                      <option value="產品查詢">產品查詢</option>
                      <option value="網站查詢">網站查詢</option>
                      <option value="食譜查詢">食譜查詢</option>
                      <option value="其他">其他</option>
                    </select>
                    <span class="error"></span>
                </div>
              </div>
              <div class="form-group row">
                <label for="surname" class="col-sm-2 form-control-label">姓氏</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" id="surname" placeholder="">
                </div>
              </div>
              <div class="form-group row">
                <label for="first-name" class="col-sm-2 form-control-label">名字*</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" id="first-name" placeholder="">
                  <span class="error"></span>
                </div>
              </div>
              <div class="form-group row">
                <label for="email-address" class="col-sm-2 form-control-label">電郵地址*</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="email-address" placeholder="">
                  <span class="error"></span>
                </div>
              </div>
              <div class="form-group row">
                <label for="message" class="col-sm-2 form-control-label">您的訊息*</label>
                <div class="col-sm-10">
                  <textarea class="form-control" id="message" rows="7"></textarea>
                  <span class="error"></span>
                </div>
              </div>
              <div class="line">

              </div>
              <div class="form-group row">
                <label class="col-sm-12">
                    <input type="checkbox" name="agreement" id="agreement" class="css-checkbox" /><label for="agreement" class="css-label tnc">*我已細閱並接受網站之 條款及細則 及 私隱政策 <span class="error error-checkbox">

                </span> </label>
                </label>

              </div>

              <div class="form-group row">
                <label class="col-sm-12">
                    <label class="text tnc">
                      如不同意我們或第三方使用閣下的個人資料向閣下提供有關嬰幼兒營養、食品及護理的服務資訊及產品，請於方格內加“ ✔ ”。
                    </label>
                </label>
              </div>
              <div class="form-group row">
                <label class="col-sm-12">
                    <input type="checkbox" name="disagree" id="disagree" class="css-checkbox" /><label for="disagree" class="css-label tnc">我不同意雀巢香港有限公司及其關聯公司或協助其作直接營銷之第三方使用我的個人資料向我提供有關嬰幼兒營養、食品及護理的服務資訊及產品。</label>
                </label>
              </div>
              <div class="form-group row">
                <label class="col-sm-12">
                    <label class="text tnc">
                      成功查詢後，我明白我可以於任何時候查詢或更改我的個人資料或取消有關同意，但須給予雀巢營養顧問通告，聯絡資料見下述。
                    </label>
                </label>
              </div>
              <div class="form-group row">
                <div class="col-sm-12 submit-button">
                  <button type="submit" class="btn btn-secondary hvr-fade">提交</button>
                </div>
              </div>
            </form>
            <div class="info">
                    <ul class="clearfix">
                        <li>
                            <div class="ico"><i class="fa fa-phone"></i></div>
                            <div class="txt">
                                <div class="label">雀巢香港服務熱線：</div>
                                <h6><a href="tel:85221798333">(852) 2179 8333</a></h6>
                            </div>
                        </li>
                        <li>
                            <div class="ico"><i class="fa fa-phone"></i></div>
                            <div class="txt">
                                <div class="label">雀巢澳門服務熱線：</div>
                                <h6><a href="tel:8530800663">(853) 0800 663</a></h6>
                            </div>
                        </li>
                        <li>
                            <div class="ico"><i class="fa fa-map-marker"></i></div>
                            <div class="txt">
                                <div class="label">聯絡地址：</div>
                                <h6>香港東九龍郵政局郵政信箱68867號</h6>
                            </div>
                        </li>
                        <li>
                            <div class="ico"><i class="fa fa-envelope"></i></div>
                            <div class="txt">
                                <div class="label">電郵地址：</div>
                                <h6><a href="mailto:consumerservices@hk.nestle.com">consumerservices@hk.nestle.com</a></h6>
                            </div>
                        </li>
                        <li>
                            <div class="ico"><i class="fa fa-facebook"></i></div>
                            <div class="txt">
                                <div class="label">Facebook 專頁：</div>
                                <h6><a href="http://www.facebook.com/nestlebabyhk" target="_blank">雀巢營養天地 Nestlé And Nutrition</a></h6>
                            </div>
                        </li>
                        <li>
                            <div class="ico"><i class="fa fa-hand-o-up"></i></div>
                            <div class="txt">
                                <div class="label">官方網頁：</div>
                                <h6><a href="http://www.nestle.com.hk/" target="_blank">雀巢香港</a></h6>
                            </div>
                        </li>
                    </ul>
                </div>
        </div>
    </div>
</div>



<?php include 'product/form.php'; ?>
<?php include 'footer.php'; ?>
<link rel="stylesheet" href="css/contactus.css">
<script src="js/contactus.js"></script>

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BabyFood_Gerber_Contact us
URL of the webpage where the tag is expected to be placed:
https://www.gerber.com.hk/contactus
This tag must be placed between the <body> and </body> tags, as close as possible to the
opening tag.
Creation Date: 07/25/2017
-->
<iframe
src="https://8039665.fls.doubleclick.net/activityi;src=8039665;type=gerber00;cat=ger_05;dc_l
at=;dc_rdid=;tag_for_child_directed_treatment=;ord=<?php session_start(); echo str_replace(array(",","-","_"),"",session_id()); ?>?" width="1" height="1"
frameborder="0" style="display:none"></iframe>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</body>
</html>
