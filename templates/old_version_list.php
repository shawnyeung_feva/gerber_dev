<?php include 'header.php'; ?>

<!-- Page Content -->
<div class="container">
   <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">About
                <small>It's Nice to Meet You!</small>
            </h1>
        </div>
   </div>
</div>
<div class="container list">
   <!-- Service Images -->
   <div class="row">
      <div class="item wow fadeIn panel" data-wow-duration="2s" data-wow-delay="0.1s" style="background: url(images/list_1.jpg);">
      </div>
      <div class="item wow fadeIn panel hvr-border-fade" data-wow-duration="2s" data-wow-delay="0.1s" style="background: url(images/list_2.jpg);">
      </div>
      <div class="item wow fadeIn panel hvr-border-fade" data-wow-duration="2s" data-wow-delay="0.1s" style="background: url(images/list_2.jpg);">
      </div>
   </div>
   <div class="row col-2 mobileRemove">
      <div class="item wow fadeIn panel" data-wow-duration="2s" data-wow-delay="0.1s" style="background: url(images/list_1.jpg);">
      </div>
      <div class="item wow fadeIn panel hvr-border-fade" data-wow-duration="2s" data-wow-delay="0.1s" style="background: url(images/list_2.jpg);">
      </div>
      <div class="item wow fadeIn panel hvr-border-fade" data-wow-duration="2s" data-wow-delay="0.1s" style="background: url(images/list_2.jpg);">
      </div>
   </div>
   <div class="row">
      <div class="item wow fadeIn panel" data-wow-duration="2s" data-wow-delay="0.1s" style="background: url(images/list_1.jpg);">
      </div>
      <div class="item wow fadeIn panel hvr-border-fade" data-wow-duration="2s" data-wow-delay="0.1s" style="background: url(images/list_2.jpg);">
      </div>
      <div class="item wow fadeIn panel hvr-border-fade" data-wow-duration="2s" data-wow-delay="0.1s" style="background: url(images/list_2.jpg);">
      </div>
   </div>
   <!-- /.row -->
</div>
<!-- /.container -->

<?php include 'footer.php'; ?>


<script src="js/wow.js"></script> 
<link rel="stylesheet" href="css/list.css">
<script type="text/javascript">
    wow = new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       0,          // default
          mobile:       false,       // default
          live:         true        // default
        }
        )
    wow.init();
    $(document).ready(function() {
        // 1:1 item
        resizeHeight();
        window.onresize = function(event) {
            resizeHeight();
        };

        function resizeHeight(){
            var cw = $('.item').width();
            $('.item').css({'height':cw+'px'});       
        }

        if($(window).width() <= 767){
          $('.mobileRemove').removeClass('col-2');
        }
 
    });  
</script>

</body>
</html>