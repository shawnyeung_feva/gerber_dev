<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1552874268296703'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1552874268296703&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<?php include_once("ga.php") ?>

<div id="menu-overlay" style="display:none;"></div>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button id="nav-icon4" type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span></span>
                <span></span>
                <span></span>
                <!--<span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>-->
            </button>
            <!-- You'll want to use a responsive image option so this logo looks good on devices - I recommend using something like retina.js (do a quick Google search for it and you'll find it) -->
            <a class="navbar-brand" href="<?php echo $baseUrl ?>"><img src="../images/logo.jpg" class="" alt="Gerber" title="Gerber"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="mobileOff desktop-menu">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="<?php echo $baseUrl ?>productinformation" class="hvr-underline-reveal wow fadeIn" data-wow-delay="0.1s">產品資訊</a></li>
              <li class="mobileOff wow fadeIn" data-wow-delay="1s"><span></span></li>

              <li><a href="<?php echo $baseUrl ?>aboutus" class="hvr-underline-reveal wow fadeIn" data-wow-delay="0.3s">關於我們</a>
              </li>
              <li class="mobileOff wow fadeIn" data-wow-delay="1s"><span></span></li>
              <li><a href="<?php echo $baseUrl ?>officialgood" class="hvr-underline-reveal wow fadeIn" data-wow-delay="0.5s">分辨正貨</a>
              </li>
              <li class="mobileOff wow fadeIn" data-wow-delay="1s"><span></span></li>
              <li><a href="<?php echo $baseUrl ?>contactus" class="hvr-underline-reveal wow fadeIn" data-wow-delay="0.7s">聯絡我們</a>
              </li>
                <li class="mobileOff wow fadeIn" data-wow-delay="1s"><span></span></li>
                <li><a href="<?php echo $baseUrl ?>contest" class="hvr-underline-reveal wow fadeIn active" data-wow-delay="0.9s">Gerber® 寶寶食「相」大賽</a>
                </li> 
              <li class="mobileOff wow fadeIn" data-wow-delay="1s"><span></span></li>
              <li><a href="https://www.nestlebaby.hk/?utm_source=Gerberweb&utm_medium=SHSHlogo&utm_campaign=Gerberhk" target="_blank" class="nestle-logo wow fadeIn" data-wow-delay="0.9s"><img src="<?php echo $baseUrl ?>images/nestle_logo.png" alt="" /></a>
              </li>
                <!--
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown">Blog <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#/blog">List of Posts</a>
                        </li>
                        <li><a href="#/blog/post">View One Post</a>
                        </li>
                    </ul>
                </li>-->
            </ul>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="m-nav" style="list-style: none;">
                <li class="share-block">
                  <ul class="share">
                    <li><a class="pop tw" name="twi"></a></li>
                    <li><a class="pop gplus" name="google"></a></li>
                    <li><a class="pop fb" name="fbook"></a></li>
                    <li><a class="watsapp"></a></li>
                    <li><a id="btn-mb" href="#small-dialog" data-clipboard-text="" class="link"></a></li>
                  </ul>
                </li>

                <li><a href="../productinformation">產品資訊</a>
                </li>
                <li><a href="../aboutus">關於我們</a>
                </li>
                <li><a href="../officialgood">分辨正貨</a>
                </li>
                <li><a href="../contactus">聯絡我們</a>
                </li>
                <li><a href="../contest">Gerber® 寶寶食「相」大賽</a>
                </li>
                <li><a href="https://www.nestlebaby.hk/?utm_source=Gerberweb&utm_medium=SHSHlogo&utm_campaign=Gerberhk" target="_blank" class="nestle-logo hvr-pop"><img src="../images/nestle_logo.png" alt="" /></a>
                </li>
            </ul>
        </div>

        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
