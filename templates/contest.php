<?php
define('_VALID_INCLUDE', TRUE); // flag to allow include or require files
$dir_level = "./"; //set the required files located

require_once($dir_level.'includes/vars.inc.php');
require_once($dir_level.'includes/common.inc.php');

?>
<?php include 'commonvar.php'; ?>
<?php $contest_page = 'active'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>GERBER - Gerber® 寶寶食「相」大賽</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content=" ">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="GERBER - Gerber® 寶寶食「相」大賽" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/aboutus/video_img.jpg" />
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/hover.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="plugin/swiper/v3/swiper.min.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/layout.css">

    <!-- Vendor: Javascripts -->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mobile.custom.min.js"></script>
    <script src="plugin/swiper/v3/swiper.min.js"></script>
    <script src="plugin/checkUserAgent/checkUserAgent.js"></script>
    <script src="https://use.fontawesome.com/60efdea8b0.js"></script>
    <!-- Our Website Javascripts -->
    <script src="js/main.js"></script>
    <?php include 'preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<?php include 'header.php'; ?>
<span id="btn" style="dispaly:none"></span>
<section id="kv">
    <div class="container">
        <div class="bg-wrap">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="bg" style="background-image: url('images/contest/banner_1.jpg');">
                            <div class="word-wrap">
                                <img src="">
                                <img src="">
                                <div class="link"><a class="hvr-fade" href="contest/step1">立即參加&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a></div>
                            </div>
                            <div class="tnc"><a href="#pop-tnc"><sup>*</sup><u>條款及細則</u></a></div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="bg" style="background-image: url('images/contest/banner_1.jpg');">
<div class="vdo-wrap">
    <div class='embed-container'><iframe src='http://www.youtube.com/embed/QILiHiTD3uc?rel=0&amp;showinfo=0' frameborder='0' allowfullscreen></iframe></div>
</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
        <img src="images/contest/line.png" class="line visible-md visible-lg">
    </div>
</section>
<?php

// open db connection
$conn = openConnection($conn);

?>
<section id="upload">
    <div class="container">
        <div class="head">最新上傳</div>
    </div>
    <div class="visible-xs visible-sm">
        <div class="swiper-container">
        <div class="swiper-wrapper">
<?php
$sql = " select * from `gerberfev_game_201710` where 1 ";
$sql.= " and `status` = '1' ";
$sql.= " order by `created_date` desc, `id` desc ";
$sql.= " LIMIT 0, 10";

$result = mysql_query($sql, $conn);
$num = mysql_num_rows($result);

while ($row = mysql_fetch_array($result)) {
	
	if ($row["photo_type"] == "1") {
		$photo_type = "first";
	} else
	if ($row["photo_type"] == "2") {
		$photo_type = "new";
	} else
	if ($row["photo_type"] == "3") {
		$photo_type = "organic";
	}
?>
            <div class="swiper-slide">
                <div class="thumb">
                    <div class="candidate <?php echo $photo_type; ?>"><div class="photo" style="background-image: url('<?php echo $row["photo_thumb"] ?>');"></div></div>
                    <div class="desc">
                        <div class="name"><?php echo $row["bb_name"] ?></div>
                        <div class="icon-wrap">
                            <div class="like"><i class="fa fa-thumbs-up hvr-fade" aria-hidden="true"></i><span><?php echo $row["like"] ?></span></div>
                            <div class="share"><i class="fa fa-share-alt hvr-fade" aria-hidden="true"></i></div>
                        </div>
                    </div>
                </div>
            </div>
<?php
}
?>
        </div>
        </div>
    </div>
    <div class="visible-md visible-lg">
        <div class="container">
            <div class="album-wrap">
<?php
$sql = " select * from `gerberfev_game_201710` where 1 ";
$sql.= " and `status` = '1' ";
$sql.= " order by `created_date` desc, `id` desc ";
$sql.= " LIMIT 0, 10";

$result = mysql_query($sql, $conn);
$num = mysql_num_rows($result);

while ($row = mysql_fetch_array($result)) {
	
	if ($row["photo_type"] == "1") {
		$photo_type = "first";
	} else
	if ($row["photo_type"] == "2") {
		$photo_type = "new";
	} else
	if ($row["photo_type"] == "3") {
		$photo_type = "organic";
	}
	
	$sql = " select `id` from `gerberfev_game_201710_like_count_log` where 1 ";
	$sql.= " and `bb_id` = '".htmlencode($row["id"])."' ";
	$sql.= " and `created_ip` = '".get_client_ip()."' ";
	$sql.= " and `status` = '1' ";

	$result2 = mysql_query($sql, $conn);
	$num2 = mysql_num_rows($result2);

	if ($num2 <= 0) {
		$bb_like_class = "hvr-fade";
	} else {
		$bb_like_class = "";
	}
?>
                <div class="thumb">
                    <div class="candidate <?php echo $photo_type; ?>"><div class="photo" style="background-image: url('<?php echo $row["photo_thumb"] ?>');"></div></div>
                    <div class="desc">
                        <div class="name"><?php echo $row["bb_name"] ?></div>
                        <div class="icon-wrap">
                            <div class="like" bb-id="<?php echo $row["id"]; ?>" ><i class="fa fa-thumbs-up <?php echo $bb_like_class; ?>" aria-hidden="true"></i><span class="count"><?php echo $row["like"] ?></span></div>
                            <div class="share" onclick="shareFB('<?php echo $baseUrl."contest?id=".$row["id"] ?>');"><i class="fa fa-share-alt hvr-fade" aria-hidden="true"></i></div>
                        </div>
                    </div>
                </div>
<?php
}
?>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="link"><a class="hvr-fade" href="contest/album">查看更多&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a></div>
        <img src="images/contest/line.png" class="line visible-md visible-lg">
    </div>
</section>
<?php

// close db connection
$conn = closeConnection($conn);

?>
<section id="prize">
    <div class="container">
        <div class="row"> 
            <div class="col col-xs-12 col-sm-6 col-md-4">
                <div class="box">
                    <div class="top">
                        <img id="trophy" src="images/contest/prize_1.png">
                    </div>
                    <div class="bottom">
                        <div class="title">滋味大賞（共10名）</div>
                        <div class="desc">
                            Gerber® 評審將會挑選出
                            10份作品，相片中的小主角將可
                            獲得一份獨一無二、印有自己樣子的特別版產品作為紀念！
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-xs-12 col-sm-6 col-md-4">
                <div class="box">
                    <div class="top">
                        <img id="yogurt" src="images/contest/prize_2.png">
                    </div>
                    <div class="bottom">
                        <div class="title">滋味優先賞（共300名）</div>
                        <div class="desc">
                            首300名成功上傳作品的參賽者 即可獲得Gerber® 全新產品 —— 有機乳酪乾乙包。
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-xs-12 col-sm-6 col-md-4">
                <div class="box">
                    <div class="top">
                        <img id="coupon" src="images/contest/prize_3.png">
                    </div>
                    <div class="bottom">
                        <div class="title">滋味齊齊賞</div>
                        <div class="desc">
                            家長可自由選擇參賽組別，
                            只要成功上傳作品，
                            即可獲得購物優惠券，數量有限，
                            送完即止。
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

<div id="pop-tnc" class="mfp-hide">
    <div class="wrap">
        <div class="pop-close"><i class="icon ion-close-round"></i></div>
        <h1>《回味每刻》寶寶照片大賽條款及細則</h1>
        <ol>
            <li>是次活動的主辦機構為雀巢香港有限公司，下稱「雀巢香港」。</li>
            <li>參加者屬自願性質參加本活動，參加本活動前，所有參加者必須細閱及遵守本條款及細則。凡參與此活動之人士即表示同意並接受本條款及細則之約束。</li>
            <li>參加者必須為年滿18歲之香港居民，並於香港定居，不接受海外之參加者。</li>
            <li>每張相片中必須出現該組別的GERBER®產品，否則將被取消資格。</li>
            <li>相中寶寶在拍攝相片時的年齡為6個月以上，否則相片將進一步審核，或參加資格將被取消，而不獲另行通知。</li>
            <li>禮品數量有限，送完即止。</li>
            <li>參加者須根據活動步驟指示，分享相片。</li>
            <li>所有參賽作品必須由雀巢香港審核方可公開發佈，並由雀巢香港進行評分。</li>
            <li>每位參加者只有一次贏取禮品的機會。</li>
            <li>活動日期由即日至2017 年8月18日（香港時間下午23時59分結束），活動結束後收到的申請一概不獲接受。</li>
            <li>參加者一旦參加是次活動，即表示同意雀巢香港有權修改、複印、刊登、發放或使用其參賽作品而不另致酬。雀巢香港將擁有所有參賽作品之版權並可作為商業用途。</li>
            <li>所有參加紀錄將會在整個活動結束後由雀巢香港一併整理，得獎名單將於2017年X月XX日前於雀巢營養天地Facebook專頁公佈。得獎者將會於2017年X月XX日或之前收到個別通知。</li>
            <li>得獎者必須保證所遞交的個人聯絡資料均為真實且正確。倘若經查證屬實有關資料不實或不正確，雀巢香港有權取消參加者的資格而無須另行通知。</li>
            <li>禮品圖片只供參考，一切以實物為準。禮品不得轉讓、更換、退回、作價銷售、送贈或轉售予他人，亦不得兌換為現金或其他替代品。</li>
            <li>參加者所提供之個人資料僅供是次推廣活動之用，雀巢香港將按照《個人資料(私隱) 條例》處理所收集的個人資料。</li>
            <li>雀巢香港及是次推廣活動的廣告或代理公司之員工及其直系家屬均不得參加是次活動，以示公允。</li>
            <li>倘若參加者導致雀巢香港或第三方遭受任何損害，則必須承擔一切相關責任。</li>
            <li>雀巢香港有權酌情選擇價值相等或相若的任何其他禮品代替原有禮品，且無須提前諮詢或通知參加者，得獎者將不可因此情況而對雀巢香港提出任何索償。</li>
            <li>雀巢香港保留更換禮品及處理任何頒發禮品之權力。</li>
            <li>雀巢香港保留擴大、修改、取消、終止該活動以及更改活動截止日期的權利，無需作出解釋或通知。</li>
            <li>對於是次活動之參加時間，將以雀巢香港所用之伺服器所獲得之數據作準。如有任何因電腦、網路、技術或不可歸責於雀巢香港之理由，而令參加者所寄出、登錄之資料或令雀巢香港寄出之通知有遲延、遺失、錯誤、無法辨識或毀損之情況，雀巢香港不負任何法律責任，參加者亦不得有任何異議。</li>
            <li>如有任何爭議，雀巢香港保留最終決定及解釋權。</li>
            <li>如有查詢，請致電雀巢營養服務: (852) 2179-8333。</li>
        </ol>
    </div>
</div>
<?php include 'footer.php'; ?>
<link rel="stylesheet" href="css/contest.css">
<script src="js/contest.js"></script>

<script>
$(document).ready(function() {	
	// Contest

	$(document).on("click", '#upload .icon-wrap div.like', function(e) {
		e.preventDefault();
		
		if (!$(this).hasClass("active")) {
			
			
			var that = $(this);
				
			$.post("<?php echo $baseUrl; ?>php/save_testimonial_like.php","id="+$(this).attr("bb-id")+"&r="+new Date().getTime(), function(data) {
			
				submitting = false;
				
				if (data.status == "success") {

					that.find("span.count").text(data.like_count);
					that.addClass("active");
					that.find("i").removeClass("hvr-fade");
				
				} else if (data.status == "repeat") {
					
					that.addClass("active");
					that.find("i").removeClass("hvr-fade");
					
				} else {
					
					//alert("系統繁忙，請再嘗試");
					
				}
				
			},"json");
			
		}
		
		//ga('send', 'event', 'Button', 'click', 'Like');

	});

	$(document).on("click", '#upload .icon-wrap div.share', function(e) {
		//e.preventDefault();
		
		$.post("<?php echo $baseUrl; ?>php/save_testimonial_share.php","id="+$(this).siblings("div.like").attr("bb-id")+"&type=fb&r="+new Date().getTime(), function(data) {
			
		},"json");	
		
		//ga('send', 'event', 'Share', 'click', 'Facebook');

	});
});	
</script>
</body>
</html>
