<?php include 'commonvar.php'; ?>
<?php $aboutus_page = 'active'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>GERBER - 安全與研究</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="GERBER®非常重視產品安全及品質，從挑選原材料至控制製成品規格，我們也遵循嚴格的規範。產品更經過多次品質控制測試，確保符合監管要求，讓父母更安心。">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="GERBER - 安全與研究" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="GERBER®非常重視產品安全及品質，從挑選原材料至控制製成品規格，我們也遵循嚴格的規範。產品更經過多次品質控制測試，確保符合監管要求，讓父母更安心。" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/aboutus/safe_1.jpg" />
    <!-- google -->
    <meta itemprop="name" content="GERBER - 安全與研究">
    <meta itemprop="description" content="GERBER®非常重視產品安全及品質，從挑選原材料至控制製成品規格，我們也遵循嚴格的規範。產品更經過多次品質控制測試，確保符合監管要求，讓父母更安心。">
    <meta itemprop="image" content="<?php echo $baseUrl ?>images/aboutus/safe_1.jpg" />
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/hover.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/layout.css">

    <!-- Vendor: Javascripts -->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mobile.custom.min.js"></script>

    <!-- Our Website Javascripts -->
    <script src="js/main.js"></script>
    <?php include 'preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<?php include 'header.php'; ?>

<div class="container article">
    <div class="row title">
        <div class="col-lg-12">
            <h1 class="page-header">安全與科研
            </h1>
            <ul class="share mobileOff">
              <li><a class="pop tw hvr-grow icon-twitter-logo-silhouette" name="twi"></a></li>
              <li><a class="pop gplus hvr-grow icon-google-plus" name="google"></a></li>
              <li><a class="pop fb hvr-grow icon-facebook-logo" name="fbook"></a></li>
              <li><a id="btn" class="link hvr-grow icon-unlink"></a></li>
            </ul>
        </div>
    </div>
    <div id="nav-icon4">
      <span></span>
      <span></span>
      <span></span>
    </div>
    <div class="row kv">
        <img src="images/aboutus/safe_1.jpg" class="img-responsive">
    </div>
    <div class="row">
        <div class="col-lg-12 content muti-pra safety-info">
            <div class="title-1"><h1>產品安全及品質</h1></div>
            <div><p>GERBER<sup>®</sup>品牌非常重視產品安全及品質，從挑選原材料至控制製成品規格，我們也遵循嚴格的規範。產品更經過多次品質控制測試，確保符合監管要求，讓父母更安心。</p></div>
            <div class="title-2"><h2>嚴選優質食材</h2></div>
            <div><p>GERBER<sup>®</sup>嬰幼兒食品均採用優質原材料製造。我們亦按照食材認證計劃，只與符合雀巢嚴謹標準的供應商合作。</p></div>
            <div><p>此外，GERBER<sup>®</sup>品牌亦設有品質控制計劃，每一批GERBER<sup>®</sup>嬰幼兒食品於出廠前必須通過驗證，通過微生物測試等主要品質測試。我們竭力確保產品的營養水平準確合適，並符合所有監管規定，適合寶寶食用，才會推出市面。</p></div>
            <div class="title-2"><h2>嚴謹標準</h2></div>
            <div><p>GERBER<sup>®</sup>品牌致力確保所有產品、成份及包裝均符合最新的食品安全、品質及監管標準。為了令顧客安心選購，我們的內部品質及安全標準甚至比美國食品藥物管理局的規定更嚴謹。</p></div>
            <div><p>我們亦與主要供應商通力合作，保障安全及品質。我們與原材料和包裝供應商緊密合作，充分溝通，清楚說明GERBER<sup>®</sup>品牌對品質的期望，確保供應商能遵守我們嚴謹的品質及食品安全指引。我們亦特別制定產品物流及貨運程序，確保原材料優質安全。</p></div>
            <div><p>原材料運抵GERBER<sup>®</sup>產品的廠房後，會先經過仔細檢測，確保符合內部要求。經確認後，我們會仔細追蹤原材料在每一個生產步驟中的使用狀況。</p></div>
            <div class="pic">
                <div class="left"><img class="img-responsive" src="images/aboutus/safe_2.jpg"></div>
                <div class="right"><img class="img-responsive" src="images/aboutus/safe_3.jpg"></div>
            </div>
            <div class="title-2"><h2>健康食品始於健康食材</h2></div>
            <div><p>只有營養豐富、健康天然的材料才能成就健康的嬰幼兒食品。GERBER<sup>®</sup>品牌在研發產品時，會遵循政府及公司的內部準則，並每年進行產品評估，識別進一步改良的機會。GERBER<sup>®</sup>產品研發工作皆按照嚴謹的準則進行，並參考科研實證，以及美國兒科學會、美國心臟協會、食品藥物管理局、美國農業部、國家科學院及醫學院的建議。</p></div>
            <div><p>我們亦會對食品進行全面評估，確保其大小、形狀及質感均符合寶寶各個成長階段的需要。</p></div>
        </div>
    </div><!--
    <div class="row kv wow pulse">
        <img src="images/aboutus/safe_4.jpg" class="img-responsive">
    </div>-->
    <div class="row last">
        <div class="col-lg-12 content muti-pra safety-info">
            <div class="title-1"><h1>專業科研</h1></div>
            <div><p></p></div><!--
            <div class="title-2"><h2>安全與研究</h2></div>-->
            <div><p>雀巢的營養科研嚴謹精密，成為GERBER<sup>®</sup>產品的最強後盾。我們深信，科學能助我們研發更優質的產品，為寶寶打下更穩固的基礎。</p></div>
            <div><p>雀巢設有24間遍佈全球的研究及技術中心，食品研發網絡為食品業之冠。雀巢研究中心位於瑞士洛桑，是全球規模最大的私人營養研究中心。位於密歇根州費利蒙市的雀巢產品技術中心，則負責確保所有雀巢嬰兒食品品質。</p></div>
            <div><p>雀巢的全球研發網絡為父母帶來三大承諾：安全優質、營養健康、口味與口感皆迎合寶寶喜好，方便食用。</p></div>
            <div class="title-2"><h2>創新科研 鞏固寶寶健康</h2></div>
            <div><p>雀巢及GERBER<sup>®</sup>產品在營養研究方面取得多項重大突破。我們改良專為學行寶寶而設的GERBER<sup>®</sup>產品配方，減少反式脂肪與鈉含量，並採用更健康的脂肪源。</p></div>
            <div class="pic pic-3">
                <div class="left"><img class="img-responsive" src="images/aboutus/safe_5.jpg"></div>
                <div class="middle"><img class="img-responsive" src="images/aboutus/safe_6.jpg"></div>
                <div class="right"><img class="img-responsive" src="images/aboutus/safe_7.jpg"></div>
            </div>
            <div class="title-2"><h2>醫生團隊與寶寶試味專員</h2></div>
            <div><p>多年來，我們一直與兒科醫生、兒童心理學家攜手合作，研究最有助幼兒發展的食品。</p></div>
            <div><p>2003年，我們與科學及醫療團隊攜手設計GERBER<sup>®</sup>嬰幼兒飲食藍圖，確保研發出大小、質感與形狀各有不同的嬰幼兒食品，確保孩子在每個成長階段，也能找到合適的產品，有助培養正確的飲食習慣。我們亦精益求精，參考最新的資訊改良藍圖，令內容更切合嬰幼兒的實際需要。</p></div>
            <div><p>產品除了健康，也要美味，才能吸引寶寶。為了了解寶寶對產品的接受程度，我們特別邀請2,000名寶寶作為試味專員。透過細心觀察，我們可識別改善之處，令產品更能照顧寶寶的成長所需。</p></div>
            <div><p>無論過去還是未來，深入科研對雀巢的發展也至關重要。食物研究的工作尚有許多未知的領域，我們將繼續努力，兌現「Good Food, Good Life」的承諾。</p></div>
        </div>
    </div>
</div>


<?php include 'product/form.php'; ?>
<?php include 'footer.php'; ?>
<link rel="stylesheet" href="css/aboutus.css">
<script src="js/wow.js"></script>

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BabyFood_Gerber_About us - safetyandresearch
URL of the webpage where the tag is expected to be placed:
https://www.gerber.com.hk/safetyandresearch
This tag must be placed between the <body> and </body> tags, as close as possible to the
opening tag.
Creation Date: 07/25/2017
-->
<iframe
src="https://8039665.fls.doubleclick.net/activityi;src=8039665;type=gerber00;cat=ger_08;dc_l
at=;dc_rdid=;tag_for_child_directed_treatment=;ord=<?php session_start(); echo str_replace(array(",","-","_"),"",session_id()); ?>?" width="1" height="1"
frameborder="0" style="display:none"></iframe>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</body>
</html>
