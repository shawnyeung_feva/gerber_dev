<!-- dialog itself, mfp-hide class is required to make dialog hidden -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide">
  <br>
  <p>
    分享網站
  </p>
  <textarea id="mb-share"></textarea>
</div>
<footer class="section-colored text-center ng-scope">
    <div class="container">
        <ul>
        	<li><a href="https://www.nestle.com.hk/info/Pages/tc.aspx" target="_blank">私隱政策</a></li><!--
        	<li><a href="">條款及細則</a></li>-->
        	<li><a href="sitemap">網站地圖</a></li>
        </ul>
        <ul>
        	<li>©2017 Nestlé Hong Kong Ltd. All rights reserved.</li>
        </ul>
    </div>
    <div class="container">
        <ul>
            <li>世界衛生組織建議寶寶出生的首六個月完全以母乳餵哺，雀巢公司對此全力支持，而且支持在醫護人員的建議下引入輔食品的同時持續母乳餵哺。</li>
        </ul>
    </div>
</footer>
<a id="thankyou" href="#afterform-contactus" style="display:none;"></a>
<div id="afterform-contactus" class="zoom-anim-dialog mfp-hide">
  <br>
  <p>
    感謝你的寶貴意見。
  </p>
</div>
<a id="linkcopy" href="#copypopup" style="display:none;"></a>
<div id="copypopup" class="zoom-anim-dialog mfp-hide">
  <br>
  <p>
    你已成功複製。
  </p>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl ?>plugin/magnific/magnific-popup.css">
<link rel="stylesheet" type="text/css" href="<?php echo $baseUrl ?>images/share/icomoon/icommon.css">
<script type="text/javascript">
  var baseUrl = '<?php echo $baseUrl; ?>';
</script>

<script src="<?php echo $baseUrl ?>js/wow.js"></script>
<script src="<?php echo $baseUrl ?>js/fb.js"></script>
<script src="<?php echo $baseUrl ?>js/clipboard.min.js"></script>
<script src="<?php echo $baseUrl ?>plugin/magnific/jquery.magnific-popup.min.js"></script>

<!-- Google Code for Remarketing Tag --><!-------------------------------------------------- Remarketing tags may not be associated with personally identifiable information or placed on
pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup ------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 944439155;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""
src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/944439155/?guid=ON&amp;script=0"/>
</div>
</noscript>