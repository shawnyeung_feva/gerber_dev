<?php include 'commonvar.php'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>GERBER - 分辨正貨</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="認清GERBER®正貨好重要，因為水貨貨源不明，無經過出口檢查，包裝和成份未必符合出口規格和本地營養成 份﹑聲稱及標籤要求。水貨運送期間，有可能發生因溫差﹑濕度等問題令產品變質。為左對寶寶健康有保障，媽媽需要認識如何分辨GERBER®正貨。">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="GERBER - 分辨正貨" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="認清GERBER®正貨好重要，因為水貨貨源不明，無經過出口檢查，包裝和成份未必符合出口規格和本地營養成 份﹑聲稱及標籤要求。水貨運送期間，有可能發生因溫差﹑濕度等問題令產品變質。為左對寶寶健康有保障，媽媽需要認識如何分辨GERBER®正貨。" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/aboutus/real_good_mb.jpg" />
    <!-- google -->
    <meta itemprop="name" content="GERBER - 分辨正貨">
    <meta itemprop="description" content="認清GERBER®正貨好重要，因為水貨貨源不明，無經過出口檢查，包裝和成份未必符合出口規格和本地營養成 份﹑聲稱及標籤要求。水貨運送期間，有可能發生因溫差﹑濕度等問題令產品變質。為左對寶寶健康有保障，媽媽需要認識如何分辨GERBER®正貨。">
    <meta property="og:image" content="<?php echo $baseUrl ?>images/aboutus/real_good_mb.jpg" />
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/hover.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/layout.css">

    <!-- Vendor: Javascripts -->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mobile.custom.min.js"></script>

    <!-- Our Website Javascripts -->
    <script src="js/main.js"></script>
    <?php include 'preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<?php include 'header.php'; ?>
<?php include 'header.php'; ?>
<span id="btn" style="display:none"></span>
<div class="container article site-map">
    <div class="row title">
        <div class="col-lg-12">
            <h1 class="page-header">網站地圖
            </h1>
        </div>
    </div>
    <div class="row dot-border">
    </div>
    <div class="row">
        <div class="col-lg-12 content">
            <ul>
              <li><a href="<?php echo $baseUrl ?>">首頁</a></li>
              <li><a href="productinformation">產品資訊</a></li>
              <li><a href="aboutus">關於我們</a>
              <ul>
                <li><a href="choosegerber">為什麼選擇GERBER<sup>®</sup>產品</a></li>
                <li><a href="safetyandresearch">安全與研究</a></li>
                <li><a href="ouringredient">我們的食材</a></li>
              </ul>
              </li>
              <li><a href="officialgood">分辨正貨</a></li>
              <li><a href="contactus">聯絡我們</a></li>
            </ul>
        </div>
    </div>

</div>



<?php include 'footer.php'; ?>
<link rel="stylesheet" href="css/aboutus.css">
<script src="js/wow.js"></script>
<script type="text/javascript">
</script>
</body>
</html>
