<?php include 'commonvar.php'; ?>
<?php $list_page = 'active'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="zh-TW"> <!--<![endif]-->
<head>

    <!-- Meta-Information -->
    <title>GERBER - 產品資訊</title>
    <meta charset="utf-8">
    <base href="">

    <link rel="icon" href="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="產品資訊">
    <meta name="keywords" content="Baby,GERBER,嬰幼兒食品,寶寶,產品資訊">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- face book -->
    <meta property="og:title" content="GERBER - 產品資訊" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="產品資訊" />
    <meta property="og:image" content="<?php echo $baseUrl ?>images/list/dt_baby_bg.jpg" />
    <!-- google -->
    <meta itemprop="name" content="GERBER - 產品資訊">
    <meta itemprop="description" content="產品資訊">
    <meta itemprop="image" content="<?php echo $baseUrl ?>images/list/dt_baby_bg.jpg">
    <!-- Vendor: Bootstrap Stylesheets http://getbootstrap.com -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="css/hover.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/layout.css">
    <link rel="stylesheet" href="css/list.css">
    <!-- Vendor: Javascripts -->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mobile.custom.min.js"></script>

    <!-- Our Website Javascripts -->
    <script src="js/main.js"></script>
    <?php include 'preframe.php'; ?>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<?php include 'header.php'; ?>

<!-- Page Content -->
<div class="container prod-title">
   <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">產品資訊
            </h1>
            <ul class="share mobileOff">
              <li><a class="pop tw hvr-grow icon-twitter-logo-silhouette" name="twi"></a></li>
              <li><a class="pop gplus hvr-grow icon-google-plus" name="google"></a></li>
              <li><a class="pop fb hvr-grow icon-facebook-logo" name="fbook"></a></li>
              <li><a id="btn" class="link hvr-grow icon-unlink"></a></li>
            </ul>
        </div>
   </div>
</div>
<div class="container list">
   <!-- Service Images -->
   <div class="row">
      <div class="color1 item panel supported-root" data-child="supported"  onclick="btnCall(this);" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/supported_main.jpg);">
      <img class="mobileOn" src="images/list/supported.png" alt="" width="55"/>
      <span class="mobileOn title">6個月起<br><span>享受第一口固體食物</span></span>
      <span class="mobileOn arrow"><i class="fa fa-angle-down"></i></span>
      </div>

      <!-- Shawn 2018-01-03 -->
      <div class="item panel child supported color1" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/supported_3_home_v2.jpg);">
        <a href="productinformation/gerber-dha-probiotic-cereal-rice">
            <span class="title color1">單一穀物米粉</span>
            <span class="short-desc">DHA及益生菌</span>
        </a>
      </div>
      <!-- Shawn 2018-01-03 -->

      <div class="item panel hvr-border-fade child supported color1" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/supported_1_home_v2.jpg);">
        <a href="productinformation/ricecereal">
            <span class="title color1">單一穀物米粉</span>
            <span class="short-desc">純米味</span>
        </a>
      </div>
      <!--
      <div class="item panel child supported" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/dt_baby_bg.jpg);">

      </div>-->
   </div>
   <div class="row">
    <div class="item panel hvr-border-fade child supported color1" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/supported_2_home_v2.jpg);">
        <a href="productinformation/organicoatmeal">
            <img class="organic_icon" src="images/organic-icon.png">
            <img class="new_icon" src="images/new-icon.png">
            <span class="title color1">有機燕麥粉</span>
            <span class="short-desc"></span>
        </a>
      </div>
   </div>
   <div class="row col-2 mobileRemove">
      <div class="color2 item panel supported-root" data-child="sitter" onclick="btnCall(this);" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/sitter_main.jpg);">
        <img class="mobileOn" src="images/list/sitter.png" alt="" width="55"/>
        <span class="mobileOn title">6個月起<br><span>享受更多味道和食物組合</span></span>
        <span class="mobileOn arrow"><i class="fa fa-angle-down"></i></span>
      </div>
      <div class="item panel special-1 child sitter" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/dt_sitter_col_2_v2.jpg);">
        <a href="productinformation/bananaappleic">
            <span class="title color2">穀物及水果米粉</span>
            <span class="short-desc">香蕉蘋果味</span>
            <span class="learn-more color2 hvr-fade">了解詳情 <i class="fa fa-angle-right"></i></span>
        </a>
      </div>
   </div>
   <div class="row">
      <div class="item panel hvr-border-fade child sitter color2" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/sitter_1_home.jpg);">
        <a href="productinformation/applejuice">
            <span class="title color2">100%蘋果汁</span>
            <span class="short-desc"></span>
        </a>
      </div>
      <div class="item panel hvr-border-fade child sitter color2" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/sitter_6_home_v2.jpg);">
        <a href="productinformation/chickengravy">
            <span class="title color2">雞肉蓉</span>
            <span class="short-desc"></span>
        </a>
      </div>
      <!--
      <div class="item panel child supported" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/dt_baby_bg.jpg);">

      </div>-->

      <!--
      <div class="item panel hvr-border-fade child sitter color2" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/sitter_2_home.jpg);">
        <a href="productinformation/蘋果汁">
            <span class="title color2">蘋果汁</span>
            <span class="short-desc"></span>
        </a>
      </div>-->
      <!--
      <div class="item panel hvr-border-fade child sitter color2" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/sitter_3_home.jpg);">
        <a href="productinformation/">
            <span class="title color2">第二階段食品<sup>®</sup></span>
            <span class="short-desc">蘋果味</span>
        </a>
      </div>-->
      <!--
      <div class="item panel hvr-border-fade child sitter color2" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/sitter_4_home.jpg);">
        <a href="productinformation/蘋果藍莓蓉">
            <span class="title color2">蘋果藍莓蓉</span>
            <span class="short-desc"></span>
        </a>
      </div>-->
   </div><!--
   <div class="row">
      
      <div class="item panel hvr-border-fade child sitter color2" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/sitter_5_home.jpg);">
        <a href="productinformation/蘋果蓉">
            <span class="title color2">蘋果蓉</span>
            <span class="short-desc"></span>
        </a>
      </div>
   </div>-->
   <div class="row col-3 mobileRemove">
      <div class="color3 item panel supported-root" data-child="crawler" onclick="btnCall(this);" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/crawler_main.jpg);">
        <img class="mobileOn" src="images/list/crawler.png" alt="" width="55"/>
        <span class="mobileOn title">8個月起</span>
        <span class="mobileOn arrow"><i class="fa fa-angle-down"></i></span>
      </div>
      <div class="item panel special-1 crawler-0 child crawler" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/dt_crawler_col_2_v2.jpg);">
        <a href="productinformation/organicveggies">
          <img class="organic_icon col-2-icon" src="images/organic-icon.png">
            <span class="title color3">有機星星餅</span>
            <span class="short-desc">蔬菜味</span>
              <span class="learn-more color3 hvr-fade">了解詳情 <i class="fa fa-angle-right"></i></span>
        </a>
      </div>
   </div>

  <!-- Shawn 2018-01-03 -->
  <div class="row">
    <div class="item panel hvr-border-fade child crawler color3" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/crawler_9_home.jpg);">
        <a href="productinformation/gerber-lil-bits-cereal-oatmeal-banana-strawberry">
          <img class="organic_icon" src="images/organic-icon.png">
            <span class="title color3">燕麥粉</span>
            <span class="short-desc">香蕉士多啤梨味</span>
        </a>
      </div>
      <div class="item panel hvr-border-fade child crawler color3" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/crawler_10_home.jpg);">
        <a href="productinformation/gerber-organic-yogurt-melts-banana-strawberry">
          <img class="organic_icon" src="images/organic-icon.png">
            <span class="title color3">有機乳酪乾</span>
            <span class="short-desc">香蕉士多啤梨味</span>
        </a>
      </div>

      <div class="item panel hvr-border-fade child crawler color3" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/crawler_1_home_v2.jpg);">
        <a href="productinformation/organicapple">
          <img class="organic_icon" src="images/organic-icon.png">
            <span class="title color3">有機星星餅</span>
            <span class="short-desc">蘋果味</span>
        </a>
      </div>

  </div>
  <!-- END Shawn 2018-01-03 -->


   <div class="row">
      
      <div class="item panel hvr-border-fade child crawler color3" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/crawler_2_home_v2.jpg);">
        <a href="productinformation/peachpuff">
            <span class="title color3">星星餅</span>
            <span class="short-desc">蜜桃味</span>
        </a>
      </div>
      <div class="item panel hvr-border-fade child crawler color3" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/crawler_3_home_v2.jpg);">
        <a href="productinformation/bananapuff">
            <span class="title color3">星星餅</span>
            <span class="short-desc">香蕉味</span>
        </a>
      </div>
      <div class="item panel hvr-border-fade child crawler color3" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/crawler_4_home_v2.jpg);">
        <a href="productinformation/sweetpotatopuff">
            <span class="title color3">星星餅</span>
            <span class="short-desc">甜薯味</span>
        </a>
      </div>
   </div>
   <div class="row">
      
      <div class="item panel hvr-border-fade child crawler color3" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/crawler_5_home_v2.jpg);">
        <a href="productinformation/strawberryapplepuff">
            <span class="title color3">星星餅</span>
            <span class="short-desc">士多啤梨蘋果味</span>
        </a>
      </div>
      <div class="item panel hvr-border-fade child crawler color3" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/crawler_6_home.jpg);">
        <a href="productinformation/arrowrootcookies">
            <span class="title color3">薏米曲奇餅</span>
            <span class="short-desc"></span>
        </a>
      </div>
      <div class="item panel hvr-border-fade child crawler color3" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/crawler_7_home.jpg);">
        <a href="productinformation/ymmixedberries">
            <span class="title color3">乳酪乾</span>
            <span class="short-desc">雜莓味</span>
        </a>
      </div>
   </div>
   <div class="row">
      
      <div class="item panel hvr-border-fade child crawler color3" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/crawler_8_home.jpg);">
        <a href="productinformation/ympeach">
            <span class="title color3">乳酪乾</span>
            <span class="short-desc">蜜桃味</span>
        </a>
      </div>
      <div class="item panel child crawler text more-text" data-wow-duration="2s" data-wow-delay="0.1s">
        <a href="aboutus" >
            <span class="title">了解GERBER<sup>®</sup>產品營養之旅如何幫助寶寶健康成長及發展。</span>
            <span class="learn-more hvr-fade">了解更多</span>
        </a>
      </div>
   </div>
   <div class="row">
      <div class="color4 item panel supported-root" data-child="toddler"  onclick="btnCall(this);" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/toddler_main.jpg);">
        <img class="mobileOn" src="images/list/toddler.png" alt="" width="55"/>
        <span class="mobileOn title">12個月起</span>
        <span class="mobileOn arrow"><i class="fa fa-angle-down"></i></span>
      </div>
      <div class="item panel hvr-border-fade child toddler color4" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/toddler_1_home.jpg);">
        <a href="productinformation/lilbiscuit">
            <span class="title color4">牙仔餅</span>
            <span class="short-desc">小麥呍呢嗱味</span>
        </a>
      </div>
      <div class="item panel hvr-border-fade child toddler color4" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/toddler_2_home.jpg);">
        <a href="productinformation/animalcrackers">
            <span class="title color4">動物餅</span>
            <span class="short-desc">肉桂味</span>
        </a>
      </div>
   </div>
   <div class="row">
      <div class="item panel hvr-border-fade child toddler color4" data-wow-duration="2s" data-wow-delay="0.1s" style="background-image: url(images/list/list/toddler_3_home.jpg);">
        <a href="productinformation/bittybitesstrawberry">
           <img class="new_icon top-p" src="images/new-icon.png">
            <span class="title color4">士多啤梨方脆</span>
            <span class="short-desc"></span>
        </a>
      </div>
   </div>

   <!-- /.row -->
</div>
<!-- /.container -->
<?php include 'product/form.php'; ?>
<?php include 'footer.php'; ?>


<script src="js/list.js?v=20170601"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/noframework.waypoints.min.js"></script>

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BabyFood_Gerber_Products information
URL of the webpage where the tag is expected to be placed:
https://www.gerber.com.hk/productinformation
This tag must be placed between the <body> and </body> tags, as close as possible to the
opening tag.
Creation Date: 07/25/2017
-->
<iframe
src="https://8039665.fls.doubleclick.net/activityi;src=8039665;type=gerber00;cat=ger_02;dc_l
at=;dc_rdid=;tag_for_child_directed_treatment=;ord=<?php session_start(); echo str_replace(array(",","-","_"),"",session_id()); ?>?" width="1" height="1"
frameborder="0" style="display:none"></iframe>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</body>
</html>
